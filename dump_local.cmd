set startdir=%cd%
set dump_path=%cd%/dumps/local/%DATE%

mkdir dumps
mongodump -h localhost --port 27017 --gzip --out=%dump_path%
