# coding=utf-8

import logging

from src import VT_SOURCE, VT_COMPILATION, VIDEO_ID_LENGTH, CHANNEL_ID_LENGTH
from src.convert.cut import cut_video
from src.core.sources import get_sources_ids_of_compilation
from src.db import VC_Storage

from src.yt.info import get_video_info, get_channel_with_videos
from src.yt.load import get_loaded_filename, download_video

store = VC_Storage('core')
log = logging.getLogger('core')


def load_and_cut_compilation_sources(cuts):
    v_ids_to_load = set()

    for i, cut in enumerate(cuts):
        video_id = cut.get('video_id')
        from_s = cut.get("from_s")
        to_s = cut.get("to_s")
        if video_id and from_s is not None and to_s is not None:

            filename = get_loaded_filename(video_id)
            if video_id not in v_ids_to_load and not filename:
                v_ids_to_load.add(video_id)
                filename = download_video(video_id)
                if not filename:
                    raise Exception("can not download %s" % video_id)

            cut_fn = cut_video(video_id, cut['from_s'], cut['to_s'])
            if not cut_fn:
                raise Exception("can not cut filename:(")

            cuts[i]['filename'] = filename
            cuts[i]['cut_filename'] = cut_fn

            log.info("Create cut of compilation: \n%s" % "\n".join(['%s : %s' % (k, v) for k, v in cut.items()]))
        else:
            log.warning("No from_s or to_s or video_id in cut :(")

    return cuts


def check_extractor_source_video(source_video):
    if source_video and \
            not source_video.get('in_compilator') and \
            not source_video.get("deleted") and \
            not source_video.get("exclude") and \
            not store.get_pleas(source_video.get('video_id')) and \
            not store.is_channel_rejected(source_video.get("channel_id")):
        return True


def retrieve_and_store_related_videos(video):
    video_id = video.get('video_id')

    stored = store.get_videos_of_compilation(video_id)
    if stored:
        return stored

    result = []
    video_ids_in_compilation = []
    sources_video_ids = get_sources_ids_of_compilation(video.get('channel_id'),
                                                       video.get('description'))
    if not sources_video_ids:
        reloaded_video = get_video_info(video_id, False, False)
        if reloaded_video:
            sources_video_ids = get_sources_ids_of_compilation(reloaded_video.get('channel_id'),
                                                               reloaded_video.get('description'))
            if sources_video_ids:
                log.info("Reload video [%s]" % video_id)
                store.store_video(reloaded_video, VT_COMPILATION)

    log.info("For %s found sources: %s" % (video_id, sources_video_ids))
    for source_video_id in sources_video_ids:
        try:
            source_video = store.get_video(source_video_id)
            if not source_video:
                source_video = get_video_info(source_video_id)

                if source_video and source_video.get("channel_id") != video.get("channel_id"):
                    log.info('Store source: %s\n%s'%(source_video_id, source_video))
                    store.store_video(source_video, VT_SOURCE)
                    source_channel = store.get_channel(source_video.get("channel_id"))
                    if source_channel and source_channel.get('pleas_reject'):
                        store.set_video_exclude(source_video_id)
                        log.info("Exclude video %s because channel id is rejected %s" % (
                            source_video_id, source_video.get("channel_id")))
                        continue

                    log.info("Source video was loaded: \n%s" % source_video)
                elif not source_video:
                    log.info("Bad [%s]" % (source_video_id,))
                    store.delete_video(source_video_id, {"BAD": True, "because": "bad video id or bad video data"})
                    continue

            if check_extractor_source_video(source_video):
                result.append(source_video)
                video_ids_in_compilation.append(source_video_id)
            else:
                store.delete_video(source_video_id, {"BAD": True, "because": "not checked"})
                log.warning("Source video [%s] is BAD (deleted, low quality, copyright)" % source_video_id)

        except Exception as e:
            log.error("Can not load info about %s :(" % source_video_id)
            log.exception(e)

    if video_ids_in_compilation:
        store.set_compilation_id(list(set(video_ids_in_compilation)), video_id)

    return result


def load_and_store_channel_videos(channel_id, last_channel_video_id=None):
    info, channel_videos = get_channel_with_videos(channel_id, before_video_id=last_channel_video_id)

    if info:
        log.info("Storing [%s] info: \n%s" % (channel_id, info))
        store_result = store.store_channel(channel_id, VT_COMPILATION, info)
        log.info("Channel stored! \nResult: %s" % store_result.raw_result)

    if channel_videos:
        store.set_last_channel_video(channel_id, channel_videos[0].get("video_id"))
        for video in channel_videos:
            full_video = get_video_info(video.get("video_id"), False, False)
            sources_ids = get_sources_ids_of_compilation(channel_id, full_video.get("description"))
            if video and len(sources_ids):
                log.info("Found %s at %s with sources:\n%s " % (video.get("video_id"), channel_id, sources_ids))
                store.store_video(full_video, VT_COMPILATION)

    return channel_videos


def set_good(entity_id, not_):
    if not_ and not_.strip() in ['0', 'false']:
        not_ = False

    if len(entity_id) == VIDEO_ID_LENGTH:
        video = store.get_video(entity_id)
        if not video:
            video = get_video_info(entity_id)
            if not video:
                return False

        if video:
            store.set_entity_good(entity_id, not_)

        if video.get("channel_id"):
            store.set_entity_good(video.get("channel_id"), not_)
        return True

    elif len(entity_id) == CHANNEL_ID_LENGTH:
        channel_info = store.get_channel(entity_id)
        if not channel_info:
            channel_info, _ = get_channel_with_videos(entity_id, add_videos=False)
        if channel_info:
            store.set_entity_good(entity_id, not_)
        return True

    return False


# TODO use another method for analyze current channel
def get_my_channel_id(with_info=False):
    channel_info = store.get_my_channel_info()
    if channel_info:
        return channel_info.get('channel_id') if not with_info else channel_info
    comps = store.get_all_compilations({"video_id": {"$exists": True}})
    for comp in comps:
        video = get_video_info(comp.get('video_id'))
        if video and video.get('channel_id'):
            channel_id = video.get('channel_id')
            channel_info, videos = get_channel_with_videos(channel_id, add_videos=False)
            store.set_my_channel_info(channel_info)
            return channel_id if not with_info else channel_info
