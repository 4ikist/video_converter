import hashlib
import json
import logging

import re
from collections import defaultdict

from src import VT_COMPILATION
from src.db import VC_Storage, DataCache
from src.yt import get_youtube
from src.yt.comments import get_comments
from src.yt.info import get_many_videos, get_video_info
from src.yt.search import search_video_ids

VIDEO_FIND_BATCH_SIZE = 100

yt = get_youtube()
store = VC_Storage('find')
log = logging.getLogger("find")


def check_description(description, exclude_words):
    description = description.lower()

    for exclude_word in exclude_words:
        if exclude_word in description:
            return False
    return True


def check_donor_comments(comments, donors_channel_ids):
    comments_channel_ids = map(lambda x: x.get('channel_id'), comments)
    return list(set(comments_channel_ids).intersection(donors_channel_ids))


def check_have_good_comments(comments, comment_words_sequences):
    for comment in comments:
        comment_text = comment.get('text').lower()
        for word_sequence in comment_words_sequences:
            word_subs = []
            for word in word_sequence:
                word_subs.append(word in comment_text)
            if word_subs and all(word_subs):
                return True
    return False


class VideoFinder(object):
    ex_split = re.compile("[\n;]+")

    def get_channel_names(self, data):
        result = []
        for dc_id in data.get('donors_in_comments', []):
            channel = store.get_channel(dc_id)
            if channel:
                result.append(channel.get('title'))

        data['donors_in_comments'] = result
        return data

    def find_videos(self, context):
        log.info("Start find videos\n%s" % "\n".join(["%s: %s" % (k, v) for k, v in context.iteritems()]))

        q = context.get('q')
        ex_q = context.get('ex-q')
        ex_q = filter(lambda x: x, map(lambda x: x.strip(), VideoFinder.ex_split.split(ex_q)))

        posting_time_before = context.get('posting-time-before')
        posting_time_after = context.get('posting-time-after')

        video_length_min = int(context.get('video-length-min'))
        video_length_max = int(context.get('video-length-max'))

        not_saved_only = context.get('not-saved-only', False)

        with_comments = context.get('with-comment', False)
        comment_words_sequences = filter(lambda x: x,
                                         map(lambda seq: filter(lambda x: x, map(lambda word: str(word).strip(),
                                                                                 str(seq).strip().split(','))),
                                             str(context.get('comment-words')).lower().split('|')))

        channels = map(lambda x: x.get('channel_id'),
                       store.get_channels(channel_type=VT_COMPILATION, is_deleted=False, is_rejected=None))

        video_batch = []
        additional_data = {}

        log.info("Start search...")
        for video_search_info in search_video_ids(yt,
                                                  q=q,
                                                  before=posting_time_before,
                                                  after=posting_time_after):

            video_id = video_search_info.get('video_id')
            channel_id = video_search_info.get('channel_id')
            title = video_search_info.get('title')

            if not check_description(title, ex_q):
                log.info("Bad description [%s]" % video_id)
                continue

            if not_saved_only:
                video = store.get_video(video_id)
                if video:
                    log.info("Video already saved! [%s] with type: [%s]" % (video_id, video.get('type')))
                    continue

            channel = store.get_channel(channel_id)
            if channel and ('please_reject' in channel or channel.get('type') == VT_COMPILATION):
                log.info("Rejected channel [%s] for [%s]" % (channel_id, video_id))
                continue

            pleas = store.get_pleas(video_id)
            if pleas:
                log.info("Have already pleas [%s] [%s]" % (video_id, pleas.get('state')))
                continue

            if store.is_source_exclude(video_id):
                log.info("Source excluded or bad [%s]" % video_id)
                continue

            if with_comments:
                comments = list(get_comments(yt, video_id=video_id))
                donors_in_comments = check_donor_comments(comments, channels)
                have_good_comments = check_have_good_comments(comments, comment_words_sequences)

                if donors_in_comments:
                    additional_data[video_id] = {'donors_in_comments': donors_in_comments}
                elif have_good_comments:
                    pass
                else:
                    log.info("Video have not donor comments and have not good comments [%s]" % video_id)
                    continue


            video = get_video_info(video_id, part="snippet,contentDetails")
            if not video:
                log.info("Bad video [%s]"%video_id)
                continue

            duration = video.get('duration')
            description = video.get('description')

            if duration < video_length_min or duration > video_length_max or not check_description(description,
                                                                                                   ex_q):
                log.info("Was rejected [%s] because not checked description or bad length" % video_id)
                continue

            if video_id in additional_data:
                video['additional_data'] = self.get_channel_names(additional_data.get(video_id))

            log.info("Found [%s] for videos query" % video_id)

            yield video


class VideoFinderWrapper(object):
    def __init__(self):
        self.context_hash = None
        self.cache = DataCache()
        self.counter = defaultdict(int)

    def _ensure_generator(self, context):
        new_hash = hashlib.sha224(json.dumps(context)).hexdigest()

        if self.context_hash != new_hash or self.counter.get(new_hash) == 0:
            self.context_hash = new_hash
            self.counter[new_hash] = 0
            cached_data = self.cache.get(new_hash)

            if cached_data.count() > 0:
                self.video_generator = iter(list(cached_data))
                self.finder = None
                return

            self.finder = VideoFinder()
            self.video_generator = self.finder.find_videos(context)

    def get_next(self, context):
        self._ensure_generator(context)
        try:
            video = self.video_generator.next()
        except StopIteration:
            video = None
        except Exception as e:
            log.exception(e)
            return

        if self.finder and video:
            self.cache.set_data(self.context_hash, video)
        if video:
            self.counter[self.context_hash] += 1
        else:
            self.counter[self.context_hash] = 0

        return video
