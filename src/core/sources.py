# coding=utf-8

import logging
import re
import requests

from src.db import VC_Storage
from src.yt import get_youtube
from src.yt.comments import get_comments
from src.yt.info import get_video_info, get_user_channel_id

yt = get_youtube()
store = VC_Storage("core_sources")
log = logging.getLogger("core_sources")

yt_video_regexp = re.compile("https?\:\/\/((www\.youtube\.com\/watch\?v=)|(youtu\.be\/))(.{11}).*")
yt_channel_regexp = re.compile("https?\:\/\/www\.youtube\.com\/channel\/(.{24}).*")
yt_user_regexp = re.compile("https?\:\/\/www\.youtube\.com\/user\/([a-zA-Z0-9_-]+).*")
url_regexp = re.compile("https?\:\/\/[^ \n\t]+")


def _check_contains(what, into):
    for el in what:
        if el in into:
            return True
    return False


def _unshorten_url(url):
    result = requests.head(url, allow_redirects=True)
    return result.url


def _find_source_video(of, user_name=None, channel_id=None):
    if user_name:
        channel_id = channel_id or get_user_channel_id(user_name)

    if not channel_id:
        log.warning("Channel id is not recognised:( %s %s %s" % (of, user_name, channel_id))
        return None

    comments = get_comments(yt, channel_id=channel_id, less_than=1000)
    for comment in comments:
        if comment.get('channel_id') == of:
            # todo storing info!
            log.info("In comments of %s %s found this: %s" % (user_name, channel_id, comment))
            return comment.get('video_id')


retrieve_videos = lambda description, check_exclude: set(
    filter(lambda x: check_exclude and not store.is_source_exclude(x) or check_exclude,
           map(lambda x: x[3].strip(),
               yt_video_regexp.findall(description))))

retrieve_users = lambda description: set(filter(lambda x: x, yt_user_regexp.findall(description)))
retrieve_channels = lambda description, of: set(filter(lambda x: x and x != of, yt_channel_regexp.findall(description)))


def get_sources_ids_of_compilation(of, description, check_exclude=True):
    related_video_ids = retrieve_videos(description, check_exclude)

    related_users = retrieve_users(description)

    related_channels = retrieve_channels(description, of)

    related_urls = set(filter(lambda x: store.is_url_bad(x) is None and \
                                        not _check_contains(related_users, x) and \
                                        not _check_contains(related_channels, x) and \
                                        not _check_contains(related_video_ids, x),
                              url_regexp.findall(description)))

    log.info("Of %s found:\nusers:\n%s\nchannels:\n%s\nurls:\n%s\n" % (
        of, '\n'.join(related_users), '\n'.join(related_channels), '\n'.join(related_urls)))

    for user in related_users:
        source = _find_source_video(of, user_name=user)
        if source:
            log.info("Found %s of user %s" % (source, user))
            related_video_ids.add(source)

    for channel in related_channels:
        source = _find_source_video(of, channel_id=channel)
        if source:
            log.info("Found %s of channel %s" % (source, channel))
            related_video_ids.add(source)

    for _url in related_urls:
        url = _unshorten_url(_url)
        video_ids = retrieve_videos(url, check_exclude)
        if video_ids:
            log.info("Unshort video: %s" % video_ids)
            related_video_ids.add(list(video_ids)[0])
            continue

        users = retrieve_users(url)
        if users:
            user = list(users)[0]
            source = _find_source_video(of, user_name=user)
            if source:
                log.info("Unshort and found %s of user %s" % (source, user))
                related_video_ids.add(source)
                continue

        channels = retrieve_channels(url, of)
        if channels:
            channel = list(channels)[0]
            source = _find_source_video(of, channel_id=channel)
            if source:
                log.info("Unshort and found %s of user %s" % (source, channel))
                related_video_ids.add(source)
                continue

        store.add_bad_url(_url)

    return related_video_ids


if __name__ == '__main__':
    user_url = "https://www.youtube.com/user/yz450rider1"
    channel_url = "https://www.youtube.com/channel/UC7f2tzjqhCnm0ccrY0bK0RA"
    video_url = "https://www.youtube.com/watch?v=_lro0YOmIBo"
    shortened_ur = "http://bit.ly/Madness_Stickers"

    channel_id = "UC5nVgiv6elz5GnS6MTr1tVA"
    video_id = "BgbMSdn1G2I"
    video = get_video_info(video_id)
    source_ids = get_sources_ids_of_compilation(channel_id, video.get('description'))
    print "\n".join(source_ids)
