import logging

from src import init_log, PS_REJECTED, VT_COMPILATION
from src.core import get_sources_ids_of_compilation
from src.yt import get_youtube, next_page_iter
from src.yt.info import get_channel_with_videos, get_channels_deleted_info_list

from time import sleep

from src.db import VC_Storage

SLEEP_TIME = 3600 * 4
MIN_URLS_IN_DESCRIPTION = 10


def _is_channel_commented(channel_id, comments):
    for comment in comments:
        if comment.get("snippet", {}).get("authorChannelId", {}).get("value") == channel_id:
            return True
    return False


def get_tracked_channels_from_comments(youtube, video_ids, exclude_comments, exclude_channel_ids, storage):
    '''
    Return channel ids of specific comments of video_ids. Comments must be with replies and in replies must be comment
    of parent channel
    :param video_ids: ids of videos which will be process
    :param exclude_comments: exclude comments (only for topLevelComments)
    :param exclude_channel_ids:  exclude channels which commented in replies
    :return:
    '''
    for video_id in video_ids:
        q = {"part": "snippet,replies",
             "textFormat": "plainText",
             "order": "time",
             "videoId": video_id,
             "maxResults": 100}
        list_func = youtube.commentThreads().list
        try:
            items_count = 0
            for item in next_page_iter(list_func, q):
                items_count += 1
                snippet = item.get("snippet", {})
                if not snippet:
                    continue

                parent = snippet.get("topLevelComment")
                if not parent:
                    continue

                replies_count = snippet.get('totalReplyCount')
                if replies_count <= 1:
                    continue

                replies = item.get("replies", {}).get("comments")
                if not replies:
                    continue

                parent_channel_id = parent.get("snippet", {}).get("authorChannelId", {}).get("value")
                if parent_channel_id in exclude_channel_ids:
                    continue

                parent_id = parent.get("id")

                if parent_id not in exclude_comments and _is_channel_commented(parent_channel_id, replies):
                    for reply in replies:
                        reply_channel_id = reply.get("snippet", {}).get("authorChannelId", {}).get("value")
                        if reply_channel_id not in exclude_channel_ids:
                            yield reply_channel_id
                            exclude_channel_ids.add(reply_channel_id)
            if not items_count:
                storage.set_pleas_state(video_id, PS_REJECTED)
        except Exception as e:
            log.exception(e)

        log.info("wait for lower count of queries...")
        sleep(2)


def track_channels():
    log.info("Will find tracking channels...")
    tracked_channels = map(lambda x: x.get("channel_id"),
                           store.get_channels_tracked(track=True, projection={"channel_id": 1}))
    log.info("Found %s, are:\n%s" % (len(tracked_channels), ", ".join(tracked_channels)))
    deleted_channels = get_channels_deleted_info_list(tracked_channels)

    log.info("Deleted channels %s, are:\n%s" % (len(deleted_channels), ", ".join(deleted_channels)))
    for channel_id in deleted_channels:
        store.set_channel_tracking(channel_id, is_die=True, track=True)


def fill_tracked_channels():
    all_pleases = store.get_all_pleases(except_states=[PS_REJECTED], projection={"video_id": 1, "comment_id": 1})
    video_ids = map(lambda x: x.get("video_id"), all_pleases)

    channels_projection = {"channel_id": 1}
    exclude_channels = store.get_channels_tracked(track=True, projection=channels_projection) \
                       + store.get_channels_pleas_reject(projection=channels_projection) \
                       + store.get_channels(channel_type=VT_COMPILATION, is_deleted=False,
                                            projection=channels_projection) \
                       + store.get_channels(is_deleted=True, projection=channels_projection)

    exclude_channel_ids = set(map(lambda x: x.get("channel_id"), exclude_channels))

    exclude_comment_ids = set(filter(lambda x: x, map(lambda x: x.get("comment_id"), all_pleases)))

    log.info(
        "Data prepared! Will find tracking channels by videos (%s):\n%s\nexcluded:\n\tchannels (%s):\n%s\n\n\tcomments (%s): \n%s\n" \
        % (len(video_ids), ", ".join(video_ids),
           len(exclude_channel_ids), ", ".join(exclude_channel_ids),
           len(exclude_comment_ids), ", ".join(exclude_comment_ids)))

    for channel_id in get_tracked_channels_from_comments(yt, video_ids, exclude_comment_ids, exclude_channel_ids,
                                                         store):
        channel, videos = get_channel_with_videos(channel_id, add_videos=True)
        count_urls = sum(
            map(lambda x: len(get_sources_ids_of_compilation(x.get('channel_id'), x.get("description"))), videos))
        if count_urls >= MIN_URLS_IN_DESCRIPTION:
            log.info("Adding channel tracking: [%s]" % channel_id)
            store.store_channel(channel_id, VT_COMPILATION, channel)
            store.set_channel_tracking(channel_id, track=True)
            sleep(5)
        else:
            store.set_channel_tracking(channel_id, track=False)
            store.delete_channel(channel_id)


log = logging.getLogger("ct")

if __name__ == '__main__':
    init_log("channel_tracking")

    store = VC_Storage("channel_tracking")
    yt = get_youtube()

    while 1:
        track_channels()
        fill_tracked_channels()
        sleep(SLEEP_TIME)
