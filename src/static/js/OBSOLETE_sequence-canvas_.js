// Замыкание
(function () {
    /**
     * Корректировка округления десятичных дробей.
     *
     * @param {String}  type  Тип корректировки.
     * @param {Number}  value Число.
     * @param {Integer} exp   Показатель степени (десятичный логарифм основания корректировки).
     * @returns {Number} Скорректированное значение.
     */
    function decimalAdjust(type, value, exp) {
        // Если степень не определена, либо равна нулю...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // Если значение не является числом, либо степень не является целым числом...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Сдвиг разрядов
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Обратный сдвиг
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    // Десятичное округление к ближайшему
    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    // Десятичное округление вниз
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    // Десятичное округление вверх
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }
})();

var testVideos = [
    {videoId: 'NlwLckpgXqY', title: 'One', channel: 'some channel 1', rate: 1, from: 0, to: 5},
    {videoId: 'xTpfjF8NnY4', title: 'Two', channel: 'some channel 2', rate: 1, from: 0, to: 5},
    {videoId: 'OWUTCVo-osk', title: 'Three', channel: 'some channel 3', rate: 1, from: 0, to: 5, category: 'cats'}
];

var tesVideoCuts = [
    {video_id: 'NlwLckpgXqY', category: 'dogs', rate: 1, from: 0, to: 6},
    {video_id: 'xTpfjF8NnY4', category: 'cats', rate: 2, from: 1, to: 6}
];

var PLAYERS = {};

function GetVideos(categoryOrVideoId, callback) {
    $.ajax({
        type: 'get',
        url: '/compilator/data/' + value,
        success: function (data) {
            if (data.ok) {
                callback(data.related_videos);
            } else {
                $("#error-container").text(data.error);
            }
        }
    });
    return []
}

function strFtime(int) {
    let time = Math.round10(int, -2);
    console.log('srtFtime', int, time);
    let minutes = Math.floor(time / 60),
        seconds = time - minutes * 60;

    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    return minutes + ":" + seconds;
}

function strPtime(str) {
    let timeArr = str.split(":"),
        minutes = timeArr[0],
        seconds = timeArr[1],
        result = parseInt(minutes) * 60 + parseFloat(seconds);
    console.log('srtPtime', minutes, seconds, result);
    return result;
}

function createPlayer(videoId, placeholder) {
    return new YT.Player(placeholder, {
        height: 508,
        width: 754,
        videoId: videoId
    });
}

var VideoStepsButtons = React.createClass({
    propTypes: {
        videoId: React.PropTypes.string
    },

    stepVideo(left) {
        let player = PLAYERS[this.props.videoId],
            currentTime = player.getCurrentTime();
        if (left) {
            currentTime -= 0.1;
        } else {
            currentTime += 0.1;
        }
        player.seekTo(currentTime, true);
    },

    render: function () {
        let stepLeft = () => {
                this.stepVideo(true)
            },
            stepRight = () => {
                this.stepVideo(false)
            };

        return (
            <div className="col-md-12 step-buttons">
                <button className="btn btn-xs" onClick={stepLeft}>
                    <span className="glyphicon glyphicon-step-backward"/>
                </button>
                <button className="btn btn-xs" aria-hidden="true" onClick={stepRight}>
                    <span className="glyphicon glyphicon-step-forward"/>
                </button>
            </div>
        )
    }
});

class TimeInput extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            category: props.category,
            rate: props.rate,
            from: strFtime(props.from),
            to: strFtime(props.to)
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log("receive props", nextProps, '...');
        this.setState({
            index: nextProps.index,
            category: nextProps.category,
            rate: nextProps.rate,
            from: strFtime(nextProps.from),
            to: strFtime(nextProps.to)
        })
    }

    onFocus(fieldName, e) {
        let player = PLAYERS[this.props.videoId],
            currentTime = player.getCurrentTime(),
            showedValue = strFtime(currentTime),
            node = ReactDOM.findDOMNode(e.target);

        node.value = showedValue;
        if (showedValue !== this.state[fieldName]) {
            this.onFieldChange(fieldName, e)
        }
        node.focus();
    }

    getTimeInputState() {
        var result = this.state;
        result['from'] = strPtime(this.state.from);
        result['to'] = strPtime(this.state.to);
        return result;
    }

    static isTimeField(fieldName) {
        return fieldName === 'from' || fieldName === 'to'
    }

    onFieldChange(fieldName, e) {
        e.preventDefault();
        console.log('field changed... time? :', TimeInput.isTimeField(fieldName));
        var value = TimeInput.isTimeField(fieldName) ? strPtime(e.target.value) : e.target.value,
            oldValue = TimeInput.isTimeField(fieldName) ? strPtime(this.state[fieldName]) : this.state[fieldName];
        if (oldValue !== value) {
            this.props.update(this.props.index, fieldName, value);
        }
    }

    render() {
        let state = this.state ? this.state : GetDefaultTimeInput(this.props);
        let add = () => {
                this.props.add(strPtime(this.state.to), strPtime(this.state.to), this.state.category, this.state.rate)
            },
            remove = () => {
                this.props.remove(this.props.index)
            };
        return (
            <div className="row">
                <div className="col-xs-2">
                    {this.props.index === 0 ? <VideoStepsButtons videoId={this.props.videoId}/> : null}
                </div>
                <div className="col-xs-2">
                    <label>From:</label>
                    <input className="form-control" type='text' ref='fromInput'
                           onFocus={this.onFocus.bind(this, 'from')} onChange={this.onFieldChange.bind(this, 'from')}
                           value={state.from}/>
                </div>
                <div className="col-xs-2">
                    <label>To:</label>
                    <input className="form-control" type='text' ref='toInput' onFocus={this.onFocus.bind(this, 'to')}
                           onChange={this.onFieldChange.bind(this, 'to')} value={state.to}/>
                </div>
                <div className="col-xs-2">
                    <label>Category:</label>
                    <input className="form-control" type='text' ref='catInput'
                           onChange={this.onFieldChange.bind(this, 'category')}
                           value={state.category ? state.category : ""}/>
                </div>
                <div className="col-xs-2">
                    <label>Rate:</label>
                    <input className="form-control" ref='rateInput' type="number"
                           onChange={this.onFieldChange.bind(this, 'rate')} value={state.rate ? state.rate : 0}/>
                </div>


                <div className="col-xs-2">
                    <button className="btn btn-xs" onClick={add} disabled={state.from === state.to}>
                        <span className="glyphicon glyphicon-plus"/>
                    </button>
                    <button className="btn btn-xs" onClick={remove}
                            disabled={state.from === state.to || this.props.count() === 1}>
                        <span className="glyphicon glyphicon-minus"/>
                    </button>
                </div>
            </div>
        )
    }
}

TimeInput.propTypes = {
    videoId: React.PropTypes.string,

    from: React.PropTypes.number,
    to: React.PropTypes.number,
    rate: React.PropTypes.number,
    category: React.PropTypes.string,

    index: React.PropTypes.number,

    remove: React.PropTypes.func,
    add: React.PropTypes.func,
    count: React.PropTypes.func,
    update: React.PropTypes.func,

};

class TimeInputs extends React.Component {
    constructor(props) {
        super(props)
    }

    id(item) {
        return `time-input-${this.props.videoId}-${item.from}-${item.to}`
    }

    render() {
        let
            template = this.props.getItems().map((item, index) => {
                let id = this.id.bind(this, item);
                console.log(item, index, id);
                return (
                    <TimeInput
                        key={id()}

                        videoId={this.props.videoId}
                        index={index}

                        from={item.from}
                        to={item.to}
                        category={item.category}
                        rate={item.rate}

                        add={this.props.add}
                        remove={this.props.remove}
                        count={this.props.count}
                        update={this.props.update}
                    />
                )
            });

        return (
            <div className='time-inputs'>
                {template}
            </div>
        )
    }
}

TimeInputs.propTypes = {
    videoId: React.PropTypes.string,
    getItems: React.PropTypes.func,

    remove: React.PropTypes.func,
    add: React.PropTypes.func,
    count: React.PropTypes.func,
    update: React.PropTypes.func,
};

var GetDefaultTimeInput = (props) => {
    return {
        from: props.from ? props.from : 0,
        to: props.to ? props.to : 0,
        category: props.string ? props.string : null,
        rate: props.rate ? props.rate : 0
    }
};

var ModalCloseButton = React.createClass({
    propTypes: {
        toggleCallback: React.PropTypes.func
    },
    render: function () {
        return (
            <button className="close" aria-label="Close" onClick={(e) => {
                this.props.toggleCallback(e)
            }}>
        <span aria-hidden="true">&times;
        </span>
            </button>
        )
    }
});

class ModalVideoCuts extends React.Component {
    constructor(props) {
        console.log('ModalVideoCuts constructor');
        super(props);
        this.state = {
            show: false,
            timeInputs: [GetDefaultTimeInput(this.props.cutContainer)]
        }
    }

    toggle() {
        console.log('will toggle...');
        if (!(this.props.videoId in PLAYERS)) {
            PLAYERS[this.props.videoId] = createPlayer(this.props.videoId, this.id('video-placeholder'))
        }
        this.setState({show: !this.state.show})
    }

    getItems() {
        return this.state.timeInputs;
    }

    addInput(from, to, category, rate) {
        console.log('add input', from, to, category, rate);
        let items = this.state.timeInputs;
        items.push({from: from, to: to, category: category, rate: rate});
        this.setState({timeInputs: items})
    }

    updateInput(index, fieldName, value) {
        console.log('update', index, fieldName, value);
        let items = this.state.timeInputs;
        items[index][fieldName] = value;
        this.setState({timeInputs: items})
    }

    removeInput(index) {
        console.log('remove input', index);
        let items = this.state.timeInputs;
        items.splice(index, 1);
        this.setState({timeInputs: items})
    }

    count() {
        return this.state.timeInputs.length
    }

    submit() {
        console.log(this.state.timeInputs);
        let add = this.props.cutMethods.addCut,
            update = this.props.cutMethods.updateCut,
            myCuts = this.state.timeInputs;

        for (var x in myCuts) {
            let cut = myCuts[x];
            cut['videoId'] = this.props.videoId;
            if (x === 0) {
                update(this.props.position, cut)
            } else {
                add(this.props.position + x - 1, cut)
            }
        }

        this.toggle();

    }

    id(what) {
        return `${what}-${this.props.videoId}-${this.props.position}`
    }

    render() {
        let id = `manage-video-cuts-${this.props.videoId}`;
        let toggleCallback = this.toggle.bind(this),
            add = this.addInput.bind(this),
            remove = this.removeInput.bind(this),
            update = this.updateInput.bind(this),
            count = this.count.bind(this),
            getItems = this.getItems.bind(this);

        return (
            <div className="form-group">
                <button className="btn btn-sm" data-toggle="modal"
                        data-target={`#${id}`} onClick={toggleCallback}>
                    <span className="position-move glyphicon glyphicon-play"/>
                </button>
                <div className={`modal ` + (this.state.show ? 'show' : 'fade')} tabIndex="-1" role="dialog" id={id}>
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-header">
                            <ModalCloseButton toggleCallback={toggleCallback}/>
                            <h4 className="modal-title">{`Watch and choose cuts of ${this.props.title}`}</h4>
                        </div>
                        <div className="modal-content">
                            <div id={this.id('video-placeholder')} ref='videoPlaceholder'/>
                            <TimeInputs
                                videoId={this.props.videoId}
                                add={add}
                                remove={remove}
                                update={update}
                                count={count}
                                getItems={getItems}
                            />
                            <button className='btn btn-sm' onClick={() => {
                                return this.submit()
                            }} disabled={this.state.timeInputsCount === 0}>
                                <span className="position-move glyphicon glyphicon-ok"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ModalVideoCuts.PropTypes = {
    videoId: React.PropTypes.string,
    position: React.PropTypes.number,

    cutContainer: React.PropTypes.shape({
        from: React.PropTypes.number,
        to: React.PropTypes.number,
        rate: React.PropTypes.number,
        category: React.PropTypes.string
    }),

    cutMethods: React.PropTypes.shape({
        addCut: React.PropTypes.func,
        updateCut: React.PropTypes.func
    }),

};


var VideoContainer = React.createClass({
    propTypes: {
        position: React.PropTypes.number,
        videoId: React.PropTypes.string,
        title: React.PropTypes.string,
        channel: React.PropTypes.string,

        cutContainer: React.PropTypes.shape({
            from: React.PropTypes.number,
            to: React.PropTypes.number,
            rate: React.PropTypes.number,
            category: React.PropTypes.string,
        }),

        cutMethods: React.PropTypes.shape({
            addCut: React.PropTypes.func,
            updateCut: React.PropTypes.func
        }),

        updatePositions: React.PropTypes.func,
        getPositions: React.PropTypes.func
    },

    changePosition: function (up) {
        let items = this.props.getPositions(),
            oldPosition = this.props.position;

        let newPosition;
        if (up) {
            newPosition = oldPosition - 1
        } else {
            newPosition = oldPosition + 1
        }
        let anotherItem = items[newPosition];
        items[newPosition] = this.props;
        items[oldPosition] = anotherItem;
        this.setState({position: newPosition});
        this.props.updatePositions(items);
    },

    moveUp: function () {
        this.changePosition(true);
    },

    moveDown: function () {
        this.changePosition(false);
    },

    render: function () {
        return (
            <div className='video-container'>
                <p className='video-title'> {this.props.title} </p>
                <p className='video-channel'> {this.props.channel}</p>
                <ModalVideoCuts
                    videoId={this.props.videoId}
                    position={this.props.position}
                    title={this.props.title}
                    cutContainer={this.props}
                    cutMethods={{addCut: this.props.addCut, updateCut: this.props.updateCut}}

                />
                <div className='form-group'>
                    <button className="btn btn-sm" onClick={this.moveUp} disabled={this.props.position === 0}>
                        <span className="position-move glyphicon glyphicon-arrow-up" aria-hidden="true"/>
                    </button>
                    <button className="btn btn-sm" onClick={this.moveDown}
                            disabled={this.props.position === this.props.getPositions().length - 1}>
                        <span className="position-move glyphicon glyphicon-arrow-down" aria-hidden="true"/>
                    </button>
                </div>

            </div>
        )
    }
});

class ResultTime extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let time = strFtime(this.props.calcTime());

        return (
            <div className='result-time'>
                <span id="all-time" className="bg-primary big">{time}|{this.props.time}</span>
            </div>
        )
    }
}

ResultTime.PropTypes = {
    calcTime: React.PropTypes.func,
    time: React.PropTypes.number
};

class Sequence extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            positions: [],
            cuts: {},
            videos: {},
            time: 0
        }
    }

    updatePositions(items) {
        let videos = this.state.videos;
        for (let item in items) {
            videos[item.videoId] = item
        }

        this.setState({positions: items})
    }

    getPositions() {
        return this.state.positions
    }

    getVideoInfo(videoId) {
        return this.state.videos[videoId]
    }

    static insertToCuts(cuts, cut) {
        if (!(cut.videoId in cuts)) {
            cuts[cut.videoId] = {}
        }
        if (!(cut.from in cuts[cut.videoId])) {
            cuts[cut.videoId][cut.from] = {}
        }
        cuts[cut.videoId][cut.from][cut.to] = cut;
        return cuts
    }

    calcTime(cuts) {
        let allTime = 0,
            calcCuts = (cuts === undefined) ? this.state.cuts : cuts;

        for (const videoId of Object.keys(calcCuts)) {
            for (const from of Object.keys(calcCuts[videoId])) {
                for (const to of Object.keys(calcCuts[videoId][from])) {
                    allTime += to - from
                }
            }
        }
        return allTime
    }

    updateCut(position, cut) {
        let cuts = this.state.cuts,
            positions = this.state.positions,
            oldCut = positions[position];

        if ((oldCut.videoId in cuts) && (oldCut.from in cuts[oldCut.videoId]) && (oldCut.to in cuts[oldCut.videoId][oldCut.from])) {
            delete cuts[oldCut.videoId][oldCut.from]
        }
        positions[position] = cut;
        cuts = Sequence.insertToCuts(cuts, cut);
        this.setState({cuts: cuts, positions: positions, time: this.calcTime(cuts)})

    }

    addCut(afterPosiotion, cut) {
        var positions = this.state.positions,
            cuts = this.state.cuts;

        cuts = Sequence.insertToCuts(cuts, cut);
        positions.splice(afterPosiotion, 0, cut);

        this.setState({cuts: cuts, positions: positions, time: this.calcTime(cuts)})
    }

    render() {
        let positions = this.state.positions,
            updatePositions = this.updatePositions.bind(this),
            getPositions = this.getPositions.bind(this),
            addCut = this.addCut.bind(this),
            updateCut = this.updateCut.bind(this),
            calcTime = this.calcTime.bind(thie);


        let template = positions.map((item, index) => {

            let title = item.title ? item.title : this.state.videos[item.videoId].title,
                channel = item.channel ? item.channel : this.state.videos[item.videoId].channel;

            return (
                <VideoContainer key={`${index}-${item.videoId}`}
                                position={index}
                                videoId={item.videoId}

                                title={title}
                                channel={channel}

                                from={item.from}
                                to={item.to}
                                category={item.category}
                                rate={item.rate}

                                updatePositions={updatePositions}
                                getPositions={getPositions}
                                addCut={addCut}
                                updateCut={updateCut}
                />
            )
        });

        return (
            <div className='sequence'>
                <ResultTime calcTime={calcTime}
                            time={time}

                />

                <div className='video-containers'>
                    {template}
                </div>
            </div>
        )
    }
}

// TODO realise with autocomplete
var CategoryInput = React.createClass({
    componentDidMount: function () { //ставим фокус в input
        ReactDOM.findDOMNode(this.refs.categoryInput).focus();
    },

    onBtnClickHandler: function () {
        let value = ReactDOM.findDOMNode(this.refs.categoryInput).value;
        console.log(value);
    },

    render: function () {
        return (
            <div>
                <input
                    className='test-input'
                    defaultValue=''
                    ref='categoryInput'
                    placeholder='введите значение'
                />
                <button onClick={this.onBtnClickHandler}>Подсосать</button>
            </div>
        );
    }
});

class SequenceCanvas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentCategory: '',
            videos: testVideos,
        }
    }

    render() {
        return (
            <div className='sequence-canvas'>
                <CategoryInput/>
                <Sequence videos={this.state.videos}/>
            </div>
        )
    }
});

function run() {
    ReactDOM.render(<SequenceCanvas/>, document.getElementById('sequence-canvas'));
}

const loadedStates = ['complete', 'loaded', 'interactive'];

if (loadedStates.includes(document.readyState) && document.body) {
    run();
} else {
    window.addEventListener('DOMContentLoaded', run, false);
}
