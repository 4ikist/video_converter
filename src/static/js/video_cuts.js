//#all-time
var players = {};
var all_categories;

window.videoManageButtonPartial = function (video_id) {
    var template = _.template(`
       <button onclick="initModal('<%= video_id%>')" class="btn btn-sm" title="Выбрать кусочки для компиляции с этого видоса">
            <span class="glyphicon glyphicon-indent-left" aria-hidden="true"></span>
        </button>
    `);
    return template({video_id: video_id});
};


window.cutModalTemplatePartial = function (videoSource) {
    if (videoSource.number == undefined) {
        videoSource.number = 0;
    }
    var template = _.template(`
                    <div class="modal fade modal-lg" tabindex="-1" role="dialog" id="modal-video-placeholder-<%= videoSource.video_id%>">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title"><%= videoSource.title %></h4>
                                </div>
                                <div class="modal-body" video-id="<%= videoSource.video_id %>">
                                    <div id="video-placeholder-<%= videoSource.video_id %>"></div>
                                    <%= timeInputTemplateHtmlPartial(videoSource.video_id, videoSource.from_s, videoSource.to_s, videoSource.number) %>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary btn-sm" onclick="storeCuts('<%= videoSource.video_id%>');">Ага</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="time-cut-info">

                    </div>
    `);
    return template({videoSource: videoSource});
};


window.timeInputTemplateHtmlPartial = function (videoId, timeFrom, timeTo, number) {
    var template = _.template(`
    <div class="row time-input-container number-<%= number %>" video-id='<%= videoId %>'>
        <div class="col-md-10">
            <div class="row time-manage-container not-listened <% if (used == true) { %> imply-cut <% } %>" video-id="<%= videoId %>">
                <div class="col-xs-2">
    
                    <button class="btn btn-default btn-xs" aria-hidden="true" onclick="step_video('<%= videoId %>', true);">
                        <span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
                    </button>
                    <button class="btn btn-default btn-xs" aria-hidden="true" onclick="step_video('<%= videoId %>', false);">
                        <span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
                    </button>
    
                </div>
                <div class="col-xs-2">
                    <label>From:</label>
                    <input class="form-control vt_from time-input" type='text' video-id="<%= videoId %>" value="<%= timeFrom %>"/>
                </div>
                <div class="col-xs-2">
                    <label>To:</label>
                    <input class="form-control vt_to time-input" type='text' video-id="<%= videoId %>" value="<%= timeTo %>"/>
                </div>
                
                <div class="col-xs-3">
                    <label>Category</label>
                    <input class="form-control category-preload number-<%= number %>" type="text" video-id="<%= videoId %>" placeholder="Вводи имя категории"/>
                </div>
                
                <div class="col-xs-2">
                    <label>Rate</label>
                    <input class="form-control rate number-<%= number %>" type="number" video-id="<%= videoId %>" value="1"/>
                </div>
                
                
                <div class="col-xs-1 delete-time-section" hidden="true">
                    <button class="btn btn-default btn-xs" aria-hidden="true" onclick="delete_time_manage('<%= videoId %>', <%= number %> );">
                         <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                    </button>
                </div>
                
                 <div class="col-xs-1 add-time-section">
                    <button class="btn btn-default btn-xs" aria-hidden="true" onclick="add_time_manage('<%= videoId %>', <%= number %> );">
                         <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    `);
    return template({
        videoId: videoId,
        timeFrom: toKitchenTime(timeFrom),
        timeTo: toKitchenTime(timeTo),
        number: number,
        used: false
    });
};

function formatTime(time) {
    time = Math.round(time);

    var minutes = Math.floor(time / 60),
        seconds = time - minutes * 60;

    seconds = seconds < 10 ? '0' + seconds : seconds;

    return minutes + ":" + seconds;
}
function parseTime(timeStr) {
    var timeArr = timeStr.split(":"),
        minutes = timeArr[0],
        seconds = timeArr[1];
    return parseInt(minutes) * 60 + parseInt(seconds);
}
function toKitchenTime(timeInt) {
    if (timeInt == undefined) {
        return "00:00"
    }
    var seconds = "0" + (timeInt % 60),
        minutes = "0" + ((timeInt - seconds) / 60);
    return minutes.substr(-2) + ":" + seconds.substr(-2)
}

function _addCutTime(cutTimeContainer, cuts) {
    var from = cutTimeContainer.find('.vt_from').val(),
        to = cutTimeContainer.find('.vt_to').val(),
        category = cutTimeContainer.find('.category-preload').val(),
        rate = cutTimeContainer.find('.rate').val();

    if (from !== "" && to !== "" && category !== "" && rate!== "") {
        cuts.push({'from': parseTime(from), 'to': parseTime(to), 'category': category, 'rate':rate});
        cutTimeContainer.find('.time-manage-container').addClass('imply-cut');
    } else {
        alert("Не все заполнил! Заполняй все!");
        return null;
    }
    return cuts
}

function storeCuts(videoId) {
    var inputContainers = $('.time-input-container[video-id=' + videoId + ']'),
        cuts = [];

    for (i=0; i<inputContainers.length; i++){
        var container = $(inputContainers[i]);
        var newCuts = _addCutTime(container, cuts);
        if (newCuts != null){
            cuts = newCuts;
        } else {
            return;
        }
    }


    $.ajax({
        url: "/cuts",
        type: 'POST',
        data: JSON.stringify({"cuts": cuts, "video_id":videoId}),
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data.ok) {
                console.log(videoId, "cuts saved!");
            } else {
                console.log(videoId, "cuts save error:", data.error);
            }
        },
        error: function () {
            console.log('Error cuts save');
        }
    });

    players[videoId].stopVideo();
    $('#'+videoId).addClass('used');
    $('#modal-video-placeholder-'+videoId).modal('hide');
}

function createPlayer(videoId) {
    return new YT.Player("video-placeholder-" + videoId, {
        height: 315,
        width: 760,
        videoId: videoId
    });
}

function step_video(videoId, left) {
    var player = players[videoId],
        currentTime = player.getCurrentTime();

    if (left) {
        currentTime--;
    } else {
        currentTime++;
    }

    player.seekTo(currentTime, true);
}



function add_time_manage(videoId, number) {
    var container = $(".time-input-container[video-id='" + videoId + "'].number-" + number);
    container.find('.add-time-section').hide();
    container.find('.delete-time-section').show();
    $(timeInputTemplateHtmlPartial(videoId, undefined, undefined, number + 1, false)).insertAfter(container);
    addInputTimeFocusListener();
    addCategoryAutocomplete(videoId, number+1);
}

function delete_time_manage(videoId, number) {
    var allContainers = $(".time-input-container[video-id='" + videoId + "']"),
        prev_number = number - 1;

    if (allContainers.length > 1){
        if (number === 0){
            prev_number = number + 1;
        }
    } else {
        return;
    }

    var container = $(".time-input-container[video-id='" + videoId + "'].number-" + number),
        prev_container = $(".time-input-container[video-id='" + videoId + "'].number-" + prev_number),
        from = container.find('.vt_from').val(),
        to = container.find('.vt_to').val(),
        cat = container.find('.category-preload').val();

    $.ajax({
        url: '/cuts',
        type: 'delete',
        data: JSON.stringify({"video_id": videoId, "from":from, 'to':to, 'category':cat}),
        contentType: 'application/json',
        dataType: 'json'
    });

    container.remove();
    prev_container.find('.add-time-section').show();
    prev_container.find('.delete-time-section').hide();
}

function addInputTimeFocusListener() {
    var timeInput = $('.not-listened >> .time-input');
    timeInput.focus(function () {
        var videoId = $(this).attr("video-id"),
            player = players[videoId];
        $(this).val(formatTime(player.getCurrentTime()));
    });

    $('.time-manage-container').removeClass('not-listened');
}

function addCategoryAutocomplete(videoId, number){
    $('.category-preload[video-id=' + videoId + '].number-' + number).autocomplete({
        source: all_categories,
        minLength: 0,
        delay: 10,
        autoFocus: true,
        classes: {
            "ui-autocomplete": "main-autocomplete"
        }
    });

}



function initModal(videoId) {
    var modal = $('#modal-video-placeholder-' + videoId);

    players[videoId] = createPlayer(videoId);

    $.ajax({
        url: '/categories/all',
        success: function (data) {
            if (data.ok) {
                all_categories = data.categories;
                addCategoryAutocomplete(videoId, 0);
                addInputTimeFocusListener();
                modal.modal('show');
                modal.on('hidden.bs.modal', function (e) {
                    players[videoId].stopVideo();
                })
            } else {
                console.log("can not load categories in autocomplete because: ", data);
            }
        }
    });
}
