 function send_last_pleas(compilation_id) {
        var stateContainer = $(".compilation-state[compilation-id=" + compilation_id + "]");
        $.ajax({
            url: '/compilation/send_last_pleas/' + compilation_id,
            type:'post',
            success: function (data) {
                if (data.ok) {
                    stateContainer.text(stateContainer.text() + " last please sent;");
                }else{
                    stateContainer.text(stateContainer.text() + data.error);
                }
            },
            error: function () {
                stateContainer.text("Error please try again");
            }
        })
    }

    function upload(compilation_id) {
        var stateContainer = $(".compilation-state[compilation-id=" + compilation_id + "]");
        $.ajax({
            url: '/compilation/upload/' + compilation_id,
            type:'post',
            success: function (data) {
                if (data.ok) {
                    stateContainer.text(stateContainer.text() + " uploaded;");
                } else{
                    stateContainer.text(stateContainer.text() + data.error);
                }
            },
            error: function () {
                stateContainer.text("Error please try again");
            }
        })
    }

    function concatenate(compilation_id) {
        var stateContainer = $(".compilation-state[compilation-id=" + compilation_id + "]");
        $.ajax({
            url: '/compilation/concatenate/' + compilation_id,
            type:'post',
            success: function (data) {
                if (data.ok) {
                    stateContainer.text(stateContainer.text() + " concatenated;");
                }else{
                    stateContainer.text(stateContainer.text() + data.error);
                }
            },
            error: function () {
                stateContainer.text("Error please try again");
            }
        })
    }

    function send_to_sniffer(compilation_id) {
        var stateContainer = $(".compilation-state[compilation-id=" + compilation_id + "]");
        var forChannel = $("#ca-"+compilation_id).val().split('± ');

        $.ajax({
            url: '/compilation/send_to_sniffer/' + compilation_id,
            type:'post',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify({'for_channel':forChannel}),
            success: function (data) {
                if (data.ok) {
                    stateContainer.text(stateContainer.text() + "Sended! "+data.result+";");
                } else{
                    stateContainer.text(stateContainer.text() + " error: "+data.error+";");
                }
            },
            error: function () {
                stateContainer.text("Error please try again");
            }
        })
    }

    function delete_compilation(compilation_id) {
        var compilationContainer = $("#" + compilation_id);
        var stateContainer = $(".compilation-state[compilation-id=" + compilation_id + "]");
        $.ajax({
            url: '/compilation/delete/' + compilation_id,
            type:'post',
            success: function (data) {
                if (data.ok) {
                    compilationContainer.addClass('more-opacity');
                }
            },
            error: function () {
                stateContainer.text("Error please try again");
            }
        })
    }