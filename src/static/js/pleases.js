/**
 * Created by alesha on 20.01.17.
 */

const pCInnerTemplate = `
<div class="container-fluid collapse in" id="pleas-dialog-<%=videoId%>">
    <div class="row">
        <div class="col-md-10">
            <% _.each(dialog, function(phrase) { %>
            <div class="row dialog">
                <div class="col-md-2">
                    <a href="https://www.youtube.com/channel/<%= phrase.owner_channel_id %>"><%= phrase.owner_name %></a>
                </div>
                <div class="col-md-7 reply-text">
                    <%= phrase.text %>
                </div>
                <div class="col-md-1">
                    <%= tst_to_date(phrase.time) %>
                </div>
            </div>
            <hr>
           
            <% }) %>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-10">
            <div class="form-group">
                <div class="col-sm-11">
                    <input class="form-control pleas-text pleas-autocomplete" video-id="<%=videoId%>"
                           placeholder="Твой ответ на эту вакханалию...">
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-sm btn-info" onclick="send_pleas('<%=videoId%>')">
                        <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                    </button>
                </div>
            
            </div>
        </div>
        <div class="col-md-2">
            <button class="btn btn-sm btn-success" data-toggle="collapse"
                    data-target="#annotation-<%=videoId%>">
            <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
            </button>
           
             <% if (typeof(good) === "undefined")  { %>
            <button class="btn btn-sm good" id="good-btn-<%=videoId%>" onclick="setVideoIsGood('<%=videoId%>', false)"
            data-toggle="tooltip" data-placement="left" title="Добавить в хорошие (добавляется и канал этого видео в хорошие...)"
            >
            
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
            </button>
            <% } else { %>
            <button class="btn btn-sm btn-info good" id="good-btn-<%=videoId%>" onclick="setVideoIsGood('<%=videoId%>', true)"
            data-toggle="tooltip" data-placement="left" title="Убрать из хороших (канал из хороших не убирается)"
            >
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
            </button>
             <% } %>
            
        </div>
    </div>
    <div class="row collapse annotation" id="annotation-<%=videoId%>">
        <div class="col-md-10">
            <div class="form-group">
                <div class="col-sm-11">
                    <textarea rows="2" class="form-control annotation" video-id="<%=videoId%>"
                              placeholder="Напиши там че-почем..."></textarea>
                </div>
                
                <div class="col-sm-1">
                    <button class="btn btn-sm btn-warning annotation-btn" video-id="<%=videoId%>"
                        onclick="send_annotation('<%=videoId%>')">
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    </button>
                 </div>
            </div>
           
         </div>
        <br>
    </div>
    <br>
    <hr>
    <br>
</div>
`;


function tst_to_date(timeStamp) {
    // Create a new JavaScript Date object based on the timestamp
// multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date = new Date(timeStamp * 1000);
// Hours part from the timestamp
    var hours = date.getHours();
// Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
// Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();
    var month = '0' + (date.getMonth() + 1);


// Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) + ' (' + date.getDate() + '.' + month.substr(-2) + '.' + date.getFullYear() + ')';
    return formattedTime;
}

function send_annotation(video_id) {
    var annotationText = $('.annotation[video-id=' + video_id + ']').val();

    $.ajax({
        url: '/pleas/add_annotation',
        type: 'post',
        data: JSON.stringify({"video_id": video_id, "annotation": annotationText}),
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data.ok) {
                var annotationBtn = $('.annotation-btn[video-id=' + video_id + ']');
                annotationBtn.removeClass('btn-warning');
                annotationBtn.addClass('btn-info');
            }
        }
    })
}

function send_pleas(videoId) {
    var text = $(".pleas-text[video-id=" + videoId + "]").val(),
        container = $(".pleas_container[video-id=" + videoId + "]");

    container.addClass("more-opacity");

    $.ajax({
        url: '/pleas/send_response',
        type: "post",
        data: JSON.stringify({"video_id": videoId, "text": text}),
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data.ok) {
                container.removeClass("more-opacity");
                container.addClass("used");
            }
        }
    })
}

function setPleaseAutocomplete(videoId) {
    $('.pleas-autocomplete[video-id="' + videoId + '"]').tokenfield({
        autocomplete: {
            source: function (request, response) {
                jQuery.get("/please_replies", {
                    query: request.term
                }, function (data) {
                    response($.map(data.result, function (item) {
                        return {
                            label: item,
                            value: item
                        }
                    }));
                })
            }
        },
        showAutocompleteOnFocus: true,
        delimiter: '±',
        createTokensOnBlur: true
    });

}
function pleas_news(e) {
    var videoId = $(this).attr('video-id'),
        dialogContainer = $('#pleas-dialog-' + videoId);

    if (dialogContainer.length > 0) {
        dialogContainer.collapse("toggle");
    } else {
        var button = $(this),
            container = $('.pleas-container[video-id="' + videoId + '"]').parent();

        container.addClass("more-opacity");
        $.ajax({
            url: '/pleas/news',
            type: "post",
            data: JSON.stringify({"video_id": videoId}),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                container.removeClass("more-opacity");
                if (data.ok) {

                    var t = _.template(pCInnerTemplate);
                    container.append(t({
                        dialog: data.pleas.dialog,
                        videoId: videoId,
                        tst_to_date: tst_to_date,
                        good: data.video['good']
                    }));
                    setPleaseAutocomplete(videoId);

                    button.attr('data-toggle', 'collapse');
                    button.attr('data-target', '#pleas-dialog-' + videoId);
                    $("#pleas-dialog-" + videoId).collapse({
                        toggle: false
                    });
                }
            }
        })
    }
}

$('.pleas-info-anchor').on('click', pleas_news);

function add_please_news_listener(videoId) {
    console.log("add please news listener: ", videoId);
    $('.pleas-info-anchor[video-id="' + videoId + '"]').on('click', pleas_news)
}
