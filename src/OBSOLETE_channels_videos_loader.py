import logging
from time import sleep

from src import init_log, VT_COMPILATION
from src.core import load_and_store_channel_videos
from src.db import VC_Storage

store = VC_Storage()
log = logging.getLogger("channels_video_loader")

if __name__ == '__main__':
    init_log("channels_video_loader", without_yt=True)
    while 1:
        sleep(60 * 2)
        channels = store.get_channels(is_rejected=None, is_deleted=False, channel_type=VT_COMPILATION)
        log.info("Start uploading new videos from channels:\n%s" % \
                 ("\n".join(["[%s] %s" % (x.get("channel_id"), x.get("title")) for x in channels]))
                 )

        for channel in channels:
            channel_id = channel.get("channel_id")
            last_channel_video_id = channel.get("last_video_id")
            try:
                new_channel_videos = load_and_store_channel_videos(channel_id, last_channel_video_id)
            except Exception as e:
                log.exception(e)
                log.error("Can not load new compilations of %s" % channel)
                continue

        sleep(60 * 60)
