import logging
import uuid

import time
from collections import defaultdict

import pymongo

from bson import ObjectId

from rr_lib.db import DBHandler
from src import VT_COMPILATION, PS_ASKED, config, VT_SOURCE, VIDEO_ID_LENGTH, CHANNEL_ID_LENGTH, PS_APPROVED

log = logging.getLogger("DB")


class CollectionInitter(DBHandler):
    def init_collection(self, collection_name, indexes=None, **kwargs):
        if collection_name not in self.collection_names:
            collection = self.db.create_collection(collection_name, **kwargs)
            if indexes:
                for index in indexes:
                    self.db[collection_name].create_index(index)
        else:
            collection = self.db.get_collection(collection_name)

        setattr(self, collection_name, collection)


class VC_Storage(DBHandler):
    def __init__(self, connection_name="?", name="main", ensure_indexes=False):
        super(VC_Storage, self).__init__(connection_name=connection_name, name=name)
        if "videos" not in self.collection_names or ensure_indexes:
            try:
                self.videos = self.db.create_collection("videos")
            except:
                self.videos = self.db.get_collection("videos")
                self.videos.drop_indexes()

            self.videos.create_index("video_id", unique=True)
            self.videos.create_index("status")
            self.videos.create_index("type")
            self.videos.create_index("channel_id")
            self.videos.create_index("compilation_id", sparse=True)
            self.videos.create_index("exclude", sparse=True)
            self.videos.create_index("deleted", sparse=True)
            self.videos.create_index("in_compilator", sparse=True)
            self.videos.create_index("good", sparse=True)
            self.videos.create_index("have_cuts", sparse=True)
        else:
            self.videos = self.db.get_collection("videos")

        if "compilations" not in self.collection_names or ensure_indexes:
            try:
                self.compilations = self.db.create_collection("compilations")
            except:
                self.compilations = self.db.get_collection("compilations")
                self.compilations.drop_indexes()

            self.compilations.create_index("state")

        else:
            self.compilations = self.db.get_collection("compilations")

        if "channels" not in self.collection_names or ensure_indexes:
            try:
                self.channels = self.db.create_collection("channels")
            except:
                self.channels = self.db.get_collection("channels")
                self.channels.drop_indexes()

            self.channels.create_index("channel_id", unique=True)
            self.channels.create_index("type")
            self.channels.create_index("pleas_reject", sparse=True)
            self.channels.create_index("good", sparse=True)
            self.channels.create_index("is_tracked", sparse=True)
        else:
            self.channels = self.db.get_collection("channels")

        if "oauth" not in self.collection_names or ensure_indexes:
            try:
                self.oauth = self.db.create_collection("oauth")
            except:
                self.oauth = self.db.get_collection("oauth")
        else:
            self.oauth = self.db.get_collection("oauth")

        if 'pleases' not in self.collection_names or ensure_indexes:
            try:
                self.pleases = self.db.create_collection("pleases")
            except:
                self.pleases = self.db.get_collection("pleases")
                self.pleases.drop_indexes()

            self.pleases.create_index("video_id", unique=True)
            self.pleases.create_index("state")
            self.pleases.create_index("send")
        else:
            self.pleases = self.db.get_collection("pleases")

        if "pleas_templates" not in self.collection_names or ensure_indexes:
            try:
                self.pleas_templates = self.db.create_collection("pleas_templates")
            except:
                self.pleas_templates = self.db.get_collection("pleas_templates")
                self.pleas_templates.drop_indexes()

            self.pleas_templates.create_index("pt_id", unique=True)
            self.pleas_templates.create_index("finish")
            self.pleas_templates.create_index("add-video")
        else:
            self.pleas_templates = self.db.get_collection("pleas_templates")

        if "pleas_replies" not in self.collection_names or ensure_indexes:
            try:
                self.pleas_replies = self.db.create_collection("pleas_replies")
            except:
                self.pleas_replies = self.db.get_collection("pleas_replies")
                self.pleas_replies.drop_indexes()

            self.pleas_replies.create_index("pr_id", unique=True)
        else:
            self.pleas_replies = self.db.get_collection("pleas_replies")

        if "copyrights" not in self.collection_names or ensure_indexes:
            try:
                self.copyrights = self.db.create_collection("copyrights")
            except:
                self.copyrights = self.db.get_collection("copyrights")
                self.copyrights.drop_indexes()

            self.copyrights.create_index("name", unique=True)
        else:
            self.copyrights = self.db.get_collection("copyrights")

        if "etc" not in self.collection_names or ensure_indexes:
            try:
                self.etc = self.db.create_collection("etc", capped=True, size=1024 * 1024 * 100)
            except:
                self.etc = self.db.get_collection("etc")
        else:
            self.etc = self.db.get_collection("etc")

    # channels

    def store_channel(self, channel_id, type, data):
        upd = dict({"channel_id": channel_id, "type": type}, **data)
        return self.channels.update_one({"channel_id": channel_id}, {"$set": upd}, upsert=True)

    def set_last_channel_video(self, channel_id, video_id):
        return self.channels.update_one({"channel_id": channel_id}, {"$set": {"last_video_id": video_id}})

    def get_channels(self, channel_type=VT_COMPILATION, is_deleted=False, is_rejected=False, projection=None):
        if projection is None:
            projection = {"_id": 0}

        q = {}
        if channel_type:
            q['type'] = channel_type

        if is_deleted is not None:
            if not is_deleted:
                q['deleted'] = {"$exists": False}
            else:
                q['deleted'] = True

        if is_rejected is not None:
            if not is_rejected:
                q['pleas_reject'] = {"$exists": False}
            else:
                q['pleas_reject'] = True

        log.info("Getting channels q: %s" % q)

        return list(self.channels.find(q, projection=projection))

    def get_channel(self, channel_id):
        return self.channels.find_one({"channel_id": channel_id}, projection={"_id": False})

    def set_channel_pleas_reject(self, channel_id, yes=True):
        if not yes:
            upd = {"$unset": {"pleas_reject": yes}}
        else:
            upd = {"$set": {"pleas_reject": yes}}
        return self.channels.update_one({"channel_id": channel_id}, upd, upsert=True)

    def set_channel_good(self, channel_id):
        return self.channels.update_one({"channel_id": channel_id}, {"$set": {"good": True}}, upsert=True)

    def get_channels_pleas_reject(self, channel_ids=None, projection=None):
        q = {"pleas_reject": {"$exists": True}}
        if channel_ids and isinstance(channel_ids, list):
            q['channel_id'] = {"$in": channel_ids}
        result = self.channels.find(q, projection=projection)
        return list(result)

    def is_channel_rejected(self, channel_id):
        found = self.channels.find_one({"channel_id": channel_id, "pleas_reject": {"$exists": True}})
        return found is not None

    def delete_channel(self, channel_id):
        return self.channels.update_one({"channel_id": channel_id}, {"$set": {"deleted": True}}, upsert=True)

    def restore_channel(self, channel_id):
        return self.channels.update_one({"channel_id": channel_id}, {"$unset": {"deleted": True}}, upsert=True)

    def set_channel_tracking(self, channel_id, is_die=False, track=True):
        to_set = {"is_tracked": track}
        if is_die:
            to_set['is_die'] = is_die

        return self.channels.update_one({"channel_id": channel_id}, {"$set": to_set}, upsert=True)

    def get_channels_tracked(self, is_die=False, track=True, projection=None):
        q = {"is_tracked": track}
        if is_die:
            q["is_die"] = is_die
        return list(self.channels.find(q, projection=projection))

    # oauth

    def set_my_channel_info(self, channel_info):
        self.oauth.update_one({}, {"$set": channel_info}, upsert=True)

    def get_my_channel_info(self):
        found = self.oauth.find_one({}, projection={"_id": False})
        return found

    # videos

    def store_video(self, video_data, type):
        video_data['type'] = type
        to_set = {"$set": video_data}
        return self.videos.update_one({"video_id": video_data['video_id']}, to_set, upsert=True)

    def update_video_type(self, video_id, new_type):
        return self.videos.update_one({'video_id': video_id}, {'$set': {'type': new_type}})

    def get_videos_with_cuts(self, category):
        videos = list(self.videos.find(
            {"have_cuts": True, "in_compilator": True, 'cut_times': {"$elemMatch": {"category": category}}},
            projection={"_id": False}))
        video_numbers = defaultdict(int)
        cuts = []
        for video in videos:
            video_id = video.get('video_id')
            for cut in video.get('cut_times'):
                if cut.get('category') == category:
                    cuts.append(dict({
                        "to_s": cut['to'],
                        'from_s': cut['from'],
                        'length_time': cut['to'] - cut['from'],
                        'rate': int(cut.get('rate', -1) or -1),
                    },
                        **video))

                    video_numbers[video_id] += 1

        return self._filter_pleases_state(cuts)

    def is_cut_exists(self, video_id, from_time, to_time):
        return self.videos.find_one(
            {"$and": [{"video_id": video_id}, {'cut_times': {"$elemMatch": {'from': from_time, 'to': to_time}}}]})

    def store_video_cut(self, video_data, from_time, to_time, category, rate):
        self.videos.update_one({"video_id": video_data['video_id']},
                               {"$set": dict({"have_cuts": True, "in_compilator": True}, **video_data),
                                "$addToSet": {
                                    "cut_times": {'from': from_time, 'to': to_time, 'category': category, 'rate': rate},
                                    "categories": category}},
                               upsert=True)

    def delete_video_cut(self, video_id, from_time, to_time):
        self.videos.update_one({"video_id": video_id},
                               {"$pull": {'cut_times': {'$and': [{'from': from_time}, {'to': to_time}]}}})

    def update_video_cut(self, video_id, from_time, to_time, category, rate):
        self.delete_video_cut(video_id, from_time, to_time)
        self.videos.update_one({"video_id": video_id},
                               {"$addToSet": {'cut_times': {'from': from_time, 'to': to_time, 'category': category,
                                                            'rate': rate}}})

    def set_compilation_id(self, video_ids, compilation_video_id):
        self.videos.update_many({"video_id": {"$in": video_ids}},
                                {"$addToSet": {"compilation_id": compilation_video_id}})

    def get_all_categories(self):
        return map(lambda x: x.get("_id"),
                   self.videos.aggregate([{"$match": {"exclude": {"$exists": False},
                                                      "deleted": {"$exists": False},
                                                      "categories": {"$exists": True}}},
                                          {"$unwind": "$categories"},
                                          {"$group": {"_id": "$categories"}}]
                                         ))

    def set_video_compilation_categories(self, video_compilation_id, categories):
        comp_result = self.videos.update_one({"video_id": video_compilation_id},
                                             {"$addToSet": {"categories": {"$each": categories}}})
        source_result = self.videos.update_many({"compilation_id": {"$elemMatch": {"$eq": video_compilation_id}}},
                                                {"$addToSet": {"categories": {"$each": categories}}})
        log.info("Set video category: \n%s \n%s" % (comp_result, source_result))

    def add_video_source_categories(self, video_id, categories):
        if isinstance(video_id, list):
            q = {'video_id': {"$in": video_id}}
        else:
            q = {"video_id": video_id}
        self.videos.update_many(q, {"$addToSet": {"categories": {"$each": categories}}})

    def change_videos_sources_categories(self, video_ids, new_categories):
        self.videos.update_many({"video_id": {"$in": video_ids}}, {"$set": {"categories": new_categories}})

    def remove_videos_from_compilator(self, video_ids, our_compilation_id=None):
        upd = {"$set": {"in_compilator": False}}
        if our_compilation_id:
            upd["$addToSet"] = {"our_compilations": our_compilation_id}

        self.videos.update_many({"video_id": {"$in": video_ids}}, upd)

    def move_videos(self, video_ids, to_compilator=True):
        return self.videos.update_many({"video_id": {"$in": video_ids}}, {"$set": {"in_compilator": to_compilator}})

    def delete_video(self, video_id, additional_attributes=None, real=False):
        log.info("DELETE VIDEO %s %s" % (video_id, "!!REAL!!" if real else ""))
        if not additional_attributes:
            additional_attributes = {}

        if isinstance(video_id, list):
            q = {'video_id': {"$in": video_id}}
        else:
            q = {"video_id": video_id}

        if real:
            return self.videos.delete_many(q)
        else:
            return self.videos.update_many(
                q,
                {"$set": dict({"deleted": True}, **additional_attributes)},
                upsert=True)

    def get_video(self, video_id):
        return self.videos.find_one({"video_id": video_id}, projection={"_id": False})

    def get_videos(self, video_ids):
        return list(self.videos.find({"video_id": {"$in": video_ids}}))

    def get_video_compilations_without_categories(self, channel_id=None):
        q = {"type": VT_COMPILATION, "deleted": {"$exists": False}, "categories": {"$exists": False}}
        if channel_id:
            q['channel_id'] = channel_id

        return self.videos.find(q)

    def get_videos_by_channel_id(self, channel_id):
        return list(self.videos.find({"channel_id": channel_id}))

    def get_source_videos(self, category=None, pleas_state=None, in_compilator=True, as_list=True):
        q = {
            "type": VT_SOURCE,
            "exclude": {"$exists": False},
            "deleted": {"$exists": False},
            "have_cuts": {"$exists": False}
        }
        if category:
            q["categories"] = {"$elemMatch": {"$eq": category}}

        if in_compilator is not None:
            if in_compilator:
                q["$or"] = [{"in_compilator": {"$exists": False}}, {"in_compilator": True}]
            else:
                q["in_compilator"] = False

        videos = self.videos.find(q, projection={"_id": False})

        if as_list:
            videos = list(videos)

        if not pleas_state:
            return videos

        if videos:
            return self._filter_pleases_state(videos, pleas_state)

    def _filter_pleases_state(self, videos, pleas_state=PS_APPROVED):
        video_ids = map(lambda x: x.get("video_id"), videos)
        pleases = dict(map(lambda x: (x.get("video_id"), x.get("state")),
                           self.pleases.find({"video_id": {"$in": video_ids}},
                                             projection={"video_id": True, "state": True})))
        return filter(
            lambda x: (x.get('video_id') in pleases and pleases.get(
                x.get("video_id")) == pleas_state) or not pleas_state,
            videos
        )

    def get_videos_of_compilation(self, compilation_video_id):
        return list(self.videos.find(
            {"type": VT_SOURCE,
             "compilation_id": {"$elemMatch": {"$eq": compilation_video_id}},
             "deleted": {"$exists": False},
             "exclude": {"$exists": False}})
        )

    def set_video_exclude(self, exclude_video_id):
        self.videos.update_one({"video_id": exclude_video_id}, {"$set": {"exclude": True}}, upsert=True)

    def get_videos_exclude(self):
        result = self.videos.find({"exclude": {"$exists": True}})
        return list(result)

    def set_entity_good(self, video_id, not_=False):
        if not_:
            upd = {"$unset": {"good": True}}
        else:
            upd = {"$set": {"good": True}}

        if len(video_id) == VIDEO_ID_LENGTH:
            q = {"video_id": video_id}
            coll = self.videos
        elif len(video_id) == CHANNEL_ID_LENGTH:
            q = {"channel_id": video_id}
            coll = self.channels
        else:
            return None

        return coll.update_one(q, upd, upsert=True)

    def get_videos_good(self):
        q = {"good": True}
        return list(self.videos.find(q)), list(self.channels.find(q))

    def is_source_exclude(self, video_id):
        found = self.videos.find_one({"video_id": video_id, "$or": [{"exclude": True}, {"BAD": {"$exists": True}}]})
        return found is not None

    # compilations

    def store_compilation(self, title, video_cuts, description):
        dt = time.time()
        result = self.compilations.insert_one(
            {"title": title, "cuts": video_cuts, "time": dt, "description": description})
        return str(result.inserted_id)

    def update_compilation(self, compilation_id, to_update):
        to_update['last_update'] = time.time()
        log.info('UPDATE COMPILATION: %s\n%s' % (compilation_id, to_update))
        return self.compilations.update_one({"_id": ObjectId(compilation_id)}, {"$set": to_update}, upsert=True)

    def set_compilation_state(self, compilation_id, state, error=None, state_data=None):
        upd = {state: {"ok": True, "time": time.time()}}
        if error:
            upd[state]['error'] = error
            upd[state]['ok'] = False
        else:
            upd["state"] = state

        if state_data and isinstance(state_data, dict):
            upd = dict(upd, **state_data)
        return self.update_compilation(compilation_id, upd)

    def set_compilation_task(self, compilation_id, task):
        q = {"_id": ObjectId(compilation_id)}
        c = self.compilations.find_one(q)
        if 'task' not in c:
            task_obj = {'name': task, 'at': time.time()}
            return self.compilations.update_one({"_id": ObjectId(compilation_id)},
                                                {'$set': {'task': task_obj}})

    def get_compilations_with_tasks(self):
        agg_result = self.compilations.aggregate([{'$match': {'task': {'$exists': True}}},
                                                  {'$sort': {'task.at': -1}},
                                                  {'$group': {'_id': '$task.name',
                                                              'comp_ids': {'$push': '$_id'}}}])
        for el in agg_result:
            task, ids = el.get('_id'), el.get('comp_ids')
            for id in ids:
                yield task, self.compilations.find_one({'_id': ObjectId(id)})

    def get_compilations_task_count(self):
        return self.compilations.count({'task': {'$exists': True}})

    def imply_compilation_task(self, compilation_id, end=False, result=None):
        q = {'_id': ObjectId(compilation_id)}
        if end:
            self.compilations.update_one(q, {'$set': {'task_proc.end': time.time(), 'task_proc.result': result}})
            return
        try:
            compilation = self.compilations.find_one(q)
            self.compilations.update_one(q, {'$unset': {'task': 1}})
            task = {'name': compilation.get('task').get('name'), 'at': time.time()}
            self.compilations.update_one(q, {'$set': {'task_proc': task}})
        except Exception as e:
            log.exception(e)

    def get_compilation(self, compilation_id, fill_videos=False):
        found = self.compilations.find_one({"_id": ObjectId(compilation_id)})
        if found:
            if not found.get("compilation_id"):
                found['compilation_id'] = str(found.pop("_id"))

            if fill_videos:
                videos = list(self.videos.find(
                    {"video_id": {"$in": map(lambda x: x.get("video_id"), found.get("cuts"))}},
                    projection={"_id": False}))
                videos = dict(map(lambda x: (x.get("video_id"), x), videos))
                for i, cut in enumerate(found.get("cuts")):
                    if cut.get("video_id") in videos:
                        found['cuts'][i] = dict(cut, **videos[cut.get("video_id")])

        return found

    def delete_compilation(self, compilation_id):
        return self.compilations.delete_one({"_id": ObjectId(compilation_id)})

    def get_compilation_names(self, compilation_ids):
        result = self.compilations.find({"_id": {"$in": [ObjectId(x) for x in compilation_ids]}},
                                        projection={"title": True})
        return map(lambda x: x.get('title'), result)

    def get_all_compilations(self, q=None, limit=100, skip=0):
        if q is None:
            q = {}
        result = list(self.compilations.find(q).sort([('time', -1),]).skip(skip).limit(limit))
        for i, compilation in enumerate(result):
            result[i]['compilation_id'] = str(compilation.pop("_id"))
        return result

    # pleases
    def store_pleas(self, video_id, text, pt_id, channel_id):
        found = self.pleases.find_one({"video_id": video_id})
        if found:
            return None, found

        to_insert = {"state": PS_ASKED,
                     "video_id": video_id,
                     "text": text,
                     "comment_id": None,
                     "pt_id": pt_id,
                     "created": time.time(),
                     "send": False,
                     "channel_id": channel_id
                     }
        return self.pleases.insert_one(to_insert), to_insert

    def set_pleas_state(self, video_id, new_state):
        return self.pleases.update_one({"video_id": video_id}, {"$set": {"state": new_state, "updated": time.time()}},
                                       upsert=True)

    def get_please_not_sended(self):
        for pleas in self.pleases.find({"send": False}):
            yield pleas

    def update_please_send_state(self, video_id, send=True, next_text=None, comment_id=None, error=None):
        u = {"send": send, "updated": time.time()}
        if next_text:
            u['next_text'] = next_text
        if comment_id:
            u['comment_id'] = comment_id
        if error:
            u['send_error'] = error
        log.info("UPDATE PLEASE STATE %s\n%s" % (video_id, u))
        self.pleases.update_one({"video_id": video_id}, {"$set": u}, upsert=True)

    def set_pleas_resended(self, video_id):
        self.pleases.update_one({"video_id": video_id}, {"$set": {"resended": time.time()}})

    def set_pleas_annotation(self, video_id, annotation_text):
        return self.pleases.update_one({"video_id": video_id}, {"$set": {"annotation": annotation_text}})

    def set_pleas_dialog_step(self, video_id, dialog_phrase_data):
        return self.pleases.update_one({"video_id": video_id},
                                       {"$push": {"dialog": dialog_phrase_data}})

    def set_pleas_read(self, video_id, pr_id):
        self.pleases.update_one({"video_id": video_id, "dialog.pr_id": pr_id}, {"$unset": {"dialog.$.not_read": True}})

    def get_pleas(self, video_id):
        return self.pleases.find_one({"video_id": video_id}, projection={"_id": False})

    def get_pleases(self, video_ids):
        return list(self.pleases.find({"video_id": {"$in": video_ids}}))

    def get_all_pleases(self, except_states=None, projection=None):
        q = {}
        if except_states:
            if isinstance(except_states, list):
                q["state"] = {"$nin": except_states}
            else:
                q['state'] = {'$ne': except_states}
        return list(self.pleases.find(q, projection=projection))

    def get_pleases_with_state(self, pleas_state, with_dialog=True, skip=None, limit=None):
        q = {"state": pleas_state, }
        if with_dialog is not None:
            q["dialog"] = {"$exists": with_dialog}

        cursor = self.pleases.find(q)
        if skip:
            cursor.skip(skip)
        if limit:
            cursor.limit(limit)

        return list(cursor.sort("created", pymongo.DESCENDING))

    def get_pleases_with_state_count(self, pleas_state, with_dialog=True, ):
        q = {"state": pleas_state, "dialog": {"$exists": with_dialog}}
        return self.pleases.count(q)

    def delete_pleas(self, video_id):
        return self.pleases.delete_one({"video_id": video_id})

    # please templates

    def store_pleas_template(self, content, pt_params=None):
        if pt_params is None:
            pt_params = {}
        pt_id = str(uuid.uuid3(uuid.NAMESPACE_DNS, content.encode("utf-8")))
        found = self.pleas_templates.find_one({"pt_id": pt_id})
        if not found:
            self.pleas_templates.insert_one(dict({"pt_id": pt_id, "content": content}, **pt_params))
            return pt_id

    def remove_pleas_template(self, pt_id):
        result = self.pleas_templates.delete_one({"pt_id": pt_id})
        return result.deleted_count

    def get_pleas_templates(self, finish=None, add_video=None):
        q = {}

        if finish == True:
            q['finish'] = True
        elif finish == False:
            q['$or'] = [{"finish": False}, {"finish": {"$exists": False}}]

        if add_video == True:
            q['add_video'] = True
        elif add_video == False:
            av_or = [{"add_video": False}, {"add_video": {"$exists": False}}]
            if '$or' in q:
                q['$and'] = [{'$or': q.pop('$or')}, {'$or': av_or}]
            else:
                q['$or'] = av_or

        result = list(self.pleas_templates.find(q, projection={"_id": False}))
        return result

    def get_pleas_template(self, pt_id):
        return self.pleas_templates.find_one({"pt_id": pt_id})

    # pleas replies

    def store_pleas_reply(self, pr_id, reply_text):
        self.pleas_replies.update_one({"pr_id": pr_id}, {"$set": {"text": reply_text}}, upsert=True)

    def get_all_pleas_replies(self):
        result = self.pleas_replies.find({}, projection={"text": True})
        return map(lambda x: x.get("text"), result)

    # copyrights
    def get_copyrights(self):
        return map(lambda x: x.get('name'), self.copyrights.find({}, projection={"name": True}))

    def set_copyrights(self, copyrights):
        stored = self.get_copyrights()
        diff = set(copyrights).difference(stored)
        for c in diff:
            self.copyrights.insert_one({"name": c})
        return diff

    # etc
    def add_bad_url(self, url):
        self.etc.insert_one({"_id": hash(url)})

    def is_url_bad(self, url):
        return self.etc.find_one({"_id": hash(url)})

    def get_stop_words(self):
        return (self.etc.find_one({"_id": "stop_words"}) or {}).get('data', [])

    def add_stop_words(self, new_words):
        return self.etc.update_one({"_id": "stop_words"}, {"$set": {"data": new_words}}, upsert=True)


class DataCache(DBHandler):
    def __init__(self, ensure_index=False):
        super(DataCache, self).__init__(name="main")
        if ensure_index:
            self.db.drop_collection('cache')

        if 'cache' not in self.collection_names:
            self.cache = self.db.create_collection('cache', capped=True, max=10000, size=1024 * 1024 * 1024 * 10)
            self.cache.create_index('video_id')
            self.cache.create_index('hash')
        else:
            self.cache = self.db.get_collection('cache')

    def set_data(self, hash, data, key='video_id'):
        if not self.cache.find_one({key: data.get(key), 'hash': hash}):
            self.cache.insert_one(dict({'hash': hash}, **data))

    def get(self, hash):
        return self.cache.find({"hash": hash}, projection={'_id': False})

    def get_data(self, key_value, key_name='video_id'):
        return self.cache.find_one({key_name: key_value})


if __name__ == '__main__':
    VC_Storage(name="ENSURE INDEXES", ensure_indexes=True)
    DataCache(ensure_index=True)
