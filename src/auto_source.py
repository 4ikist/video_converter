import logging
import re
from collections import Counter, defaultdict

from src.db import VC_Storage

splitter = re.compile("[\s]")
url_retriever = re.compile("https?\:\/\/((www\.youtube\.com\/watch\?v=)|(youtu\.be\/))(.{11})")
kitchen_time = re.compile("\d{1,2}\:\d{2}")

store = VC_Storage("auto_source")

log = logging.getLogger("auosource")

class ParseResult(object):
    def __init__(self, cuts=None, to_compilator=None, error=None):
        self.cuts = cuts
        self.to_compilator = to_compilator
        self.error = error

    def __repr__(self):
        return "cuts: %s\nto_compilator: %s\nERROR: %s" % (self.cuts, self.to_compilator, self.error)


def get_seconds_from_kitchen(kitchen):
    minutes, seconds = tuple(kitchen.split(':'))
    return int(minutes) * 60 + int(seconds)


def get_cuts_from_file(file_data):
    videos_counter = defaultdict(int)
    to_compilator = set()
    cuts = []

    for i, row in enumerate(file_data.split('\n'), 1):
        row = row.strip()
        if row:
            elements = filter(lambda x: x, splitter.split(row))
            if elements:
                try:
                    video_id = url_retriever.findall(elements[0])[0][3]
                except Exception as e:
                    return ParseResult(error="Bad row %s" % i)

                if not video_id:
                    return ParseResult(error="Can not read video_id in row %s" % i)

                videos_counter[video_id] += 1
                video = store.get_video(video_id)
                if not video:
                    return ParseResult(error="Can not find %s in db at row %s" % (video_id, i))

                if len(elements) == 1:
                    log.info("Add to compilator: %s"%video_id)
                    to_compilator.add(video_id)
                    continue

                if len(elements) == 3:
                    t_from = elements[1]
                    t_to = elements[2]

                    if kitchen_time.match(t_from):
                        t_from = get_seconds_from_kitchen(t_from)
                    else:
                        return ParseResult(error="Time START video is bad at row %s" % i)

                    if kitchen_time.match(t_to):
                        t_to = get_seconds_from_kitchen(t_to)
                    else:
                        return ParseResult(error="Time STOP video is bad at row %s" % i)

                    cut = {
                        "video_id": video_id,
                        "number": videos_counter[video_id] - 1,
                        "from_s": t_from,
                        "to_s": t_to,
                        "title": video.get('title'),
                        "channel_title": video.get('channel_title')
                    }

                    log.info("Found cut!\n%s"%cut)

                    cuts.append(cut)
    if cuts:
        return ParseResult(cuts=cuts, to_compilator=list(to_compilator))

    if to_compilator:
        return ParseResult(to_compilator=list(to_compilator))

    return ParseResult(error="No cuts find :(")


if __name__ == '__main__':
    file_data = '''

    https://www.youtube.com/watch?v=7xk2jrWnqW8

    https://www.youtube.com/watch?v=VS4IM38l3eE     4:12    4:55
    https://www.youtube.com/watch?v=VS4IM38l3eE     12:22 22:12

    https://www.youtube.com/watch?v=f5P2Ja4xsKk     12:22 21:12

    https://www.youtube.com/watch?v=mo9cqSWr72c     12:22 23:12

    '''
    print get_cuts_from_file(file_data)
