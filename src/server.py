# coding=utf-8
import json
import os

import requests
from bson import ObjectId
from flask import Flask
from flask import request
from flask import url_for
from flask.helpers import flash, send_from_directory
from flask.json import jsonify
from flask.templating import render_template
from werkzeug.utils import redirect

from src import PS_ASKED, PS_REJECTED, VPS, VT_SOURCE, PS_APPROVED, CAT_FROM_COMPILATOR, CAT_FROM_4LB, \
    VIDEO_ID_LENGTH, CS_UPLOADED, CS_CONCATENATED, CS_CREATED, CS_ERROR, \
    CS_PREPARED, init_log, VT_COMPILATION, IS_TEST, config, VT_COMPILATION_WITHOUT_SOURCES
from src.auto_source import get_cuts_from_file
from src.compilations import CONCATENATE, UPLOAD, LOAD_AND_CUT
from src.convert.concat import concatenate_filter_complex, concatenate_filter_concat
from src.convert.cut import compilation_file_name
from src.core import set_good, load_and_store_channel_videos, retrieve_and_store_related_videos, \
    load_and_cut_compilation_sources
from src.db import VC_Storage
from src.pleas import send_second_please, prepare_compilation_please, \
    load_and_store_new_pleas_replies, is_have_not_reads_replies, initialise_first_pleas, send_first_please, \
    prepare_add_video_please

from src.utils import tst_to_dt, array_to_string, Completer, CompleterDict
from src.views.channels_tracked import ChannelsTrackedAPI
from src.views.cuts import CutsApi
from src.views.exclude import ExcludeAPI
from src.views.find_videos import FindVideosApiView
from src.views.find_channels import FindChannelsApiView
from src.views.words_speed_view import WordsSpeedView, KeysCoreView
from src.yt import get_youtube, STORAGE_FILE
from src.yt.comments import delete_comment
from src.yt.info import get_video_info, get_my_channel_info, get_channel_with_videos

from src.yt.upload import upload_video

log = init_log("server", stdout=False)

cur_dir = os.path.dirname(__file__)
app = Flask("rr", template_folder=cur_dir + "/templates", static_folder=cur_dir + "/static")

app.secret_key = 'fooooooo'
app.config['SESSION_TYPE'] = 'filesystem'

store = VC_Storage("server")
yt = get_youtube()

app.jinja_env.filters["tst_to_dt"] = tst_to_dt
app.jinja_env.globals.update(array_to_string=array_to_string)


@app.route("/service-worker.js")
def service_worker():
    return send_from_directory(os.path.join(cur_dir, 'static'), 'service-worker.js')


@app.route("/")
def main():
    return redirect(url_for('pleases'))


@app.route("/compilator-react")
def compilator_react():
    return render_template("compilator-react.html")


@app.route("/compilator")
def compilator():
    # not_ended_compilations = store.get_all_compilations({"state": {"$in": [CS_CREATED, CS_PREPARED]}})
    # return render_template("compilator.html", **{"not_ended_compilations": not_ended_compilations})
    return redirect('/compilator-react')

@app.route("/compilator/remove_videos", methods=["POST"])
def remove_from_compilator():
    data = json.loads(request.data)
    store.remove_videos_from_compilator(data.get("video_ids"))
    return jsonify(**{"ok": True})


@app.route("/categories/all")
def all_categories():
    categories = store.get_all_categories()
    category_completer.options = set(categories)
    return jsonify(**{"ok": True, "categories": categories})


channels_completer = CompleterDict(
    dict(
        map(
            lambda x: (x.get('channel_id'), x.get('title')),
            filter(
                lambda x: x.get('title') is not None,
                store.get_channels(channel_type=VT_COMPILATION, is_rejected=None, is_deleted=False)
            )
        )
    )
)


@app.route("/channels/autocomplete")
def channels_autocomplete():
    query = request.args.get("query")
    result = channels_completer.complete(query)
    return jsonify(**{"ok": True, "result": result})


@app.route("/compilator/data/<category>")
def compilator_data(category):
    if category in category_completer.options:
        category_videos = store.get_source_videos(category=category, pleas_state=PS_APPROVED) or []
        category_cuts = store.get_videos_with_cuts(category) or []
        return jsonify({"ok": True, "related_videos": category_videos, "related_cuts": category_cuts})

    if len(category) == VIDEO_ID_LENGTH:
        video_id = category
        video = get_video_info(video_id)
        if video:
            store.store_video(video, VT_SOURCE)
            store.add_video_source_categories(video_id, [CAT_FROM_COMPILATOR])
            category_completer.options.add(CAT_FROM_COMPILATOR)
            store.set_pleas_state(video_id, PS_APPROVED)
            return jsonify(**{"ok": True, "related_videos": [video]})
        return jsonify(**{"ok": False, "error": "bad video source :("})

    return jsonify(**{"ok": False, "error": "no category or video_id :( "})


@app.route("/videos")
def videos():
    in_compilator = request.args.get('in_compilator') or False
    categories = store.get_all_categories()
    video_id = request.args.get("id")
    if video_id:
        video = store.get_video(video_id)
        if video:
            return render_template("videos.html", videos=[video], categories=categories, in_compilator=in_compilator)

    category = request.args.get('category')
    if not category and request.args.get("categories"):
        category = request.args.get("categories").split(",")

    if in_compilator and in_compilator == 'False':
        in_compilator = False

    videos = store.get_source_videos(category=category, in_compilator=in_compilator, pleas_state=PS_APPROVED)
    if videos and not in_compilator:
        for i, video in enumerate(videos):
            our_compilations = video.get('our_compilations')
            if our_compilations:
                videos[i]['compilation_names'] = store.get_compilation_names(our_compilations)

    return render_template("videos.html", videos=videos, categories=categories, in_compilator=in_compilator)


@app.route("/videos/update", methods=['POST'])
def video_update():
    data = json.loads(request.data)
    video_ids = data.get('video_ids')
    update_action = data.get('action')
    if video_ids:
        if update_action == 'move_to_compilator':
            store.move_videos(video_ids)
            return jsonify(**{"ok": True})

        if update_action == 'delete':
            store.delete_video(video_ids)
            return jsonify(**{"ok": True})

        if update_action == 'change_category':
            categories = map(lambda x: x.get("value"), data.get("new_categories"))
            store.change_videos_sources_categories(video_ids, categories)
            return jsonify(**{"ok": True})

        if update_action == 'move_from_compilator':
            store.move_videos(video_ids, False)
            return jsonify(**{"ok": True})

        return jsonify(**{"ok": False, "error": "No update action"})

    return jsonify(**{"ok": False, "error": "No video ids"})


@app.route("/videos/set_good/<video_id>")
def video_set_good(video_id):
    not_ = request.args.get("not")

    if not set_good(video_id, not_):
        return jsonify(**{"ok": False, "error": "Can not set video good %("})

    return jsonify(**{"ok": True})


@app.route("/compilations")
def compilations():
    per_page = 25

    query = request.args.get('query')
    if query:
        try:
            _id = ObjectId(query)
        except Exception as e:
            _id = None
        if _id:
            q = {'_id': _id}
        else:
            reg = {'$regex': '.*%s.*' % query, '$options':'i'}
            q = {'$or': [
                {'title': reg},
                {'video_id': reg},
            ]}
    else:
        q = {}

    page = int(request.args.get('page') or 0)
    compilations = store.get_all_compilations(q=q, skip=page * per_page, limit=per_page)

    for i, compilation in enumerate(compilations):
        cuts = compilation.get("cuts")
        if cuts:
            pleases = store.get_pleases(map(lambda x: x.get('video_id'), cuts))
            compilations[i]['pleases'] = pleases

    comps = store.compilations.count()
    return render_template("compilations.html",
                           compilations=compilations, pages=comps / per_page, page=page, query=query)


@app.route('/compilation/<compilation_id>')
def compilation(compilation_id):
    compilation = store.get_compilation(compilation_id)
    if compilation:
        return render_template("compilation.html", compilation=compilation)
    else:
        return redirect("/compilations")


@app.route("/compilation/info/<compilation_id>", methods=['GET'])
def get_compilation(compilation_id):
    compilation = store.get_compilation(compilation_id, fill_videos=True)
    if compilation:
        return jsonify(**{"ok": True, "compilation": compilation})
    return jsonify(**{"ok": False, "error": "compilation not found..."})


@app.route("/compilation/create", methods=['POST'])
def compilation_create():
    data = json.loads(request.data)
    data['title'] = data.get('title', '0,o nO nAMe :( o.0')
    data['description'] = data.get('description', '0.o nO dESCRIPTION :( o.0')

    if "cuts" in data:
        if "compilation_id" in data:
            compilation_id = data.get("compilation_id")
            store.update_compilation(compilation_id, {"title": data.get('title'),
                                                      "cuts": data.get("cuts"),
                                                      "description": data.get("description")
                                                      })
        else:
            compilation_id = store.store_compilation(data.get("title"), data.get("cuts"), data.get("description"))
            for cut in data['cuts']:
                store.update_video_cut(cut['video_id'], cut['from_s'], cut['to_s'], cut['category'], cut['rate'])

        store.set_compilation_state(compilation_id, CS_CREATED)
        store.set_compilation_task(compilation_id, LOAD_AND_CUT)
        data["compilation_id"] = compilation_id
        return jsonify(**dict({"ok": True}, **data))

    return jsonify(**{"ok": False, "error": "Bad input data"})


@app.route("/compilation/update_meta", methods=['POST'])
def update_compilation():
    data = json.loads(request.data)
    compilation_id, title, description = data.get('compilation_id'), data.get('title'), data.get('description')
    if compilation_id and title and description:
        store.update_compilation(compilation_id, data)
        return jsonify(**{"ok": True})
    return jsonify(**{"ok": False, "error": "can not update compilation meta because some of them are empty"})


@app.route("/compilation/concatenate/<compilation_id>", methods=['POST'])
def concatenate(compilation_id):
    result = store.set_compilation_task(compilation_id, CONCATENATE)
    if result.modified_count == 1:
        return jsonify(**{'ok': True, 'compilation_id': compilation_id})
    return jsonify(**{'ok': False, 'error': 'Can not create task to concatenate compilation :( '})


@app.route("/compilation/upload/<compilation_id>", methods=["POST"])
def upload(compilation_id):
    result = store.set_compilation_task(compilation_id, UPLOAD)
    if result.modified_count == 1:
        return jsonify(**{'ok': True, 'compilation_id': compilation_id})
    return jsonify(**{'ok': False, 'error': 'Can not create task to concatenate compilation :( '})


@app.route("/compilation/send_last_pleas/<compilation_id>", methods=['POST'])
def send_last_please(compilation_id):
    compilation = store.get_compilation(compilation_id)
    if compilation:
        for cut in compilation.get("cuts"):
            prepare_compilation_please(cut.get("video_id"), compilation)
        return jsonify(**{"ok": True})
    return jsonify(**{"ok": False, "error": "NO stored compilation for this id:("})


def _retrieve_for_channel(for_channel_raw):
    for_channel = filter(lambda x: len(x) == 24, for_channel_raw)
    for channel in for_channel:
        channel = store.get_channel(channel)
        if not channel:
            channel_data, _ = get_channel_with_videos(channel_id=channel, add_videos=False)
            store.store_channel(channel, VT_COMPILATION, channel_data)
    return for_channel


@app.route("/compilation/send_to_sniffer/<compilation_id>", methods=['POST'])
def send_to_sniffer(compilation_id):
    compilation = store.get_compilation(compilation_id)
    data = json.loads(request.data)

    # todo if have not compilation video id it must wait it or use this procedure after upload!
    if compilation and compilation.get('video_id'):
        headers = {'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest'}
        output = {
            "video_id": compilation.get('video_id'),
            "title": compilation.get('title'),
            "description": compilation.get('description')
        }
        if data.get("for_channel"):
            for_channel = _retrieve_for_channel(data.get('for_channel'))
            output['for_channel'] = for_channel

        log.info("WIll send to sniffer: \n%s" % output)

        result = requests.put(config.get('sniffer_url'),
                              json.dumps(output),
                              headers=headers)

        log.info("Send to sniffer: %s %s" % (result.status_code, result.content))
        if result.status_code == 200:
            result_data = json.loads(result.content)
            if result_data.get('ok') == True:
                upserted_id, category = result_data.get('upserted_id'), result_data.get('category')
                store.update_compilation(compilation_id, {"sn_id": upserted_id, "sn_cat": category})
                return jsonify(**{"ok": True, "result": "Add to sniffer with category %s" % category})
            else:
                return jsonify(**{"ok": False, "error": "Error at sniffer side: %s" % result_data.get("error")})

        return jsonify(**{"ok": False, "error": "Sniffer return %s code" % result.status_code})

    return jsonify(**{"ok": False, "error": "No compilation or compilation have not video_id :("})


@app.route("/compilation/delete/<compilation_id>", methods=['POST'])
def delete_compilation(compilation_id):
    compilation = store.get_compilation(compilation_id, fill_videos=False)
    if compilation:
        result = store.delete_compilation(compilation_id)
        return jsonify(**{"ok": True, 'delete_count': result.deleted_count})
    return jsonify(**{'ok': False, 'error': 'already deleted'})


@app.route("/compilation/create_from_file", methods=['POST'])
def compilation_create_from_file():
    data = json.loads(request.data)
    file_data = data.get('file_data')
    file_name = data.get('file_name')
    if file_data:
        parse_result = get_cuts_from_file(file_data)
        if parse_result.error:
            return jsonify(**{"error": parse_result.error})

        if parse_result.to_compilator:
            store.move_videos(parse_result.to_compilator, to_compilator=True)

        if parse_result.cuts:
            compilation_id = store.store_compilation(file_name, parse_result.cuts, "")
            store.set_compilation_state(compilation_id, CS_CONCATENATED, state_data={"out_fn": file_name})
            return jsonify(**{"ok": True, "compilation_id": compilation_id})
        else:
            return jsonify(**{"ok": True, "result": "videos moved to compilator!"})

    return jsonify(**{"ok": False, "error": "no file data!"})


#####pleases

@app.route('/pleases/load_old')
def load_old_pleases():
    asked_not_replied = store.get_pleases_with_state(PS_ASKED, with_dialog=False)
    log.info("Found asked and not replied pleases: %s" % len(asked_not_replied))
    load_and_store_new_pleas_replies(asked_not_replied)

    return jsonify(**{'ok': True})


@app.route("/pleases")
def pleases():
    state = request.args.get("state")
    if not state:
        state = PS_ASKED

    not_replied = request.args.get("not_replied")
    with_dialog = not_replied is None

    unread_count = 0

    from_ = int(request.args.get('from') or 0)
    to_ = int(request.args.get('to') or 100)

    pleases_count = store.get_pleases_with_state_count(state, with_dialog=with_dialog)
    pleases = store.get_pleases_with_state(state, with_dialog=with_dialog, skip=from_, limit=100)

    def cmp_please(x, y):
        dx, dy = x.get('dialog'), y.get('dialog')
        if dx[-1].get('time') > dy[-1].get('time'):
            return -1
        else:
            return 1

    if pleases:
        try:
            log.info(
                "Start retrieve news of %s:\n%s" % (len(pleases), ', '.join(map(lambda x: x.get('video_id'), pleases))))
            pleases = load_and_store_new_pleas_replies(pleases)
            if with_dialog:
                read = []
                unread = []
                for please in pleases:
                    if is_have_not_reads_replies(please):
                        unread.append(please)
                        unread_count += 1
                    else:
                        read.append(please)
                read.sort(cmp=cmp_please)
                unread.extend(read)
                pleases = unread[:]
        except Exception as e:
            log.error("Error at retrieve new replies, %s" % e)

    videos_info = dict(map(lambda x: (x.get("video_id"), x),
                           store.get_videos(map(lambda x: x.get("video_id"), pleases))))
    pleases = filter(lambda x: x.get('video_id') in videos_info, pleases)

    pages_count = ((pleases_count or 1) / 100) or 1
    current_page = to_ / 100

    return render_template("pleases.html", pleases=pleases, videos=videos_info, not_replied=not_replied, state=state,
                           unread_count=unread_count, pages_count=pages_count, current_page=current_page)


@app.route("/pleas/re_send/<video_id>")
def resend_pleas(video_id):
    pleas = store.get_pleas(video_id)
    comment_id = pleas.get("comment_id")
    text = pleas.get("text")
    result = delete_comment(yt, comment_id)
    log.info("Result of delete comment: %s" % result)

    video = store.get_video(video_id)
    new_comment_id = send_first_please(video, text)
    if new_comment_id:
        log.info("First please of %s sent" % video_id)
        store.update_please_send_state(video_id, True, next_text=text, comment_id=new_comment_id)
        store.set_pleas_resended(video_id)
        return jsonify(**{"ok": True})

    else:
        log.warning("No new comment id for please:\n%s" % pleas)
        return jsonify(**{"ok": False, "error": "No comment id at sending first pleas"})


@app.route("/pleas/delete/<video_id>")
def delete_pleas(video_id):
    result = store.delete_pleas(video_id)
    return jsonify(**{"ok": True, "result": result.deleted_count})


@app.route("/pleas/add_annotation", methods=['POST'])
def please_annotation():
    data = json.loads(request.data)
    pleas_id = data.get("video_id")
    annotation_text = data.get("annotation")
    if pleas_id and annotation_text:
        store.set_pleas_annotation(pleas_id, annotation_text)
        return jsonify(**{"ok": True})
    return jsonify(**{"ok": False})


@app.route("/pleas/set_read/<video_id>/<pr_id>")
def set_pleas_read(video_id, pr_id):
    store.set_pleas_read(video_id, pr_id)
    return jsonify(**{"ok": True})


@app.route("/pleas/send_response", methods=['POST'])
def please_response():
    data = json.loads(request.data)
    video_id = data.get("video_id")
    text = data.get("text")
    if video_id and text:
        send_second_please(video_id, text)
        return jsonify(**{"ok": True})
    return jsonify(**{"ok": False, "error": "no video id or text"})


@app.route("/pleas/set_please_state", methods=['POST'])
def set_please_state():
    data = json.loads(request.data)
    video_id = data.get("video_id")
    pleas_state = data.get("pleas_state")
    if video_id and pleas_state in VPS:
        store.set_pleas_state(video_id, pleas_state)
    if pleas_state == PS_REJECTED:
        video = store.get_video(video_id)
        store.set_channel_pleas_reject(video.get("channel_id"))
    return jsonify(**{"ok": True})


@app.route("/pleas/news", methods=['POST'])
def get_pleas_news():
    data = json.loads(request.data)
    video_id = data.get('video_id')
    if video_id:
        pleas = store.get_pleas(video_id)
        if not pleas:
            return jsonify(**{"ok": False, "error": "No please for this video :("})
        pleas = load_and_store_new_pleas_replies([pleas])
        return jsonify(**{"ok": True, "pleas": pleas[0], "video": store.get_video(video_id)})
    return jsonify(**{"ok": False})


@app.route("/pleas/templates")
def pleases_templates():
    pleases = store.get_pleas_templates()
    return render_template("pleases_templates.html", pleases=pleases)


@app.route("/pleas/template/add", methods=['POST'])
def add_pleas_template():
    data = json.loads(request.data)
    if data.get("content"):
        pleas_id = store.store_pleas_template(data['content'], data.get("pt_params"))
        if pleas_id:
            return jsonify(**{"ok": True, "pleas": {"content": data['content'], "id": pleas_id}})

        return jsonify(**{"ok": False, "error": "Pleas already exist"})

    return jsonify(**{"ok": False, "error": "Bad data"})


@app.route("/pleas/template/remove/<pleas_id>")
def remove_please_template(pleas_id):
    result = store.remove_pleas_template(pleas_id)
    return jsonify(**{"result": result})


@app.route("/pleas/template/test/<pleas_id>/<video_id>")
def test_pleas(pleas_id, video_id):
    pleas = store.get_pleas_template(pleas_id)
    video = store.get_video(video_id)
    if pleas and video:
        pleases_template = pleas.get("content")
        text = pleases_template % video
        return jsonify(**{"ok": True, "text": text})
    return jsonify(**{"ok": False, "error": "pleas or video id not found :("})


please_completer = Completer(store.get_all_pleas_replies())


@app.route("/please_replies/add", methods=['POST'])
def pleas_add_reply():
    data = json.loads(request.data)
    text = data.get("text").strip()
    pr_id = data.get("pr_id")
    if text and pr_id:
        store.store_pleas_reply(pr_id, text)
        please_completer.options.add(text)
        return jsonify(**{"ok": True})


@app.route("/please_replies")
def please_replies():
    query = request.args.get("query")
    result = please_completer.complete(query)
    return jsonify(**{"ok": True, "result": result})


#####extractor
COUNT_VIDEOS_SHOW = 5


@app.route("/extractor")
def extractor():
    filter_channel_id = request.args.get("channel_id")
    next_batch = request.args.get("next")
    if next_batch:
        next_batch = int(next_batch) + 5

    if filter_channel_id:
        channel = store.get_channel(filter_channel_id)
        load_and_store_channel_videos(filter_channel_id, channel.get('last_video_id'))

    video_compilations = store.get_video_compilations_without_categories(channel_id=filter_channel_id)
    result_video_compilation = []
    for video_compilation in video_compilations:
        sources = retrieve_and_store_related_videos(video_compilation)
        if sources:
            if len(result_video_compilation) < COUNT_VIDEOS_SHOW:
                result_video_compilation.append(dict({'sources': sources}, **video_compilation))
            else:
                break
        else:
            bad_comp_video_id = video_compilation.get('video_id')
            store.update_video_type(bad_comp_video_id, VT_COMPILATION_WITHOUT_SOURCES)

    channels = store.get_channels(channel_type=VT_COMPILATION, is_deleted=False, is_rejected=None)
    log.info(
        "Channels: \n%s" % '\n'.join(["%s: %s" % (chan.get('title'), chan.get('channel_id')) for chan in channels]))

    return render_template("extractor.html", **{"videos": result_video_compilation,
                                                "channels": channels,
                                                "next_batch": next_batch,
                                                "channel_id": filter_channel_id})


category_completer = Completer(store.get_all_categories())


@app.route("/extractor/delete_channel/<channel_id>")
def delete_channel(channel_id):
    result = store.delete_channel(channel_id)
    if result.modified_count > 0:
        return jsonify(**{"ok": True})
    return jsonify(**{"ok": False})


@app.route("/extractor/exclude_source/<video_id>")
def exclude_source(video_id):
    store.set_video_exclude(video_id)
    return jsonify(**{"ok": True})


@app.route("/extractor/video_categories")
def video_categories():
    query = request.args.get("query")
    result = category_completer.complete(query)
    return jsonify(**{"ok": True, "result": result})


@app.route("/extractor/channels_add", methods=['POST'])
def add_extractor_channels():
    data = json.loads(request.data)
    channels = filter(lambda x: x, map(lambda x: x.strip(), data.get("channels", "").split("\n")))
    log.info("Will add this channels: %s" % channels)
    for channel_id in channels:
        load_and_store_channel_videos(channel_id)

    return jsonify(**{"ok": True})


@app.route("/extractor/remove_video/<video_id>")
def remove_video(video_id):
    result = store.delete_video(video_id, {"because": "3030 want it in extractor"})
    return jsonify(**{"ok": True, "result": result.modified_count})


@app.route("/extractor/set_video_categories", methods=["POST"])
def set_video_categories():
    data = json.loads(request.data)
    video_id = data.get("video_id")
    categories = map(lambda x: x.get("value"), data.get("categories"))
    if video_id and categories:
        store.set_video_compilation_categories(video_id, categories)
        videos_of_compilation = store.get_videos_of_compilation(video_id)

        for video in videos_of_compilation:
            result = initialise_first_pleas(video)
            if result:
                log.info("Was store pleas for [%s]" % (video.get("video_id")))

        category_completer.options = category_completer.options.union(categories)
        return jsonify(**{"ok": True})

    else:
        return jsonify(**{"ok": False, "error": "No video id or categories"})


@app.route("/good", methods=['POST', 'GET'])
def good_page():
    if request.method == 'POST':
        entity_id = request.form.get("entity_id").strip()
        not_ = request.form.get("not")
        if not entity_id:
            return redirect('/good')
        if not set_good(entity_id, not_):
            flash("Can not recognise video id or channel id")

    videos, channels = store.get_videos_good()
    return render_template("good.html", channels=channels, videos=videos)


def get_my_channel_name():
    my_channel = store.get_my_channel_info()
    if my_channel:
        return my_channel.get("title")


@app.route("/change_account")
def change_account():
    os.remove(STORAGE_FILE)
    yt = get_youtube()
    channel_info = get_my_channel_info(yt)
    store.set_my_channel_info(channel_info)
    return redirect("/")


@app.route('/add_video', methods=['POST'])
def add_video():
    data = json.loads(request.data)
    video_id = data.get('video_id')
    categories = map(lambda x: x.get("value"), data.get("categories"))
    if video_id and categories:
        video = get_video_info(video_id)
        if video:
            store.store_video(video, VT_SOURCE)
            store.add_video_source_categories(video_id, categories + [CAT_FROM_4LB])
            prepare_add_video_please(video)
            return jsonify(**{"ok": True})

        return jsonify(**{"ok": False, 'error': "Can not load video_id: %s" % video_id})

    return jsonify(**{"ok": False, 'error': 'No video_id or categories :('})


app.add_url_rule('/exclude/', view_func=ExcludeAPI.as_view("exclude_page"))

channels_tracked_view = ChannelsTrackedAPI.as_view("channels_tracked")
app.add_url_rule('/channels_tracked/', view_func=channels_tracked_view)
app.add_url_rule('/channels_tracked/<channel_id>', view_func=channels_tracked_view, methods=['DELETE'])

app.jinja_env.globals.update(my_channel_name=get_my_channel_name)

fv_view = FindVideosApiView.as_view("find_videos")
app.add_url_rule("/find_videos", view_func=fv_view)

fc_view = FindChannelsApiView.as_view("find_channels")
app.add_url_rule("/find_channels", view_func=fc_view)

cuts_view = CutsApi.as_view('cuts')
app.add_url_rule('/cuts', view_func=cuts_view)
app.add_url_rule('/cuts/<video_id>', view_func=cuts_view, methods=['GET'])

ws_view = WordsSpeedView.as_view('ws')
app.add_url_rule('/ws', view_func=ws_view)

core_view = KeysCoreView.as_view('keys_core')
app.add_url_rule('/keys_core', view_func=core_view)

if __name__ == '__main__':
    if not os.getenv("NOT_START_FUCKING_BROWSER", False):
        import webbrowser

        webbrowser.open("http://localhost:9999", new=2)

    port = 9999
    while 1:
        try:
            log.info("Trying start at: %s" % port)
            run_params = {'port': port}
            if IS_TEST:
                run_params.update({'debug': True, 'host': '0.0.0.0'})
                app.jinja_env.auto_reload = True
                app.config['TEMPLATES_AUTO_RELOAD'] = True
                log.info('Start server at TEST mode')

            app.run(**run_params)
            log.info("STARTED! %s" % port)
        except Exception as e:
            log.info("%s is busy :(" % port)
            port -= 1
