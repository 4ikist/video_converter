import httplib
import random
import re
import string

import httplib2
import os
import time
import datetime

from apiclient.discovery import build

from oauth2client.tools import argparser, run_flow
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser


from src import WORK_DIR, net_tryings

# Explicitly tell the underlying HTTP transport library not to retry, since
# we are handling retry logic ourselves.

httplib2.RETRIES = 1

# Maximum number of times to retry before giving up.
MAX_RETRIES = 10

# Always retry when these exceptions are raised.
RETRIABLE_EXCEPTIONS = (httplib2.HttpLib2Error, IOError, httplib.NotConnected,
                        httplib.IncompleteRead, httplib.ImproperConnectionState,
                        httplib.CannotSendRequest, httplib.CannotSendHeader,
                        httplib.ResponseNotReady, httplib.BadStatusLine)

# Always retry when an apiclient.errors.HttpError with one of these status
# codes is raised.
RETRIABLE_STATUS_CODES = [500, 502, 503, 504]

# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret. You can acquire an OAuth 2.0 client ID and client secret from
# the {{ Google Cloud Console }} at
# {{ https://cloud.google.com/console }}.
# Please ensure that you have enabled the YouTube Data API for your project.
# For more information about using OAuth2 to access the YouTube Data API, see:
#   https://developers.google.com/youtube/v3/guides/authentication
# For more information about the client_secrets.json file format, see:
#   https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
CLIENT_SECRETS_FILE = os.path.join(WORK_DIR, "client_secrets.json")
STORAGE_FILE = os.path.join(WORK_DIR, "oauth2_storage.json")

# This OAuth 2.0 access scope allows an application to upload files to the
# authenticated user's YouTube channel, but doesn't allow other types of access.
YOUTUBE_SCOPE = "https://www.googleapis.com/auth/youtube"
YOUTUBE_UPLOAD_SCOPE = "https://www.googleapis.com/auth/youtube.upload"
YOUTUBE_READ_WRITE_SSL_SCOPE = "https://www.googleapis.com/auth/youtube.force-ssl"
YOUTUBE_CHANNEL_AUDIT = "https://www.googleapis.com/auth/youtubepartner-channel-audit"
YOUTUBE_PARTNER = "https://www.googleapis.com/auth/youtubepartner"

YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

# This variable defines a message to display if the CLIENT_SECRETS_FILE is
# missing.
MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file
found at:

   %s

with information from the {{ Cloud Console }}
{{ https://cloud.google.com/console }}

For more information about the client_secrets.json file format, please visit:
https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
""" % os.path.abspath(os.path.join(os.path.dirname(__file__),
                                   CLIENT_SECRETS_FILE))

VALID_PRIVACY_STATUSES = ("public", "private", "unlisted")


def get_authenticated_service(args):
    flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE,
                                   scope=[YOUTUBE_SCOPE,
                                          YOUTUBE_UPLOAD_SCOPE,
                                          YOUTUBE_READ_WRITE_SSL_SCOPE,
                                          YOUTUBE_CHANNEL_AUDIT,
                                          YOUTUBE_PARTNER],
                                   message=MISSING_CLIENT_SECRETS_MESSAGE,
                                   redirect_uri="http://localhost:9999")

    storage = Storage(STORAGE_FILE)
    credentials = storage.get()

    if credentials is None or credentials.invalid:
        credentials = run_flow(flow, storage, args)

    return build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                 http=credentials.authorize(httplib2.Http()))

def rename_cache_file(new_name):
    from googleapiclient.discovery_cache.file_cache import FILENAME
    if new_name:
        FILENAME += str(new_name)
    else:
        FILENAME += ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
    return


def get_youtube(for_name=False):
    rename_cache_file(for_name)
    args = argparser.parse_args()
    youtube = get_authenticated_service(args)
    return youtube


def yt_date_to_tst(yt_date):
    return time.mktime(datetime.datetime.strptime(yt_date, "%Y-%m-%dT%H:%M:%S.000Z").timetuple())


def next_page_iter(list_func, q):
    while 1:
        result = net_tryings(list_func(**q).execute)()
        if not result:
            break

        items = result.get("items", [])
        if items:
            for item in items:
                yield item
        else:
            break

        if not result.get("nextPageToken"):
            break
        else:
            q['pageToken'] = result.get("nextPageToken")


COMMENTS_CHUNK_SIZE = 40


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]


duration_obj_to_seconds = lambda x: 3600 * int(x['hours'] or 0) + \
                        60 * int(x['minutes'] or 0) + \
                                    int(x['seconds'] or 0)

duration_reg = re.compile(u"PT((?P<hours>\d+)H)?((?P<minutes>\d+)M)?((?P<seconds>\d+)S)?")


def _parse_time(duration_str):
    for d in duration_reg.finditer(duration_str):
        result = d.groupdict()
        result = dict(map(lambda x: (x[0], int(x[1]) if x[1] else None), result.items()))
        return result


def get_duration(duration_str):
    return duration_obj_to_seconds(_parse_time(duration_str))
