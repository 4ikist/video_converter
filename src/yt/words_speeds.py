import logging
import re
from Queue import Empty
from collections import Counter

import time
from multiprocessing import Process, Queue

from stop_words import safe_get_stop_words
from src import VT_COMPILATION
from src.core import get_my_channel_id
from src.db import DBHandler, VC_Storage, CollectionInitter
from src.yt import get_youtube
from src.yt.info import get_channel_with_videos, get_many_videos, get_channel_info
from src.yt.search import search_video_ids

log = logging.getLogger('words')

WSCS_KEY_PREPARED = 'keys_prepared'
WSCS_SEARCH_RESULT_PREPARED = 'search_result_prepared'
WSCS_END = 'words_speed_prepared'
WSCS_NOT_STARTED = 'not_started'


class WordsStore(CollectionInitter):
    def __init__(self, ):
        super(WordsStore, self).__init__(name='main')
        self.init_collection('search_keys', indexes=['t', 'key'])
        self.init_collection('words_results', indexes=['t', 'word'])
        self.c = 0

    def push_search_keys(self, data):
        self.c += 1
        t = time.time()
        for key, count in data.items():
            self.search_keys.update_one({'key': key}, {"$set": {'count': count, 't': t}}, upsert=True)

    def get_like(self, key_part):
        return list(self.search_keys.find({'key': {'$regex': '.*%s.*' % key_part, '$options': 'i'}},
                                          sort=[('count', -1), ('t', -1)]))

    def get_last_search_keys(self):
        found = list(self.search_keys.find({}, sort=[('t', -1)], limit=1))
        if found:
            sk = list(self.search_keys.find({'t': found[0].get('t')}))
            return sk

    def get_all_search_keys(self, projection=None):
        return list(self.search_keys.find({}, projection=projection))

    def delete_search_keys(self, keys):
        return self.search_keys.delete_many({'key': {'$in': keys}})

    def store_words_result(self, data):
        t = time.time()
        for word, positions in data.iteritems():
            stored = self.words_results.find_one({'word': word})
            if stored:
                positions.update(stored.get('positions', {}))
            self.words_results.update_one({'word': word}, {"$set": {'t': t, 'positions': positions}}, upsert=True)

    def get_last_words_result(self):
        found = list(self.words_results.find({}, sort=[('t', -1)], limit=1))
        if found:
            wr = self.words_results.find({'t': found[0].get('t')})
            return list(wr)

    def get_all_words_results(self, projection=None):
        return list(self.words_results.find({}, projection=projection))


store = VC_Storage()

stop_words = safe_get_stop_words('en')
stop_words.extend(store.get_stop_words())


def _clear_word(word):
    if len(word) < 3:
        return False

    digit = re.compile('\d+')
    if digit.match(word):
        return None

    not_alpha = re.compile('\W+')
    if not_alpha.match(word):
        return None

    chars = re.findall('[a-z]+', word)
    if chars:
        if chars[0] in stop_words:
            return None
        return chars[0]


def get_semantic_core(channel_id):
    channel_data, videos = get_channel_with_videos(channel_id, add_videos=True)
    acc = []
    for video in videos:
        video_words = video.get('title').lower().split()
        acc.extend(video_words)

    c = Counter(acc)
    return dict(
        filter(
            lambda x: x[0] and x[1] > 1,
            map(
                lambda x: (
                    _clear_word(x[0]),
                    x[1]
                ), c.items()
            )
        )
    )


def calculate_channel_speed(yt, channel_id, channel_min_views, channel_min_videos_count):
    channel_statistic = get_channel_info(yt, channel_id, part='statistics')
    if channel_statistic:
        if int(channel_statistic.get('viewCount')) <= channel_min_views:
            log.info("Channel %s so low for views count" % channel_id)
            return
        if int(channel_statistic.get('videoCount')) < channel_min_videos_count:
            log.info('Channel %s so empty' % channel_id)

        channel_info, videos_of_channel = get_channel_with_videos(channel_id,
                                                                  max_videos=channel_min_videos_count,
                                                                  add_videos=True,
                                                                  part='snippet')

        fasted_video = None
        video_speeds = []
        videos_of_channel = get_many_videos(yt, map(lambda x: x.get('video_id'), videos_of_channel))

        for video in videos_of_channel:
            video_speed = video.get('yt_views') / (time.time() - video.get('time'))
            video_speeds.append(video_speed)
            if fasted_video is None or fasted_video[1] < video_speed:
                video['speed'] = video_speed
                fasted_video = (video, video_speed)

        if videos_of_channel:
            average_channel_speed = sum(video_speeds) / len(videos_of_channel)
            return average_channel_speed, fasted_video[0]


def prepare_semantic_core():
    words_store = WordsStore()
    my_channel_id = get_my_channel_id()
    core = get_semantic_core(my_channel_id)
    donors = store.get_channels(channel_type=VT_COMPILATION, is_deleted=False, is_rejected=None)
    for donor in donors:
        donor_core = get_semantic_core(donor.get('channel_id'))
        core.update(donor_core)
    words_store.push_search_keys(core)
    return core


class ChannelSpeedCalculator():
    def __init__(self, q, channels_count_per_gramm,
                 video_max_age,
                 channel_min_views,
                 channel_min_videos_count):
        self.q = q
        self.channels_count_per_gramm = channels_count_per_gramm
        self.video_max_age = video_max_age
        self.channel_min_views = channel_min_views
        self.channel_min_videos_count = channel_min_videos_count

    def retrieve_channels(self):
        log.info("Start retrieve %s" % self.q)
        yt = get_youtube(self.q)
        count = 0
        calculated_channels = set()
        result = []

        for i, video in enumerate(search_video_ids(yt, self.q, video_definition=None, type=None, order='viewCount')):
            if (time.time() - video.get('time')) > self.video_max_age:
                log.info('Video %s too old for analyse' % video.get('video_id'))
                continue

            channel_id = video.get('channel_id')
            if channel_id and channel_id not in calculated_channels:
                log.info("retrieve data from %s" % channel_id)
                channel_parameters = calculate_channel_speed(yt, channel_id,
                                                             self.channel_min_views,
                                                             self.channel_min_videos_count)
                if channel_parameters:
                    speed, fasted_video = channel_parameters
                    result.append((channel_id, fasted_video, speed))
                    count += 1

                calculated_channels.add(channel_id)

            if count > self.channels_count_per_gramm:
                break

        return result


class WordSpeedCalculator(Process):
    keys_prepared_ttl = 3600 * 24 * 7
    keys_shift_left = 0
    keys_shift_right = 20

    search_key_delimeter = ' '
    gramm_size = 3

    video_max_age = 3600 * 24 * 30
    channel_min_videos_count = 10
    channel_min_views = 2 * 1000 * 1000
    channels_count_per_gramm = 100

    video_min_views = 10000

    yt = get_youtube()
    ws = WordsStore()

    def __init__(self):
        super(WordSpeedCalculator, self).__init__()
        self.queue = Queue(3)
        self.last_state = WSCS_NOT_STARTED

    def set_params(self, params):
        def_params = self.__class__.__dict__
        for k, v in params.iteritems():
            if k in self.__class__.__dict__:
                self.__dict__[k] = type(def_params[k])(v)

    def search_state(self, search_keys):
        result = []
        gramms = _get_ngrams(search_keys, self.gramm_size)

        log.info("Will retrieve. %s iterations " % len(gramms))
        for i, keys in enumerate(gramms):
            q = self.search_key_delimeter.join(keys)

            calc = ChannelSpeedCalculator(
                q,
                self.channels_count_per_gramm,
                self.video_max_age,
                self.channel_min_views,
                self.channel_min_videos_count,
            )
            result.extend(calc.retrieve_channels())

        sorted(result, key=lambda x: x[2])

        return result

    def consolidate_state(self, sorted_channels_infos):
        '''
        :param sorted_channels_infos:
        :return: {
                    word:{
                            position: {speed:<average speed of all videos with this word in this position>,
                count: }}}
        '''
        result = {}
        for channel_params in sorted_channels_infos:
            channel_id, fasted_video, channel_speed = channel_params
            keys = fasted_video.get('title').split()
            for position, key in enumerate(keys):
                key_info = result.get(key, {})

                key_position_info = key_info.get(position, {})
                key_position_info['speed'] = (key_position_info.get('speed', 0) + fasted_video.get('speed')) \
                                             / (key_position_info.get('count', 0) + 1)
                key_position_info['count'] = key_position_info.get('count', 0) + 1

                key_info[str(position)] = key_position_info
                result[key] = key_info

        return result

    def process(self):
        saved_keys = self.ws.get_last_search_keys()
        if not saved_keys or (time.time() - saved_keys[0].get('t') > self.keys_prepared_ttl):
            saved_keys = prepare_semantic_core()
        else:
            saved_keys = dict(map(lambda x: (x.get('key'), x.get('count')), saved_keys))
        keys_with_counts = saved_keys.items()
        sorted(keys_with_counts, key=lambda x: x[1])
        keys = map(lambda x: x[0], keys_with_counts)
        self.queue.put_nowait(WSCS_KEY_PREPARED)
        log.info("KEYS: \n%s" % ("\n".join(["%s:\t%s" % (k, v) for k, v in keys_with_counts])))

        search_result = self.search_state(keys[self.keys_shift_left:self.keys_shift_right])
        self.queue.put_nowait(WSCS_SEARCH_RESULT_PREPARED)
        log.info("SEARCH_RESULT: \n%s" % (
            "\n".join(["%s\t%s\t%s" % (ch_id, v.get('title'), speed) for ch_id, v, speed in search_result])))

        result = self.consolidate_state(search_result)
        self.ws.store_words_result(result)
        self.queue.put_nowait(WSCS_END)
        log.info('RESULT STORED: \n%s' % ('\n'.join(
            ['%s:\n\t%s' % (word, '\n\t'.join(['%s:\t%s' % (position, speed) for position, speed in positions.items()]))
             for word, positions in result.items()])))
        return result

    def get_current_state(self):
        try:
            self.last_state = self.queue.get_nowait()
        except Empty:
            pass
        return self.last_state

    def run(self):
        log.info("WordsSpeedCalculator started")
        self.process()
        log.info("WordsSpeedCalculator ended")


def _get_ngrams(list, n):
    result = []
    for i in range(len(list)):
        if len(list) >= i + n:
            result.append([])
            for j in range(n):
                result[i].append(list[i + j])
    return result


if __name__ == '__main__':
    # ws_store = WordsStore()
    # ws_store.search_keys.drop()
    # ws_store.words_results.drop()

    ws_calc = WordSpeedCalculator()
    ws_calc.set_params({'keys_shift_left': 10})
    ws_calc.start()
    time.sleep(1)
    while 1:
        q = ws_calc.get_current_state()
        print q
        if q == WSCS_END:
            break
        else:
            time.sleep(5)
    ws_calc.join()
