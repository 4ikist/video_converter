from datetime import datetime, timedelta

from src.yt import next_page_iter, yt_date_to_tst


def search_video_ids(yt, q, before=None, after=None, video_definition='high', type='video', order='date'):
    query = {
        'q': q,
        'part': "id,snippet",
        'maxResults': 50,
        'order': order,
    }

    if type:
        q['type'] = type

    if video_definition:
        q['videoDefinition'] = video_definition

    if before:
        before += "Z" if not before.endswith("Z") else ""
        q['publishedBefore'] = before

    if after:
        after += "Z" if not after.endswith("Z") else ""
        q['publishedAfter'] = after

    for item in next_page_iter(yt.search().list, query):
        snippet = item.get('snippet')
        video_id = item.get('id', {}).get('videoId')
        channel_id = snippet.get('channelId')
        yield {"video_id": video_id,
               "channel_id": channel_id,
               "title": snippet.get('title'),
               "time": yt_date_to_tst(snippet.get('publishedAt'))}
