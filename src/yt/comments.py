# coding: utf-8
import json
from collections import defaultdict

from src import net_tryings
from src.yt import yt_date_to_tst, get_youtube, next_page_iter, chunks, COMMENTS_CHUNK_SIZE
from src.yt.info import log, get_video_info


def get_replies_of_me(youtube, comment_ids):
    result = {}
    for chunk in chunks(comment_ids, COMMENTS_CHUNK_SIZE):
        q = {"part": "snippet,replies",
             "textFormat": "plainText",
             "order": "time",
             "id": ",".join(chunk)}
        list_func = youtube.commentThreads().list
        try:
            for item in next_page_iter(list_func, q):
                snippet = item.get("snippet", {})
                if snippet:
                    top_level_comment = snippet.get("topLevelComment")
                    parent_id = top_level_comment.get("id")
                    if parent_id in comment_ids:
                        result[parent_id] = map(lambda reply: {
                            "owner_name": reply.get("snippet", {}).get("authorDisplayName"),
                            "owner_channel_id": reply.get("snippet", {}).get("authorChannelId", {}).get("value"),
                            "time": yt_date_to_tst(reply.get("snippet", {}).get("publishedAt")),
                            "text": reply.get("snippet", {}).get("textDisplay") or reply.get("snippet", {}).get(
                                "textOriginal"),
                            "pr_id": reply.get("id")
                        }, item.get("replies", {}).get("comments", []))
        except Exception as e:
            log.exception(e)
            log.error("can not load chunk of comment thread... :(")
    return result


@net_tryings
def send_reply(youtube, parent_id, text):
    insert_result = youtube.comments().insert(
        part="snippet",
        body=dict(
            snippet=dict(
                parentId=parent_id,
                textOriginal=text
            )
        )
    ).execute()
    log.info(
        "Reply send for comment [%s] with text: [%s]\n Result: %s" % (
            parent_id, text, json.dumps(insert_result, indent=4)))
    return insert_result


@net_tryings
def send_comment(youtube, channel_id, video_id, text):
    insert_result = youtube.commentThreads().insert(
        part="snippet",
        body=dict(
            snippet=dict(
                channelId=channel_id,
                videoId=video_id,
                topLevelComment=dict(
                    snippet=dict(
                        textOriginal=text
                    )
                )
            )
        )
    ).execute()
    log.info(
        "Comment send for [%s] with text: [%s]\n Result: %s" % (video_id, text, json.dumps(insert_result, indent=4)))
    comment_id = insert_result.get("snippet", {}).get("topLevelComment", {}).get("id")
    return comment_id


@net_tryings
def get_comments(youtube, video_id=None, channel_id=None, less_than=3000):
    if not video_id and not channel_id:
        raise Exception

    q = {
        "part": "id,snippet",
        "maxResults": 100
    }
    if video_id:
        q["videoId"] = video_id
    if channel_id:
        q['allThreadsRelatedToChannelId'] = channel_id

    list_func = youtube.commentThreads().list
    counter = 0
    for item in next_page_iter(list_func, q):
        comment = item.get("snippet", {}).get("topLevelComment")
        if comment:
            comment_snippet = comment.get("snippet")

            yield dict(comment_id=comment.get('id'),
                       author=comment_snippet.get("authorDisplayName"),
                       channel_id=comment_snippet.get('authorChannelId', {}).get('value'),
                       text=comment_snippet.get("textDisplay"),
                       video_id=comment_snippet.get("videoId")
                       )
            counter += 1
            if less_than and counter > less_than:
                break


@net_tryings
def delete_comment(youtube, comment_id):
    try:
        result = youtube.comments().delete(
            id=comment_id
        ).execute()
        log.info("Comment deleted [%s]" % comment_id)
        return result
    except Exception as e:
        log.exception(e)
        return None


if __name__ == '__main__':
    youtube = get_youtube()
    comments = get_comments(youtube, channel_id="UC5vSBbGV8mrAlPTNbXGHqmg")

    # delete_comment(youtube, comments[0]['id'])

    # comment_id = send_comment(youtube, channel_id, video_id,
    #                           "What about some stupid babies or tits or asses? I want it at next video!")
    # send_reply(youtube, comment_id, "And more crashes:) Yes yes yes!")

    # print get_replies_of_me(youtube, video_id, comment_id)
    # print get_replies_of_me(youtube, video_id, my_parent_id=comment_id)
