import logging
import os

import pafy
from pytube import YouTube

from src import VIDEOS_PATH, tryings
from src.convert import DEFAULT_EXTENSION, DEFAULT_RESOLUTION_SHORT

log = logging.getLogger("load")


def file_path(video_id):
    file_name = "%s.%s" % (video_id, DEFAULT_EXTENSION)
    return os.path.join(VIDEOS_PATH, file_name), file_name


@tryings()
def get_video_download_stream(video_id):
    v = pafy.new(video_id, basic=False, ydl_opts={'force':True})
    s = v.getbest(preftype=DEFAULT_EXTENSION)
    return s


@tryings(time_to_sleep=30, count_tryings=3, randomized=True)
def download_pytube(video_id):
    _, name = file_path(video_id)

    yt = YouTube('http://youtube.com/watch?v=%s' % video_id, )
    yt.register_on_complete_callback(
        lambda stream, file_handle:
        log.info('%s download complete\n\t to: %s' % (
            video_id, file_handle
        )))

    return yt.streams \
        .filter(progressive=True, file_extension=DEFAULT_EXTENSION, res=DEFAULT_RESOLUTION_SHORT) \
        .first() \
        .download(output_path=VIDEOS_PATH, filename=name)


@tryings(time_to_sleep=30, count_tryings=3, randomized=True)
def download_pafy(video_id):
    path, _ = file_path(video_id)
    s = get_video_download_stream(video_id)
    result = s.download(filepath=path)
    log.info("%s download complete\n\t to: %s" % (video_id, result))
    return result


def download_video(video_id):
    for method in [download_pafy, download_pytube]:
        result = method(video_id)
        if result:
            return result


def get_loaded_filename(video_id):
    fn, _ = file_path(video_id)
    if os.path.isfile(fn):
        return fn


if __name__ == '__main__':
    video_id = "w6u9-msmrMM"
    print download_video(video_id)
