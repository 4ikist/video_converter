import logging

import random
import time
from datetime import datetime, timedelta

from apiclient.http import MediaFileUpload
from apiclient.errors import HttpError

from src.yt import RETRIABLE_STATUS_CODES, RETRIABLE_EXCEPTIONS, MAX_RETRIES, get_youtube

log = logging.getLogger("yt_upload")


def initialize_upload(youtube, file_name, title, description):
    log.info("Will upload: %s \nwith title: %s\n and description:\n%s" % (file_name, title, description))
    body = dict(
        snippet=dict(
            title=title,
            description=description,
            categoryId='22',

        ),
        status=dict(
            privacyStatus='private',
            publicStatsViewable='private',
            publishAt="%s.000Z" % (datetime.utcnow() + timedelta(days=7)).replace(microsecond=0).isoformat()
        )
    )

    # Call the API's videos.insert method to create and upload the video.
    insert_request = youtube.videos().insert(
        part=",".join(body.keys()),
        body=body,
        # The chunksize parameter specifies the size of each chunk of data, in
        # bytes, that will be uploaded at a time. Set a higher value for
        # reliable connections as fewer chunks lead to faster uploads. Set a lower
        # value for better recovery on less reliable connections.
        #
        # Setting "chunksize" equal to -1 in the code below means that the entire
        # file will be uploaded in a single HTTP request. (If the upload fails,
        # it will still be retried where it left off.) This is usually a best
        # practice, but if you're using Python older than 2.6 or if you're
        # running on App Engine, you should set the chunksize to something like
        # 1024 * 1024 (1 megabyte).
        media_body=MediaFileUpload(file_name, chunksize=5 * 1024 * 1024, resumable=True)
    )

    return resumable_upload(insert_request)


# This method implements an exponential backoff strategy to resume a
# failed upload.
def resumable_upload(insert_request):
    response = None
    error = None
    retry = 0
    while response is None:
        try:
            log.info("Uploading file...")
            status, response = insert_request.next_chunk()
            if response is not None:
                if 'id' in response:
                    log.info("Video id '%s' was successfully uploaded." % response['id'])
                    return response['id']
                else:
                    log.error("The upload failed with an unexpected response: %s" % response)
                    return
        except HttpError, e:
            if e.resp.status in RETRIABLE_STATUS_CODES:
                error = "A retriable HTTP error %d occurred:\n%s" % (e.resp.status,
                                                                     e.content)
            else:
                raise
        except RETRIABLE_EXCEPTIONS, e:
            error = "A retriable error occurred: %s" % e

        if error is not None:
            log.error(error)
            retry += 1
            if retry > MAX_RETRIES:
                log.error("No longer attempting to retry.")

            max_sleep = 2 ** retry
            sleep_seconds = random.random() * max_sleep
            log.warning("Sleeping %f seconds and then retrying..." % sleep_seconds)
            time.sleep(sleep_seconds)


def upload_video(file_name, title, description):
    youtube = get_youtube()
    try:
        return initialize_upload(youtube, file_name, title, description), None
    except Exception as e:
        log.exception(e)
        return None, e


if __name__ == '__main__':
    from src import VIDEOS_PATH
    import os

    upload_video(
        os.path.join(VIDEOS_PATH, '_pre_logo.mp4'),
        'test',
        'test description'
    )
