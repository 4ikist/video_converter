# coding=utf-8
import json
import logging
import re

import time
from apiclient.discovery import build
from apiclient.errors import HttpError
from youtube_dl.utils import ISO3166Utils

from src import net_tryings
from src.convert import DEFAULT_RESOLUTION
from src.db import VC_Storage
from src.yt import next_page_iter, yt_date_to_tst, chunks, COMMENTS_CHUNK_SIZE, get_duration, get_youtube
from src.yt.load import get_video_download_stream

re_time = re.compile("^\d{2}\:\d{2}$")

DEVELOPER_KEY = "AIzaSyALPCgnpIM6KcJsilUsi1VxO5A7xgLujPQ"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

# youtube = get_youtube()
youtube = build(YOUTUBE_API_SERVICE_NAME,
                YOUTUBE_API_VERSION,
                developerKey=DEVELOPER_KEY)

log = logging.getLogger("yt_info")


# todo refactor!

def get_channels_deleted_info_list(channel_ids, chunk_size=COMMENTS_CHUNK_SIZE):
    result = []
    for chunk in chunks(channel_ids, chunk_size):
        response = youtube.channels().list(part="id", id=",".join(chunk)).execute()
        items = response.get('items', [])
        result.extend(map(lambda x: x.get("id"), items))

    difference = set(channel_ids).difference(set(result))
    return difference


@net_tryings
def get_user_channel_id(user_name):
    q = {
        "part": "id",
        "forUsername": user_name,
    }
    result = youtube.channels().list(**q).execute()
    if 'items' in result:
        for item in result['items']:
            if 'id' in item:
                channel_id = item['id']
                return channel_id


@net_tryings
def get_channel_info(yt, channel_id, part=None):
    part = part or 'statistics'
    q = {
        'part': part or 'statistics',
        'id': channel_id,
    }
    result = yt.channels().list(**q).execute()
    channel_object = result.get('items', [None])[0]
    if channel_object:
        fields = map(lambda x: x.strip(), part.split(','))
        result = {}
        for field in fields:
            result.update(channel_object[field])
        return result


def get_channel_with_videos(channel_id, before_video_id=None, add_videos=True, max_videos=None, part=None):
    q = {
        "part": part or "snippet",
        "maxResults": 50,
        "order": "date",
        "channelId": channel_id
    }

    func = youtube.search().list
    result = {}
    videos = []
    before_found = False

    for item in next_page_iter(func, q):
        video_id = item.get("id").get("videoId")
        _channel_id = item.get("id").get("channelId")

        if _channel_id and _channel_id == channel_id:
            result['title'] = item['snippet']['title']
            result['description'] = item['snippet']['description']
            result['channel_id'] = _channel_id

        if video_id == before_video_id:
            before_found = True

        if (before_video_id and before_found) or not add_videos or (max_videos and len(videos) >= max_videos):
            if not result:
                continue
            else:
                break

        if video_id:
            videos.append({
                "video_id": video_id,
                "title": item['snippet']['title'],
                "description": item['snippet']['description'],
                "channel_title": item['snippet']['channelTitle'],
                "channel_id": item['snippet']['channelId'],
                "time": yt_date_to_tst(item['snippet']['publishedAt'])
            })

    return result, videos


store = VC_Storage("yt_info")


@net_tryings
def get_video_info(video_id, check_quality=True, check_copyright=True, part=None, raw_result=False):
    log.info("Will load from YT: %s" % video_id)
    video_response = youtube.videos().list(
        id=video_id,
        part=part or 'snippet,statistics,contentDetails'
    ).execute()

    for video_result in video_response.get("items", []):
        snippet = video_result.get("snippet", {})
        statistic_data = video_result.get("statistics", {})
        content_details = video_result.get("contentDetails", {})
        if snippet:
            description = snippet.get("description")
            if check_copyright:
                copyrights = store.get_copyrights()
                for copyright in copyrights:
                    if copyright in description:
                        log.info("video [%s] have copyright" % video_id)
                        return None

            return {
                "description": snippet.get("description"),
                "title": snippet.get("title"),
                "img": snippet.get("thumbnails", {}).get("default", {}).get("url"),
                "id": video_id,
                "video_id": video_id,
                "channel_title": snippet.get("channelTitle"),
                "channel_id": snippet.get("channelId"),
                "licensed": content_details.get("licensedContent"),
                "yt_comments": int(statistic_data.get('commentCount', 0)),
                "yt_likes": int(statistic_data.get('likeCount', 0)),
                "yt_dislikes": int(statistic_data.get('dislikeCount', 0)),
                "yt_views": int(statistic_data.get('viewCount', 0)),
                "yt_favorites": int(statistic_data.get('favoriteCount', 0)),
                "yt_tags": snippet.get("tags"),
                "time": yt_date_to_tst(snippet.get("publishedAt")),
                "duration": get_duration(content_details.get('duration'))
                # "yt_data": video_result
            }


@net_tryings
def get_many_videos(youtube, video_ids, raw_result=False, part=None, exclude_fields=None, generator=False):
    video_ids = list(set(video_ids))
    _part = part or "snippet,statistics,contentDetails,id"
    for chunk in chunks(video_ids, 50):
        log.info("Will load from YT:\n%s" % ", ".join(chunk))
        cur_load_videos_data = youtube.videos().list(
            **{"id": ",".join(chunk), "part": _part}).execute()

        for video in cur_load_videos_data.get('items', []):
            if raw_result:
                parts = _part.split(',')
                video_data = {}
                for part in parts:
                    if part == 'id':
                        update = {'video_id': video.get(part)}
                    else:
                        update = video.get(part, {})
                    video_data.update(update)

                if exclude_fields:
                    for field in exclude_fields:
                        if field in video_data:
                            video_data.pop(field)

                for k, v in video.iteritems():
                    if 'count' in k.lower():
                        video[k] = int(v)

                if 'duration' in video:
                    video['duration'] = get_duration(video['duration'])
                if 'publishedAt' in video:
                    video['publishedAt'] = yt_date_to_tst(video['publishedAt'])
                if 'recordingDate' in video and video['recordingDate']:
                    video['recordingDate'] = yt_date_to_tst(video['recordingDate'])

            else:
                snippet = video.get("snippet", {})
                statistics = video.get("statistics", {})
                content_details = video.get("contentDetails", {})
                if snippet and statistics and content_details:
                    video_data = {
                        "video_id": video.get("id"),
                        "channel_id": snippet.get("channelId"),
                        "channel_title": snippet.get("channelTitle"),
                        "img": snippet.get("thumbnails", {}).get("default", {}).get("url"),
                        "title": snippet.get("title"),
                        "description": snippet.get("description"),
                        "yt_likes": int(statistics.get('likeCount', 0)),
                        "yt_views": int(statistics.get('viewCount', 0)),
                        "time": yt_date_to_tst(snippet.get("publishedAt")),
                        "duration": get_duration(content_details.get('duration'))
                    }
                else:
                    log.warning('Video [%s] have not data' % video.get('id'))
                    continue

            yield video_data


@net_tryings
def get_related_videos(yt, video_id):
    results = yt.search().list(
        type='video',
        relatedToVideoId=video_id,
        part='snippet',
        maxResults=50,
    ).execute()
    return filter(None, map(lambda x: x.get('id', {}).get('videoId'), results.get('items', [])))


@net_tryings
def get_my_channel_info(yt):
    results = yt.channels().list(
        part="snippet",
        mine="true"
    ).execute()
    items = results.get('items')
    if items:
        snippet = items[0].get('snippet')
        if snippet:
            return {"title": snippet.get("title"),
                    "description": snippet.get("description"),
                    "channel_id": items[0].get("id").get("channelId")}


if __name__ == '__main__':
    # video = get_video_info("tntBBFto6lU", part="snippet,contentDetails")
    # print video.get('duration')
    # print video.get('description')
    yt = get_youtube()
    print get_related_videos(yt, 'fyQAdn5fN4E')
