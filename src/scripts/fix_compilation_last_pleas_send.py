from src import CS_UPLOADED, CS_LAST_PLEAS_SENT
from src.db import VC_Storage

storage = VC_Storage("fix compilation script")

if __name__ == '__main__':
    compilation_video_id = raw_input("Please input video_id of your compilation:").strip()
    compilation = storage.compilations.find_one({"video_id": compilation_video_id})
    if not compilation:
        print "Compilation with video id [%s] are not found :( " % compilation_video_id
        exit(1)
    elif not compilation.get('last_pleas_send'):
        print "Compilation with video id [%s] is have not last please send state! And for all cuts of this, last pleases will be send!" % compilation_video_id
        exit(1)
    else:
        print "Found compilation: %s" % compilation

    result = storage.compilations.update_one(
        {"video_id": compilation_video_id},
        {
            "$unset": {CS_LAST_PLEAS_SENT: ""},
            "$set": {"state": "fix_lps"}
        }
    )
    print "Result of update compilation is: %s! \n And now you can press button for send last pleases send!" % result.raw_result