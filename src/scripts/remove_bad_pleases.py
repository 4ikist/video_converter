from src.db import VC_Storage

if __name__ == '__main__':
    store = VC_Storage()
    not_sended_pleases = store.pleases.find({"send": False})
    for pleas in not_sended_pleases:
        print pleas
        video = store.get_video(pleas.get('video_id'))
        store.videos.update_one({'video_id': pleas.get('video_id')}, {"$unset": {"categories": 1}})
        store.videos.update_one({"video_id": video.get('compilation_id')}, {"$unset": {'categories': 1}})
        store.pleases.delete_one({"video_id": pleas.get('video_id')})
        print "deleted"