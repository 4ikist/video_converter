from src import VT_COMPILATION
from src.db import VC_Storage
from src.yt.info import get_video_info


def load_compilations_data():
    store = VC_Storage()
    video_compilations = store.videos.find({"type": VT_COMPILATION, "deleted": {"$exists": False}})
    for vc in video_compilations:
        if not vc.get('licensed'):
            video = get_video_info(vc.get("video_id"), False, False)
            if video:
                store.store_video(video, VT_COMPILATION)
            else:
                print "not loaded info about %s" % vc.get('video_id')
        else:
            print "already downloaded %s" % vc.get('video_id')


if __name__ == '__main__':
    load_compilations_data()
