from src import PS_REJECTED, init_log
from src.db import VC_Storage
from src.yt import chunks
from src.yt.info import get_video_info

if __name__ == '__main__':
    store = VC_Storage()
    not_rejected_pleases = store.get_all_pleases([PS_REJECTED], {"video_id": 1})
    for i, chunk in enumerate(chunks(not_rejected_pleases, 500)):
        print "Process %s chunk" % i
        videos = store.get_videos(map(lambda x: x.get('video_id'), chunk))
        for video in videos:
            video_id = video.get('video_id')
            loaded_video = get_video_info(video_id, True, True)
            if not loaded_video:
                store.delete_video(video_id, {"because": "fixed, not checked (quality or copyright)"})
                print "BAD video: [%s] %s" % (video_id, video.get('title'))
