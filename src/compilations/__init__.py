import logging
from multiprocessing import Process, cpu_count

import time

from src import CS_PREPARED, CS_ERROR, CS_CONCATENATED, CS_UPLOADED
from src.convert.concat import concatenate_filter_complex, concatenate_filter_concat
from src.convert.cut import compilation_file_name
from src.core import load_and_cut_compilation_sources
from src.db import VC_Storage
from src.yt.upload import upload_video

log = logging.getLogger('compilations')

LOAD_AND_CUT = 'load_and_cut'
CONCATENATE = 'concatenate'
UPLOAD = 'upload'


def state_upd(state, error=None, state_data=None):
    upd = {state: {"ok": True, "time": time.time()}}
    if error:
        upd[state]['error'] = error
        upd[state]['ok'] = False
    else:
        upd["state"] = state

    if state_data and isinstance(state_data, dict):
        upd = dict(upd, **state_data)
    return upd


def task_upd(task):
    return {'task': {'name': task, 'at': time.time()}}


class CompilationService(Process):
    def __init__(self):
        super(CompilationService, self).__init__()

    def load_and_cut(self, compilation):
        try:
            cuts = load_and_cut_compilation_sources(compilation['cuts'])
        except Exception as e:
            log.exception(e)
            return 'error'

        if cuts and cuts[-1].get('filename') and cuts[-1].get('cut_filename'):
            self.store.update_compilation(compilation['_id'],
                                          dict({'cuts': cuts},
                                               **dict(state_upd(CS_PREPARED),
                                                      **task_upd(CONCATENATE))))
            return 'ok'
        else:
            self.store.set_compilation_state(compilation['_id'], CS_ERROR, error="some error at load and cut")
            return 'error'

    def concatenate(self, compilation):
        cuts, compilation_id = compilation.get("cuts"), compilation['_id']
        cut_files = filter(lambda x: x is not None, map(lambda x: x.get("cut_filename"), cuts))
        if not cuts or len(cut_files) != len(cuts):
            log.warning('No cuts, or not all have filenames :(')
            self.store.set_compilation_state(compilation_id, CS_ERROR, error="Not all have filenames or no cuts :(")
            return 'error'
        try:
            out_fn = concatenate_filter_complex(cut_files, compilation_file_name(compilation_id))
            if not out_fn:
                log.info('Try to concatenate [%s] by filter concat...' % compilation_id)
                out_fn = concatenate_filter_concat(cut_files, compilation_file_name(compilation_id),
                                                   remove_intermediate=True)
            if out_fn:
                upd = dict(
                    dict(
                        {'out_fn': out_fn},
                        **state_upd(CS_CONCATENATED)),
                    **task_upd(UPLOAD))
                self.store.update_compilation(compilation_id, upd)
                return 'ok'

            self.store.set_compilation_state(compilation_id, CS_ERROR, error="Can not concatenate :(")
        except Exception as e:
            self.store.set_compilation_state(compilation_id, CS_ERROR, error="Can not concatenate :( [%s]" % e)
        return 'error'

    def upload(self, compilation):
        fn, title, description = compilation.get("out_fn"), compilation.get("title"), compilation.get("description")
        if compilation and fn and title and description:
            id, err = upload_video(fn, title, description)
            if err or not id:
                self.store.set_compilation_state(compilation['_id'], CS_ERROR,
                                                 error=err or "Not video id for compilation:(")
                return 'error'

            self.store.set_compilation_state(compilation['_id'], CS_UPLOADED, state_data={"video_id": id})
            cut_video_ids = map(lambda x: x.get("video_id"), compilation.get("cuts"))
            self.store.remove_videos_from_compilator(cut_video_ids, compilation['_id'])
            # TODO send to sniffer? May be notify?
            return 'ok'
        else:
            self.store.set_compilation_state(compilation['_id'], CS_ERROR,
                                             error="compilation is undef or fn or title or description...")
        return 'error'

    def iterate(self):
        for task, compilation in self.store.get_compilations_with_tasks():
            log.info('Start processing task: %s for compilation: \n%s' % (task, compilation))
            assert compilation
            f = self.rules[task]
            self.store.imply_compilation_task(compilation.get('_id'))
            log.info('For compilation [%s] will start process task: %s' % (compilation.get('_id'), task))
            result = f(compilation)
            self.store.imply_compilation_task(compilation.get('_id'), end=True, result=result)
            log.info('%s ended with status: %s' % (task, result))

    def run(self):
        self.store = VC_Storage("compilations")
        self.rules = {
            LOAD_AND_CUT: self.load_and_cut,
            CONCATENATE: self.concatenate,
            UPLOAD: self.upload
        }

        self.iterate()
        log.info('Compilations service iterate END')


if __name__ == '__main__':
    store = VC_Storage('_compilations')
    cpus = 2
    services = []
    while 1:
        count = store.get_compilations_task_count()
        if count == 0:
            time.sleep(1)
            continue

        for i, cs in enumerate(services):
            try:
                cs.join(timeout=0.1)
                services.pop(i)
                break
            except Exception as e:
                pass

        if len(services) >= cpus:
            log.info('Will wait for less services...')
            time.sleep(5)
            continue
        else:
            log.info('Start service...')
            cs = CompilationService()
            cs.start()
            services.append(cs)

        time.sleep(1)
