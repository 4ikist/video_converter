import time
from rr_lib.db import DBHandler


class Service(DBHandler):
    def __init__(self):
        super(Service, self).__init__(connection_name='services')

        if 'services' not in self.collection_names:
            self.services = self.db.create_collection('services')
            self.services.create_index({"upd": 1}, expireAfterSeconds=3)
        else:
            self.services = self.db.get_collection('services')

    def upd(self, name):
        self.services.update_one({'name': name}, {'$set': {'upd': time.time()}}, upsert=True)
