import logging
import random

from src import VT_SOURCE, IS_TEST, PS_ASKED, PS_APPROVED, PS_ADD_VIDEO
from src.core import get_my_channel_id
from src.db import VC_Storage
from src.yt import get_youtube
from src.yt.comments import send_comment, send_reply, get_replies_of_me
from src.yt.info import get_channel_with_videos

log = logging.getLogger("pleas")

storage = VC_Storage("pleas")
yt = get_youtube("pleas")


def create_please_data(video_info, context=None):
    channel_id = video_info.get("channel_id")
    channel = storage.get_channel(channel_id)
    if not channel:
        channel, videos = get_channel_with_videos(channel_id, add_videos=False)
        storage.store_channel(channel_id, VT_SOURCE, channel)
    elif channel.get("pleas_reject"):
        log.info('Channel of this video [%s] has please reject attribute.' % channel_id)
        return None

    channel_videos = storage.get_videos_by_channel_id(channel_id)
    channel_pleases = map(lambda x: x.get("pt_id"),
                          storage.get_pleases(map(lambda x: x.get('video_id'), channel_videos)))

    all_pleases_ids = map(lambda x: x.get("pt_id"), storage.get_pleas_templates(finish=False, add_video=False))

    interested_pleases = set(all_pleases_ids).difference(channel_pleases)
    interested_pleases = filter(lambda x: x, interested_pleases)
    if not interested_pleases:
        interested_pleases = all_pleases_ids

    if not interested_pleases:
        log.warning("No please templates :(")
        raise Exception("No please templates")

    pt_id = random.choice(list(interested_pleases))
    template_text = storage.get_pleas_template(pt_id).get("content")
    if not context:
        context = {}
    log.info('Will create please by template [%s]\ntext: %s\ncontent:%s' % (pt_id, template_text, context))
    please_text = template_text % dict(video_info, **context)
    return {"pt_id": pt_id, "text": please_text}


def initialise_first_pleas(video):
    video_id = video.get('video_id')
    please = storage.get_pleas(video_id)
    if not please:
        please_data = create_please_data(video)
        if please_data:
            text, pt_id = please_data.get("text"), please_data.get("pt_id")
            return storage.store_pleas(video_id, text, pt_id, video.get('channel_id'))


def send_first_please(video, text):
    video_id = video.get('video_id')

    if IS_TEST:
        log.info("!!TEST!!\nSend comment channel_id: [%s] video_id: [%s]\n%s\n----------------------" % (
            video.get('channel_id'), video.get('video_id'), text))
        comment_id = "TEST"
    else:
        comment_id = send_comment(yt,
                                  video.get("channel_id"),
                                  video_id,
                                  text)
    return comment_id


def send_second_please(video_id, text):
    please = storage.get_pleas(video_id)
    parent_id = please.get("comment_id")
    if IS_TEST:
        log.info("!!TEST!!\nSend reply to comment: [%s] \n%s\n----------------------" % (
            parent_id, text))
    else:
        send_reply(yt, parent_id, text)
    load_and_store_new_pleas_replies([please])


def prepare_compilation_please(video_id, compilation_info):
    video = storage.get_video(video_id)

    please = random.choice(
        map(lambda x: x.get('content'), storage.get_pleas_templates(finish=True)) or
        ["See your video in my compilation: https://www.youtube.com/watch?v=%s" % compilation_info.get("video_id")]
    )
    text = please % dict(video,
                         **{"compilation_info":
                                "%(title)s (https://www.youtube.com/watch?v=%(video_id)s)" % compilation_info})

    storage.update_please_send_state(video_id, False, text)


def prepare_add_video_please(video):
    templates = map(lambda x: (x.get('pt_id'), x.get('content')), storage.get_pleas_templates(add_video=True)) or (
        None, '%(channel_title)s, thanks for permission to your video! I will use it in my compilation!')
    please_template = random.choice(
        templates
    )
    text = please_template[1] % video
    video_id = video.get('video_id')
    storage.store_pleas(video_id, text, please_template[0], video.get('channel_id'))
    storage.set_pleas_state(video_id, PS_ADD_VIDEO)


def is_have_not_reads_replies(please):
    dialog = please.get("dialog", [])
    for reply in dialog:
        if reply.get("not_read"):
            return True


def load_and_store_new_pleas_replies(pleases):
    comment_ids = filter(lambda x: x is not None, map(lambda x: x.get('comment_id'), pleases))
    if not comment_ids:
        return pleases
    all_replies = get_replies_of_me(yt, comment_ids)
    my_channel = get_my_channel_id(with_info=True)

    for i, please in enumerate(pleases):
        stored_replies = dict(map(lambda x: (x.get("pr_id"), x), please.get("dialog", [])))
        loaded_replies = all_replies.get(please.get('comment_id'))
        if loaded_replies:
            loaded_replies.sort(cmp=lambda x, y: int(x.get("time") - y.get("time")))
            dialog = []
            for reply in loaded_replies:
                if reply.get('owner_channel_id') in (please.get('channel_id'), my_channel.get('channel_id')):
                    if reply.get("pr_id") not in stored_replies:
                        reply['not_read'] = True
                        storage.set_pleas_dialog_step(please.get('video_id'), reply)
                        log.info("Store new reply for %s\n%s" % (please.get('video_id'), reply))
                        dialog.append(reply)
                    else:
                        dialog.append(stored_replies[reply.get("pr_id")])

            pleases[i]['dialog'] = dialog
            log.info("For please [%s] dialog added [%s]" % (pleases[i]['video_id'], len(dialog)))

    return pleases


if __name__ == '__main__':
    channels = storage.channels.find({'pleas_reject': {'$exists': False}})
    for channel in channels:
        video = storage.get_videos_by_channel_id(channel.get('channel_id'))
        if video:
            create_please_data(video[0])
