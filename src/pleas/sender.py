import random
from time import sleep

from src import PS_ASKED, PS_APPROVED, PS_REJECTED, init_log, VT_SOURCE, IS_TEST, CS_LAST_PLEAS_SENT, CS_ERROR, \
    PS_CLOSED, PS_ADD_VIDEO
from src.db import VC_Storage
from src.pleas import send_first_please, send_second_please
from src.yt.info import get_video_info

log = init_log("please_sender", stdout=False)


def sleep_time(chars_count):
    return chars_count * 0.25 * random.random()


def sleep_before_send(text):
    st = sleep_time(len(text))
    if not IS_TEST:
        log.info("Wait while typing in keyboard at %s..." % st)
        sleep(st)


class _comps_to_cuts_mapping:
    cut_id_to_comp_ids = {}
    comp_id_to_comp = {}

    def update(self, cut_id, compilation_id=None):
        comp_ids = self.cut_id_to_comp_ids.get(cut_id, [])
        for comp_id in comp_ids:
            comp = self.comp_id_to_comp.get(comp_id, {})
            if compilation_id and comp_id != compilation_id:
                continue
            for i, cut in enumerate(comp.get('cuts', [])):
                if cut['video_id'] == cut_id:
                    self.comp_id_to_comp[comp_id]['cuts'][i]['last_pleas_sent'] = True

    def check(self, cut_id):
        result = []
        for comp_id in self.cut_id_to_comp_ids[cut_id]:
            for cut in self.comp_id_to_comp[comp_id]['cuts']:
                result.append(cut.get('last_pleas_sent', False) != False)

        return all(result)

    def add(self, cut_id, compilation):
        comp_id = compilation.get('compilation_id')
        if cut_id not in self.cut_id_to_comp_ids:
            self.cut_id_to_comp_ids[cut_id] = []

        if comp_id not in self.cut_id_to_comp_ids[cut_id]:
            self.cut_id_to_comp_ids[cut_id].append(comp_id)

        if comp_id not in self.comp_id_to_comp:
            self.comp_id_to_comp[comp_id] = compilation

            for cut in compilation.get('cuts'):
                self.add(cut.get('video_id'), compilation)

    def clear(self):
        self.cut_id_to_comp_ids = {}
        self.comp_id_to_comp = {}

    def items(self):
        for cut_id, comp_ids in self.cut_id_to_comp_ids.items():
            yield cut_id, [self.comp_id_to_comp[comp_id] for comp_id in comp_ids]


class PleaseSender():
    _not_sent_last_pleas_comps = _comps_to_cuts_mapping()

    def __init__(self, storage=VC_Storage()):
        self.storage = storage

    def ensure_not_sent_last_please_compilations(self, video):
        our_compilations = video.get("our_compilations")
        if not our_compilations:
            return

        result = False
        for our_compilation in our_compilations:
            compilation = self.storage.get_compilation(our_compilation)
            if not compilation.get("last_pleas_sent"):
                self._not_sent_last_pleas_comps.add(video.get('video_id'), compilation)
                result = True

        return result

    def check_last_pleas_sent_at_comp(self, video_id):
        return

    def clear_and_fix_compilations_last_pleas_sent(self):
        last_pleas_send_compilation_ids = set()

        for video_id, compilations in self._not_sent_last_pleas_comps.items():
            for compilation in compilations:

                comp_id = compilation.get('compilation_id')
                if comp_id in last_pleas_send_compilation_ids:
                    continue

                not_sent_pleases = []
                for cut in compilation.get('cuts'):
                    cut_video_id = cut.get('video_id')
                    comp_cut_pleas = self.storage.get_pleas(cut_video_id)
                    if comp_cut_pleas.get('send'):
                        self._not_sent_last_pleas_comps.update(cut_video_id, compilation.get('compilation_id'))
                    else:
                        not_sent_pleases.append(cut_video_id)

                if self._not_sent_last_pleas_comps.check(video_id):
                    log.info("All pleases for compilation [%s] sent!" % comp_id)
                    self.storage.set_compilation_state(comp_id, CS_LAST_PLEAS_SENT)
                    last_pleas_send_compilation_ids.add(comp_id)
                else:
                    log.error("ERROR! Not all last pleases sent at compilation [%s]\n%s" % (comp_id, not_sent_pleases))
                    self.storage.set_compilation_state(comp_id, CS_ERROR, {"not_sent_pleases": not_sent_pleases})

        self._not_sent_last_pleas_comps.clear()

    def process_pleas(self, pleas):
        state, video_id, next_text = \
            pleas.get("state"), pleas.get("video_id"), pleas.get("next_text") or pleas.get('text')
        video = self.storage.get_video(video_id)

        log.info("\nProcess pleas:\n[%s] [%s] \n%s\nUsed in our compilations: %s" % (
            video_id, state, next_text, video.get("our_compilations")))
        try:
            if state and video_id and next_text:
                if state in (PS_ASKED, PS_ADD_VIDEO):
                    if pleas.get("dialog") and pleas.get("comment_id"):
                        log.info("I send second pleas for [%s]" % video_id)
                        sleep_before_send(next_text)
                        send_second_please(video_id, next_text)
                        self.storage.update_please_send_state(video_id, True)
                        log.info(
                            "OK! It have dialog and i will send simple message, because i have comment id: [%s]\n" % pleas.get(
                                "comment_id"))
                    else:
                        log.info("I send first please for [%s]!" % video_id)
                        if not video:
                            video = get_video_info(video_id)
                            self.storage.store_video(video, VT_SOURCE)
                        sleep_before_send(next_text)
                        comment_id = send_first_please(video, next_text)
                        if comment_id:
                            self.storage.update_please_send_state(video_id, True, comment_id=comment_id)
                            log.info("OK! First pleas was sent! Storing info...\n")
                        else:
                            self.storage.set_pleas_state(video_id, PS_REJECTED)
                            self.storage.update_please_send_state(video_id, "error",
                                                                  error="No comment id in first pleas")
                            log.warning(
                                "ERR! Something went wrong i have not comment id in first please :(\n%s\n%s" % (
                                    pleas, video))

                elif state == PS_APPROVED:
                    ensured = self.ensure_not_sent_last_please_compilations(video)
                    if not ensured:
                        log.info("ERR! Last pleases sent for this video [%s]" % video_id)
                        self.storage.update_please_send_state(video_id, PS_CLOSED)
                        return

                    if video_id and next_text:
                        sleep_before_send(next_text)
                        send_second_please(video_id, next_text)
                        self.storage.update_please_send_state(video_id, True)
                        self._not_sent_last_pleas_comps.update(video_id)
                        log.info("OK! Send approved please %s! \n" % video_id)
                    else:
                        error = 'No video id or next text'
                        self.storage.update_please_send_state(video_id, 'error', error=error)
                        log.error("ERR! %s for [%s] :(\n%s\n%s" % (error, video_id, pleas, video))
                else:
                    self.storage.update_please_send_state(video_id, 'error', error="Bad state at pleas")
                    log.warning("ERR! \n[%s]\n is not normal!" % pleas)
            else:
                self.storage.update_please_send_state(video_id, 'error',
                                                      error="Not enough data for send this please")
                log.error("ERR! Bad pleas: \n%s\n%s" % (pleas, video))
        except Exception as e:
            log.exception(e)
            log.error('Something went wrong. Talk me about it...')

    def work(self):
        while 1:

            for pleas in self.storage.get_please_not_sended():
                self.process_pleas(pleas)

            self.clear_and_fix_compilations_last_pleas_sent()

            sleep(5)


if __name__ == '__main__':
    ps = PleaseSender()
    ps.work()
