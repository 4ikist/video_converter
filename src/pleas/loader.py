import logging

from time import sleep

from src import PS_ASKED, init_log
from src.db import VC_Storage
from src.pleas import load_and_store_new_pleas_replies

if __name__ == '__main__':
 # TODO mmove to please
    log = init_log("please_loader", without_yt=False, stdout=False)
    store = VC_Storage('please loader')

    while 1:
        asked_not_replied = store.get_pleases_with_state(PS_ASKED, with_dialog=False)
        log.info("Found asked and not replied pleases: %s" % len(asked_not_replied))
        load_and_store_new_pleas_replies(asked_not_replied)
        log.info("Will sleep")
        sleep(60 * 10)
