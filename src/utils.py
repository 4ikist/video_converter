import re
from collections import Counter
from datetime import datetime

import operator


def tst_to_dt(value):
    if not value: return '-'
    dt_format = "%H:%M:%S"
    dt = datetime.fromtimestamp(value)
    if (datetime.now() - dt).days >= 1:
        dt_format += "<small> %d.%m.%Y </small>"
    return dt.strftime(dt_format)


def array_to_string(array):
    if isinstance(array, list):
        return ", ".join([str(el) for el in array])
    return array


def levenshtein_distance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2 + 1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]


class Completer(object):  # Custom completer

    def __init__(self, options):
        self.options = set(options)

    def complete(self, text):
        if text:  # cache matches (entries that start with entered text)
            distances = dict([(s,

                               levenshtein_distance(text, s) - 10 if s.startswith(text) else 0

                               ) for s in self.options])
            distances = map(lambda x: x[0], sorted(distances.items(), key=operator.itemgetter(1)))
            return distances[:10]

        matches = list(self.options)
        return matches[:50]


class CompleterDict(Completer):
    def __init__(self, options):
        super(CompleterDict, self).__init__(options.values())
        self.option_keys = dict(map(lambda x: (x[1], x[0]), options.iteritems()))

    def complete(self, text):
        result_values = super(CompleterDict, self).complete(text)
        return map(lambda x: (self.option_keys[x], x), result_values)


if __name__ == '__main__':
    a, b = "foo", "foo bar"
    print a, b, levenshtein_distance(a, b)
    print a, b, levenshtein_distance(b, a)
    a, b = "aaa", "avraam"
    print a, b, levenshtein_distance(a, b)
    print a, b, levenshtein_distance(b, a)
    a, b = "foo", "foo"
    print a, b, levenshtein_distance(a, b)
    print a, b, levenshtein_distance(b, a)
    a, b = "foo", "ofoo"
    print a, b, levenshtein_distance(a, b)
    print a, b, levenshtein_distance(b, a)
    a, b = "foo", "gooo"
    print a, b, levenshtein_distance(a, b)
    print a, b, levenshtein_distance(b, a)
