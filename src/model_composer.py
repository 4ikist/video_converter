import time

from src import VT_COMPILATION
from src.core import get_my_channel_id
from src.db import DBHandler, VC_Storage
from src.yt import get_youtube, duration_obj_to_seconds, get_duration, yt_date_to_tst, chunks
from src.yt.info import get_channel_with_videos, get_many_videos, get_related_videos

yt = get_youtube()
store = VC_Storage()

time_step = 3600


class ModelStorage(DBHandler):
    def __init__(self, clear=False):
        super(ModelStorage, self).__init__()

        if clear:
            self.db.drop_collection('model_data')

        if 'model_data' not in self.collection_names:
            self.model_data = self.db.create_collection('model_data')
            self.model_data.create_index('video_id')
            self.model_data.create_index('time')
        else:
            self.model_data = self.db.get_collection('model_data')

    def add_video_data(self, data):
        data['time'] = (int(time.time()) / time_step) * time_step
        self.model_data.update_one({'video_id': data.get('video_id'), 'time': data['time']},
                                   {'$set': data},
                                   upsert=True)
        # self.model_data.insert_one(data)

    def get_all_video_ids(self):
        result = self.model_data.find({}, projection={'video_id': True})
        return list(set(map(lambda x: x.get('video_id'), result)))

    def get_all_related_videos(self):
        relateds = self.model_data.find({}, projection={'related': True})
        result = []
        for related in relateds:
            result.extend(related.get('related'))
        return list(set(result))


ms = ModelStorage()


def __fix_data():
    for entity in ms.model_data.find({'liveBroadcastContent': {'$exists': True}}):
        update = {'liveBroadcastContent': 1}
        ms.model_data.update_one({'video_id': entity.get('video_id'), 'time': entity.get('time')}, {'$unset': update})

    print 'data fixed...'


def form_channels():
    my_channel = get_my_channel_id()
    donors = store.get_channels(is_deleted=None, is_rejected=None, projection={'channel_id': True})
    return [my_channel] + map(lambda x: x.get('channel_id'), donors)


def retrieve_videos_and_related(videos):
    related_videos = set()
    stored_videos = set()

    def get_videos(videos):
        return get_many_videos(yt, videos, raw_result=True,
                               part='contentDetails,recordingDetails,snippet,statistics,status,topicDetails,id',
                               exclude_fields=['thumbnails', 'localized', 'dimension', 'privacyStatus', 'projection',
                                               'uploadStatus', 'regionRestriction', 'liveBroadcastContent'])

    for video in get_videos(list(videos)):
        video['related'] = get_related_videos(yt, video.get('video_id'))
        ms.add_video_data(video)
        # print 'STORED RELATED: \n%s\n------------\n' % '\n'.join(['%s\t%s' % (k, v) for k, v in video.items()])
        related_videos.update(set(video['related']))
        stored_videos.add(video.get('video_id'))

    return stored_videos, related_videos


def model_compose(channels):
    stored, related = set(), set()

    for channel in channels:
        print 'Start retrieve channel %s' % channel
        _, videos = get_channel_with_videos(channel)
        ch_stored, ch_related = retrieve_videos_and_related(map(lambda x: x.get('video_id'), videos))
        stored.update(ch_stored)
        related.update(ch_related)

    not_stored = related.difference(stored)
    retrieve_videos_and_related(list(not_stored))


def next_step_by_related_videos():
    related = ms.get_all_related_videos()
    stored = ms.get_all_video_ids()
    not_stored = set(related).difference(set(stored))
    print "Not stored: %s" % len(not_stored)
    retrieve_videos_and_related(list(not_stored))


def get_related_depth(channels, depth=1):
    stored, related = set(), set()
    for channel in channels:
        _, videos = get_channel_with_videos(channel)
        video_ids = map(lambda x: x.get('video_id'), videos)
        ch_stored, ch_related = retrieve_videos_and_related(video_ids)
        stored.update(ch_stored)
        related.update(ch_related)
        related_not_stored = related.difference(stored)
        for i in range(depth):
            r_stored, r_related = retrieve_videos_and_related(list(related_not_stored))
            stored.update(r_stored)
            related_not_stored = r_related.difference(stored)


def append_times():
    with_less_time = ms.model_data.aggregate([
        {'$project': {'video_id': 1, 'time': 1, 'viewCount': 1}},
        {'$group': {'_id': '$video_id', 'count': {'$sum': 1}}},
        {'$match': {'count': {'$lte': 2}}}
    ])
    r, s = set(), set()
    with_less_time = list(with_less_time)
    print "videos with times less than 2 is %s" % len(with_less_time)
    for chunk in chunks(with_less_time, 150):
        videos = map(lambda x: x.get('video_id'), chunk)
        related, stored = retrieve_videos_and_related(videos)
        s.update(set(stored))
        r.update(set(related))
        related_not_loaded = r.difference(s)
        related, stored = retrieve_videos_and_related(related_not_loaded)
        s.update(set(stored))
        r.update(set(related))


if __name__ == '__main__':
    # __fix_data()
    # ModelStorage(clear=False)
    # model_compose(form_channels())
    # next_step_by_related_videos()
    # next_step_by_related_videos()
    # next_step_by_related_videos()
    get_related_depth(form_channels(), 1)
    append_times()
