from src import VT_COMPILATION, VT_SOURCE
from src.db import VC_Storage

store = VC_Storage("categories test")

video_compilation_id = "TEST_ID"


def test_video_categories_save():
    print store.store_video({"video_id": video_compilation_id, "description": "bla bla bla", "etc": "foo bar baz"},
                            VT_COMPILATION)
    video_source0 = {"compilation_id": video_compilation_id, "video_id": video_compilation_id + "0"}
    store.store_video(video_source0, VT_SOURCE)
    video_source1 = {"compilation_id": video_compilation_id, "video_id": video_compilation_id + "1"}
    store.store_video(video_source1, VT_SOURCE)
    video_source2 = {"compilation_id": video_compilation_id, "video_id": video_compilation_id + "2"}
    store.store_video(video_source2, VT_SOURCE)
    video_source3 = {"compilation_id": video_compilation_id, "video_id": video_compilation_id + "3"}
    store.store_video(video_source3, VT_SOURCE)

    store.set_video_compilation_categories(video_compilation_id, ["cat1", "cat2", "cat3"])

    stored_cat1 = store.get_source_videos("cat1")
    assert stored_cat1[0]['compilation_id'] == video_compilation_id
    assert video_compilation_id + "2" in map(lambda x: x.get("video_id"), stored_cat1)


if __name__ == '__main__':
    test_video_categories_save()
