from src import VT_SOURCE, PS_APPROVED
from src.db import VC_Storage

store = VC_Storage()


def test_video_moving_compilator():
    video_id = 'test_video_id'
    video_data = {"video_id": video_id, 'title': "test", 'description': '...', 'another_data': '...'}
    compilation_title = "test compilation"

    store.videos.delete_one({"video_id": video_id})
    store.compilations.delete_one({"title": compilation_title})

    store.store_video(video_data, VT_SOURCE)

    store.store_pleas(video_id, 'some test pleas', 'test_comment_id', 'no pleas template')
    store.set_pleas_state(video_id, PS_APPROVED)

    store.add_video_source_categories(video_id, ['test_category'])
    videos = store.get_source_videos(category='test_category', pleas_state=PS_APPROVED, in_compilator=True)
    assert videos[0]['another_data'] == '...'
    assert videos[0]['description'] == '...'
    compilation_id = store.store_compilation(compilation_title, [], "...")
    store.remove_videos_from_compilator([video_id], compilation_id)

    videos = store.get_source_videos(category='test_category', pleas_state=PS_APPROVED, in_compilator=True)
    assert not videos
    videos = store.get_source_videos(pleas_state=PS_APPROVED, in_compilator=False)
    assert videos[0]['another_data'] == '...'
    assert videos[0]['description'] == '...'

    store.move_videos([video_id])

    videos = store.get_source_videos(category='test_category', pleas_state=PS_APPROVED, in_compilator=True)
    assert videos[0]['another_data'] == '...'
    assert videos[0]['description'] == '...'

    store.videos.delete_one({"video_id": video_id})
    store.compilations.delete_one({"title": compilation_title})


def add_test_videos_not_in_compilator():
    video_ids = []
    for i in range(30):
        video_id = 'test_video_id %s' % i
        video_data = {"video_id": video_id, 'title': "test video %s"%i, 'description': '... %s' % i, 'another_data': '... %s' % i,
                      'yt_views': i, 'yt_likes': i,
                      "test": True}
        store.videos.delete_one({"video_id": video_id})
        store.store_video(video_data, VT_SOURCE)

        store.store_pleas(video_id, 'some test pleas', 'test_comment_id', 'no pleas template')
        store.set_pleas_state(video_id, PS_APPROVED)

        store.add_video_source_categories(video_id, ['test_category'])

        video_ids.append(video_id)

    compilation_id1 = store.store_compilation("test_compilation1", [], "...")
    compilation_id2 = store.store_compilation("test_compilation2", [], "...")
    compilation_id3 = store.store_compilation("test_compilation3", [], "...")
    store.remove_videos_from_compilator(video_ids, compilation_id1)
    store.remove_videos_from_compilator(video_ids[2:5], compilation_id2)
    store.remove_videos_from_compilator(video_ids[3:], compilation_id3)

def test_cuts():
    st = VC_Storage()
    st.store_video_cut({"video_id": 'tttteeeesssstttt', "data": {"test": True}}, 100, 120, 'some cat')
    assert st.is_cut_exists("tttteeeesssstttt", 100, 120)
    st.store_video_cut({"video_id": 'tttteeeesssstttt', "data": {"test": True}}, 110, 130, 'some cat 2')
    assert st.is_cut_exists("tttteeeesssstttt", 110, 130)

    st.delete_video_cut("tttteeeesssstttt", 100, 120)
    st.update_video_cut("tttteeeesssstttt", 110, 130, 'some cat')
    video = st.get_video("tttteeeesssstttt")
    assert len(video.get('cut_times')) == 1
    assert video.get('cut_times')[0]['category'] == 'some cat'
    assert video.get('cut_times')[0]['from'] == 110
    assert video.get('cut_times')[0]['to'] == 130


    st.store_video_cut({"video_id": 'tttteeeesssstttt', "data": {"test": True}}, 110, 131, 'some cat 2')
    assert st.is_cut_exists("tttteeeesssstttt", 110, 131)
    video = st.get_video("tttteeeesssstttt")
    assert len(video.get('cut_times')) == 2
    assert video.get('cut_times')[1]['category'] == 'some cat 2'
    assert video.get('cut_times')[1]['from'] == 110
    assert video.get('cut_times')[1]['to'] == 131

    assert st.is_cut_exists("tttteeeesssstttt", 110, 130)
    assert st.is_cut_exists("tttteeeesssstttt", 110, 131)
    assert not st.is_cut_exists("tttteeeesssstttt", 100, 120)

    st.delete_video_cut("tttteeeesssstttt", 110, 130)
    assert not st.is_cut_exists("tttteeeesssstttt", 110, 130)
    st.delete_video_cut("tttteeeesssstttt", 110, 131)
    assert not st.is_cut_exists("tttteeeesssstttt", 110, 131)
    video = st.get_video("tttteeeesssstttt")
    assert len(video.get('cut_times')) == 0


    st.delete_video("tttteeeesssstttt", real=True)



if __name__ == '__main__':
    # add_test_videos_not_in_compilator()
    test_cuts()