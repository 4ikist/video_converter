from src.db import VC_Storage

many_ids = ['z13ohntzjuidc3nzu04cfr14aq2sxjoqys40k', 'z12svrfgvvekihric04cfr14aq2sxjoqys40k',
            'z13dh1dwvyz0zhjso22xsd1pkqbfx5rq104', 'z12pe1rggmeueniyn04cfr14aq2sxjoqys40k',
            'z13djtmbbruft5xak04cfr14aq2sxjoqys40k', 'z13bjj3hdwajyrten22xsd1pkqbfx5rq104',
            'z13aixfb0nnyunc3w22xsd1pkqbfx5rq104', 'z12jxj1qsxbryhhgi04cfr14aq2sxjoqys40k',
            'z13dzfsopzubxlx3422xsd1pkqbfx5rq104', 'z13pjpd4bsmtixj2d22xsd1pkqbfx5rq104',
            'z12odvsh5zuszvzae04cfr14aq2sxjoqys40k', 'z13rs5a5stjyilheq22xsd1pkqbfx5rq104',
            'z12nhhchqxqjebajs04cfr14aq2sxjoqys40k', 'z120z5uiowu1srzug22xsd1pkqbfx5rq104',
            'z134hpvqbovgdxwhi04cfr14aq2sxjoqys40k', 'z12cvdoymq20vz4wl04cfr14aq2sxjoqys40k',
            'z12eunrxmyf2ttjqd04cfr14aq2sxjoqys40k', 'z12lgrg4mne2u35xa04cfr14aq2sxjoqys40k',
            'z13rvfwxzl2jsfn1522xsd1pkqbfx5rq104', 'z12szjgoytj2tx5h004cfr14aq2sxjoqys40k',
            'z12zthtrzsnehxw1t22xsd1pkqbfx5rq104', 'z12rujnidn2qhpwo404cfr14aq2sxjoqys40k',
            'z13cixfivou2s1lm222xsd1pkqbfx5rq104', 'z12bdhgzirvuwvij104cfr14aq2sxjoqys40k',
            'z12jjdqhxxbuidefg22xsd1pkqbfx5rq104', 'z12ff3raovatjtxgr04cfr14aq2sxjoqys40k',
            'z134ghqpnpezzb5l122xsd1pkqbfx5rq104', 'z13vur5jqrbagxcdl22xsd1pkqbfx5rq104',
            'z13lhtzqixzhjxqqw04cfr14aq2sxjoqys40k', 'z12iwbcpjszqxtb1u22xsd1pkqbfx5rq104',
            'z12yivlrltzucj3v222xsd1pkqbfx5rq104', 'z123dbshptymdnrdi22xsd1pkqbfx5rq104',
            'z12xxd4wlqetubzpr04cfr14aq2sxjoqys40k', 'z13aebt5mvjjfrkkb22xsd1pkqbfx5rq104',
            'z13kjjqwowyhelnpf04cfr14aq2sxjoqys40k', 'z13wvhtjqzu4xzqnd22xsd1pkqbfx5rq104',
            'z12hd1aq1ufiwb4v422xsd1pkqbfx5rq104', 'z12gsr15ooydepicw22xsd1pkqbfx5rq104',
            'z12gjr1zmwexsnylh22xsd1pkqbfx5rq104', 'z13xv3noxzmoc322m22xsd1pkqbfx5rq104',
            'z13zytroaybyxfsn422xsd1pkqbfx5rq104', 'z12bjrd5tz21zb1ji04cfr14aq2sxjoqys40k',
            'z131wf0bys3nxtv5x04cfr14aq2sxjoqys40k', 'z13eedlrmmjhwptqu04cfr14aq2sxjoqys40k',
            'z13csdbiekunhxprv04cfr14aq2sxjoqys40k', 'z12osffbsxfucpyme22xsd1pkqbfx5rq104',
            'z12rvp3b1lulhpnub22xsd1pkqbfx5rq104', 'z13qftmzvubixz0lw22xsd1pkqbfx5rq104',
            'z12bzrhp1yb5ubcvf22xsd1pkqbfx5rq104', 'z12bebj45k2qerien22xsd1pkqbfx5rq104',
            'z13yzxgjaqvfjzll022xsd1pkqbfx5rq104', 'z13ui5jizseagzs2n22xsd1pkqbfx5rq104']

store = VC_Storage()
if __name__ == '__main__':
    # print len(many_ids)
    # one_id = 'z12svrfgvvekihric04cfr14aq2sxjoqys40k'
    # print comment_replies(many_ids[:50])
    for comment_id in many_ids:
        pleas = store.pleases.find_one({"comment_id": comment_id})
        print len(comment_id), comment_id, pleas
