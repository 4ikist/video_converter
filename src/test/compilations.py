from src import VT_SOURCE
from src.db import VC_Storage

from src.test import TEST_VIDEO_ID, TEST_VIDEO_IDS
from src.yt import get_youtube
from src.yt.info import get_video_info

store = VC_Storage()
yt = get_youtube()

def create_test_empty_compilation():
    store.store_compilation("test", [], "test description")


def create_test_compilation_with_cuts():
    compilation_id = store.store_compilation("test", [{"video_id": 'gvDzN3PQcAY'}], "test description")
    store.set_compilation_uploaded(compilation_id, TEST_VIDEO_ID)


def remove_all_compilations():
    store.compilations.delete_many({})


def create_test_compilations():
    for video_id in TEST_VIDEO_IDS:
        please = store.get_pleas(video_id)
        if please:
            continue
        video = get_video_info(video_id, check_quality=False)
        store.store_video(video, VT_SOURCE)
        # send_first_please(video)

    for i, video_id in enumerate(TEST_VIDEO_IDS):
        compilation_id = store.store_compilation("test compilation %s" % i, [{"video_id": x} for x in TEST_VIDEO_IDS],
                                                 "test description %s" % i)
        store.set_compilation_uploaded(compilation_id, video_id)


if __name__ == '__main__':
    remove_all_compilations()
    # create_test_empty_compilation()
    # create_test_compilation_with_cuts()
    create_test_compilations()