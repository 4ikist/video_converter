from src import VT_SOURCE
from src.db import VC_Storage
from src.pleas import create_please_data, load_and_store_new_pleas_replies, prepare_compilation_please
from src.test import TEST_VIDEO_IDS
from src.yt.info import get_video_info

store = VC_Storage("please test")


def form_please_on_template():
    video = store.get_video("gvDzN3PQcAY")
    return create_please_data(video)


def replies(video_id):
    please = store.get_pleas(video_id)
    please = load_and_store_new_pleas_replies([please])
    print please[0]


def test_finish_pleases(video_id):
    compilation = {"video_id": "TEST", "title": "TEST TETLE"}
    prepare_compilation_please(video_id, compilation)


def test_unread(video_id):
    # prepare
    store.pleases.update_one({"video_id": video_id}, {"$unset": {"dialog": True}})
    please = store.get_pleas(video_id)
    new_please = load_and_store_new_pleas_replies([please])
    reply = new_please[0].get('dialog')[0]

    store.set_pleas_read(video_id, reply.get('pr_id'))

    please = store.get_pleas(video_id)
    new_please = load_and_store_new_pleas_replies([please])
    reply = new_please[0].get('dialog')[0]
    assert reply.get('not_read') is None


def remove_dialog(video_id):
    store.pleases.update_one({"video_id": video_id}, {"$unset": {"dialog": True}})


def create_test_pleases():
    store.videos.delete_many({"video_id": {"$in": TEST_VIDEO_IDS}})
    store.pleases.delete_many({"video_id": {"$in": TEST_VIDEO_IDS}})

    for video_id in TEST_VIDEO_IDS:
        video = get_video_info(video_id, check_quality=False, check_copyright=False)
        store.store_video(video, VT_SOURCE)
        # send_first_please(video)




if __name__ == '__main__':
    test_unread('gvDzN3PQcAY')
    test_finish_pleases('test')
    remove_dialog('gvDzN3PQcAY')
    remove_dialog('U1-UHnr4bYU')
    remove_dialog('-3QVURwvr6c')
    create_test_pleases()
