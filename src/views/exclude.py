import json

from flask import render_template, request, jsonify, flash
from flask.views import MethodView

from src import CHANNEL_ID_LENGTH, VT_SOURCE, VT_EXCLUDE, VIDEO_ID_LENGTH
from src.db import VC_Storage
from src.yt.info import get_video_info, get_channel_with_videos

store = VC_Storage("exclude view")


def check_copyrights(copyrights):
    bad_videos = []
    for video in store.get_source_videos(in_compilator=None, as_list=False):
        for copyright in copyrights:
            if copyright in video.get('description'):
                bad_videos.append(video)
                store.delete_video(video.get('video_id'), {"copyright": True})
                store.delete_pleas(video.get('video_id'))
    return bad_videos


class ExcludeAPI(MethodView):
    methods = ["POST", "GET", "PUT", "DELETE"]

    def post(self):
        exclude_content = request.form.get("content").strip()
        if len(exclude_content) == VIDEO_ID_LENGTH:
            video = store.get_video(exclude_content)
            if not video:
                video = get_video_info(exclude_content, False, False)
            if video:
                store.store_video(video, VT_EXCLUDE)
                store.set_video_exclude(exclude_content)

        elif len(exclude_content) == CHANNEL_ID_LENGTH:
            channel_info = store.get_channel(exclude_content)
            if not channel_info:
                channel_info, _ = get_channel_with_videos(exclude_content, add_videos=False)
            if channel_info:
                store.store_channel(exclude_content, VT_SOURCE, channel_info)
                store.set_channel_pleas_reject(exclude_content)
        else:
            flash("Can not recognise video id or channel id")

        channels = store.get_channels_pleas_reject()
        videos = store.get_videos_exclude()
        return render_template("exclude.html", channels=channels, videos=videos)

    def put(self):
        data = json.loads(request.data)
        copyrights = filter(lambda x: x, map(lambda x: x.strip(), data.get('copyrights').split()))
        if copyrights:
            diff = store.set_copyrights(copyrights)
            bad_videos = check_copyrights(diff)
            return jsonify(
                **{"ok": True, "result": "Found and delete %s videos and pleases in your db..." % (len(bad_videos))})
        return jsonify(**{"error": "No copyrights found :( "})

    def get(self):
        channels = store.get_channels_pleas_reject()
        deleted_channels = store.get_channels(channel_type=None, is_deleted=True)

        videos = store.get_videos_exclude()
        copyrights = store.get_copyrights()
        return render_template("exclude.html", channels=channels, videos=videos, copyrights=copyrights,
                               deleted_channels=deleted_channels)

    def delete(self):
        data = json.loads(request.data)
        channel_id = data.get("channel_id")
        store.set_channel_pleas_reject(channel_id, False)
        store.restore_channel(channel_id)
        return jsonify(**{"ok": True})
