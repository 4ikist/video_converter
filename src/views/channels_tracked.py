from flask import flash, jsonify
from flask import render_template
from flask import request
from flask.views import MethodView

from src import CHANNEL_ID_LENGTH, VT_SOURCE
from src.db import VC_Storage
from src.yt.info import get_channel_with_videos

store = VC_Storage("channels tracked view")


class ChannelsTrackedAPI(MethodView):
    methods = ["POST", "GET", "DELETE"]

    def post(self):
        channel_id = request.form.get("channel_id").strip()
        if len(channel_id) != CHANNEL_ID_LENGTH:
            flash("Can not recognise channel id :(")
        else:
            channel, _ = get_channel_with_videos(channel_id, add_videos=False)
            store.store_channel(channel_id, VT_SOURCE, channel)
            store.set_channel_tracking(channel_id, track=True)

        channels = store.get_channels_tracked(is_die=False)
        died = store.get_channels_tracked(is_die=True)
        return render_template("channels_tracked.html", channels=channels, died=died)

    def delete(self, channel_id):
        store.set_channel_tracking(channel_id, track=False)
        store.delete_channel(channel_id)
        return jsonify(**{"ok": True})

    def get(self):
        channels = store.get_channels_tracked(is_die=False)
        died = store.get_channels_tracked(is_die=True)
        return render_template("channels_tracked.html", channels=channels, died=died)
