import json
import logging
import re

from flask import jsonify
from flask import request

from flask.templating import render_template
from flask.views import MethodView

from src import VT_COMPILATION
from src.core import get_sources_ids_of_compilation
from src.db import VC_Storage
from src.yt import get_youtube
from src.yt.comments import get_comments
from src.yt.info import get_channel_with_videos, get_many_videos

log = logging.getLogger("fc")
logging.getLogger("googleapiclient.discovery").setLevel(logging.WARNING)

store = VC_Storage("fc")
youtube = get_youtube()

MIN_VIDEOS_AT_CHANNEL = 2
MIN_SOURCES_AT_VIDEOS_IN_CHANNEL = 5


def get_channel_statistics(channel_info):
    info, videos = channel_info
    s_views = 0
    s_video_count = len(videos)
    video_ids = map(lambda x: x.get('video_id'), videos)
    videos = get_many_videos(youtube, video_ids)
    for video in videos:
        s_views += video.get('yt_views')

    return dict(info, **{"views": s_views, "videos": s_video_count})


class RecursiveFinder(object):
    def __init__(self, comment_words):
        self.processed_channels = set()
        self.processed_channels_candidates = set()

        self.comment_words = comment_words

    def check_channel(self, channel_id):
        if channel_id in self.processed_channels_candidates:
            return False

        info, videos = get_channel_with_videos(channel_id, add_videos=True)
        if not info and not videos:
            log.warning("[%s] IS DELETED!" % channel_id)
            return False

        self.processed_channels_candidates.add(channel_id)
        if len(videos) >= MIN_VIDEOS_AT_CHANNEL:
            video_ids = map(lambda x: x.get('video_id'), videos[:MIN_VIDEOS_AT_CHANNEL])
            sources_ids_count = 0
            for loaded_video in get_many_videos(youtube, video_ids):
                sources_ids = get_sources_ids_of_compilation(loaded_video.get('channel_id'),
                                                             loaded_video.get('description'))
                sources_ids_count += len(sources_ids)
                if sources_ids_count >= MIN_SOURCES_AT_VIDEOS_IN_CHANNEL:
                    return info, videos
        return False

    def check_comment_text(self, comment_text):
        for word in self.comment_words:
            if word in comment_text:
                return True
        return False

    def get_next(self):
        channels = store.get_channels()

        for channel in channels:
            channel_id = channel.get('channel_id')
            if channel_id not in self.processed_channels:
                log.info("Will process channel: \n%s" % channel)

                info, channel_videos = get_channel_with_videos(channel_id=channel_id, add_videos=True)
                channel_videos = get_many_videos(youtube, map(lambda x: x.get('video_id'), channel_videos))

                for video in channel_videos:
                    sources = get_sources_ids_of_compilation(video.get('channel_id'), video.get('description'))
                    for source_video_id in sources:
                        comments = get_comments(youtube, video_id=source_video_id)
                        for comment in comments:
                            comment_channel_id = comment.get('channel_id')
                            comment_text = comment.get("text")

                            if comment_channel_id \
                                    and comment_channel_id not in self.processed_channels \
                                    and self.check_comment_text(comment_text) \
                                    and not store.get_channel(comment_channel_id):
                                log.info("Found comment: \n[%s]\nof [%s]" % (comment_text, comment.get('author')))
                                info = self.check_channel(comment_channel_id)
                                if info:
                                    self.processed_channels.add(comment_channel_id)
                                    channel_statistic = get_channel_statistics(info)
                                    log.info("Found channel:\n%s" % channel_statistic)
                                    yield channel_statistic


class FinderWrapper(object):
    def __init__(self):
        self.comment_words = []
        self.ensure_generator(self.comment_words)

    def ensure_generator(self, comment_words):
        if self.comment_words != comment_words:
            log.info("comment words changed: \n%s\n--->\n%s" % (self.comment_words, comment_words))
            self.finder = RecursiveFinder(comment_words)
            self.channel_generator = self.finder.get_next()
            self.comment_words = comment_words

    def get_channel(self):
        try:
            channel = self.channel_generator.next()
            return channel
        except Exception as e:
            self.channel_generator = self.finder.get_next()
            return None


finder = FinderWrapper()


class FindChannelsApiView(MethodView):
    methods = ['POST', 'GET', 'PUT']

    def get(self):
        return render_template("find_channels.html")

    def post(self):
        data = json.loads(request.data)
        comment_words = data.get('comment_words')
        comment_words = filter(lambda x: x, map(lambda x: x.strip(), comment_words.split()))

        try:
            finder.ensure_generator(comment_words)
            channel = finder.get_channel()
            if channel:
                return jsonify(**{"channels": [channel]})
            return jsonify(**{"end": True})
        except Exception as e:
            return jsonify(**{"error": str(e)})

    def put(self):
        data = json.loads(request.data)
        channel_id, action = data.get('channelId'), data.get('action')
        if action == 'include':
            info, _ = get_channel_with_videos(channel_id, add_videos=False)
            store.store_channel(channel_id, VT_COMPILATION, info)
        else:
            store.delete_channel(channel_id)
            store.set_channel_pleas_reject(channel_id)
        return jsonify(**{"ok": True, "action": action})
