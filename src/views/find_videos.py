import json
import logging

from flask import request, jsonify
from flask.templating import render_template
from flask.views import MethodView

from src.core.find import VideoFinderWrapper
from src.db import VC_Storage

finder = VideoFinderWrapper()
store = VC_Storage('fv')

log = logging.getLogger('fv')


class FindVideosApiView(MethodView):
    methods = ['POST', 'GET', 'DELETE', 'PATCH', ]

    def get(self):
        return render_template("find_videos.html")

    def post(self):
        if not request.data:
            return jsonify(**{"ok": True, "end": True})

        context = json.loads(request.data)

        if "q" in context and not context["q"]:
            return jsonify(**{"ok": True, "end": True})

        try:
            video = finder.get_next(context)
            if video:
                return jsonify(**{"ok": True, "videos": [video]})
            return jsonify(**{"ok": True, "end": True})
        except Exception as e:
            log.exception(e)
            return jsonify(**{"ok": False, "error": e.message})

    def delete(self):
        data = json.loads(request.data)
        channel_id = data.get('channel_id')
        store.set_channel_pleas_reject(channel_id)
        return jsonify(**{"ok": True})

    def put(self):
        pass
