import json

from flask import jsonify, request, render_template
from flask.views import MethodView

from src.yt.words_speeds import WordsStore, WordSpeedCalculator, prepare_semantic_core

ws = WordsStore()
word_speed_calculator = None


class KeysCoreView(MethodView):
    methods = ['GET', 'DELETE', 'PATCH']

    def get(self):
        core = ws.get_last_search_keys()

        return render_template('keys_core.html', **{'core': core})

    def patch(self):
        result = prepare_semantic_core()
        return jsonify(**{'ok': True, 'count': len(result)})

    def delete(self):
        what = json.loads(request.data)
        result = ws.delete_search_keys(what.get('keys'))
        return jsonify(**{'ok': True, 'deleted_count': result.deleted_count})


class WordsSpeedView(MethodView):
    methods = ['POST', 'GET', 'DELETE', 'PATCH', ]

    def get(self):
        all_words_speed = ws.get_last_words_result()
        global word_speed_calculator
        if word_speed_calculator and word_speed_calculator.is_alive():
            state = word_speed_calculator.get_current_state()
        else:
            state = 'not started'
        return render_template('words_speeds.html', **{"words": all_words_speed, 'state': state})

    def post(self):
        if not request.data:
            return jsonify(**{"ok": False})

        context = json.loads(request.data)
        global word_speed_calculator
        if word_speed_calculator == None or not word_speed_calculator.is_alive():
            word_speed_calculator = WordSpeedCalculator()
            word_speed_calculator.set_params(context or {})
            word_speed_calculator.daemon = True
            word_speed_calculator.start()
            state = '(re)started'
        else:
            state = word_speed_calculator.get_current_state()

        return jsonify(**{'ok': True, 'state': state})
