import json

from flask import request
from flask.json import jsonify
from flask.views import MethodView

from src.db import VC_Storage, DataCache
from src.pleas import initialise_first_pleas
from src.yt.info import get_video_info

store = VC_Storage('cuts')
cache = DataCache()


class CutsApi(MethodView):
    methods = ['POST', 'DELETE', 'GET']

    def post(self):
        data = json.loads(request.data)
        for cut in data.get('cuts'):
            video_id, from_time, to_time, cat, rate = data.get('video_id'), cut.get('from'), cut.get('to'), cut.get('category'), int(cut.get('rate') or 1)

            if not store.is_cut_exists(video_id, from_time, to_time):
                video = cache.get_data(video_id)
                if not video:
                    video = get_video_info(video_id)
                    if not video:
                        return jsonify(**{"ok": False, "error": "Not found video %s :(" % video_id})

                pleas = store.get_pleas(video_id)
                if not pleas:
                    initialise_first_pleas(video)

                store.store_video_cut(video, from_time, to_time, cat, rate)
            else:
                store.update_video_cut(video_id, from_time, to_time, cat, rate)

        return jsonify(**{"ok": True})

    def delete(self):
        cut = json.loads(request.data)
        video_id, from_time, to_time = cut.get('video_id'), cut.get('from'), cut.get('to')
        store.delete_video_cut(video_id, from_time, to_time)
        return jsonify(**{"ok": True})

    def get(self, video_id):
        video = store.get_video(video_id)
        if 'cut_times' in video:
            cuts_sorted = sorted(video['cut_times'], key=lambda x: x.get('from'))
            return jsonify(**{"ok": True, "cuts": list(cuts_sorted)})


