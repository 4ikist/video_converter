# coding=utf-8
import json
import logging
import os
import sys
import time
from random import randint

from googleapiclient.errors import HttpError

IS_TEST = os.getenv("VC_ENV", "").lower() in ('dev', "")

WORK_DIR = os.path.dirname(os.path.dirname(__file__))
VIDEOS_PATH = os.getenv("VC_VIDEO_PATH") or os.path.join(WORK_DIR, "video_files")

if not os.path.exists(VIDEOS_PATH):
    os.mkdir(VIDEOS_PATH)

CLIENT_SECRET = os.path.join(WORK_DIR, "client_secrets.json")


def init_log(name="", without_yt=False, stdout=True):
    log_file_path = os.path.join(
        os.path.dirname(os.path.dirname(__file__)),
        "result%s.log" % ("_%s" % name if name else ""))
    print "will storing log [%s] at\n%s" % (name, log_file_path)

    fmt = logging.Formatter("%(asctime)s|%(name)s|%(filename)s(%(lineno)d)|%(process)s %(message)s ")
    log = logging.getLogger(name or None)

    env = os.environ.get('VC_ENV')

    if env == 'production':
        log.setLevel(logging.WARN)
    else:
        log.setLevel(logging.INFO)

    fh = logging.FileHandler(log_file_path, mode="a")
    fh.setFormatter(fmt)
    log.addHandler(fh)

    if stdout:
        sh = logging.StreamHandler(sys.stdout)
        sh.setFormatter(fmt)
        log.addHandler(sh)

    if without_yt:
        logging.getLogger("googleapiclient.discovery").setLevel(logging.WARNING)

    logging.getLogger("requests").setLevel(logging.WARNING)

    if IS_TEST:
        log.info("!!!!!!!!!!!!!!!!!!!WILL WORK IN DEV MODE! NO SEND TO YOUTUBE!!!!!!!!!!!!!!!!!!!!!!. [%s]" % name)

    return log


log = init_log(without_yt=True)

tryings_count = 3
sleep_between_try = 2


def tryings(time_to_sleep=sleep_between_try, count_tryings=tryings_count, randomized=False):
    def dec(fn):
        def wrapped(*args, **kwargs):
            count = 0
            while 1:
                try:
                    result = fn(*args, **kwargs)
                    return result
                except Exception as e:
                    log.exception(e)
                    log.warning("Can not exec %s :(\nbecause: %s \nwill try %s..." % (fn, e, count))

                    if count >= count_tryings:
                        return None

                    if randomized:
                        sleep_time = randint(time_to_sleep - time_to_sleep / 2, time_to_sleep + time_to_sleep / 2)
                    else:
                        sleep_time = time_to_sleep

                    log.info('Will sleep %s for next trying...')
                    time.sleep(sleep_time)
                    count += 1

        return wrapped

    return dec


def net_tryings(fn):
    def wrapped(*args, **kwargs):
        count = 0
        while 1:
            try:
                result = fn(*args, **kwargs)
                return result
            except HttpError as e:
                log.exception(e)
                if e.resp.get('status') in ("400", "403", "404"):
                    return None
            except Exception as e:
                log.exception(e)
                log.warning("can not load data for [%s]\n args: %s, kwargs: %s \n because %s" % (fn, args, kwargs, e))
                if count >= tryings_count:
                    raise e
                time.sleep(sleep_between_try)
                count += 1

    return wrapped


class ConfigManager(object):
    def __init__(self):
        env = os.getenv('VC_ENV', 'dev')
        config_file = os.path.join(WORK_DIR, "config_%s.json" % env)
        try:
            f = open(config_file, )
        except Exception as e:
            log.exception(e)
            log.error("Can not read config file %s" % config_file)
            sys.exit(-1)

        self.config_data = json.load(f)
        self.config_file = config_file

        log.info(
            "LOAD CONFIG DATA FROM %s:\n%s" % (
                config_file,
                "\n".join(["%s: %s" % (k, v) for k, v in self.config_data.iteritems()]))
        )

    def get(self, name, default=None):
        result = self.config_data.get(name)
        if not result:
            log.info("Not %s in %s :(" % (name, self.config_file))
            return default
        return result


config = ConfigManager()

PS_ASKED = "ask"
PS_APPROVED = "approve"
PS_REJECTED = 'reject'
PS_CLOSED = 'close'
PS_ADD_VIDEO = 'add_video'

VPS = [PS_ASKED, PS_APPROVED, PS_REJECTED]

VT_COMPILATION = "compilation"
VT_SOURCE = "source"
VT_EXCLUDE = "exclude"
VT_ERROR = "error"
VT_COMPILATION_WITHOUT_SOURCES = "compilation_without_sources"

CAT_FROM_COMPILATOR = u"FROM_COMPILATOR"
CAT_FROM_4LB = u"FROM_4ЛB"

VIDEO_ID_LENGTH = 11
CHANNEL_ID_LENGTH = 24

CS_CREATED = "created"
CS_PREPARED = "prepared"
CS_CONCATENATED = "concatenated"
CS_UPLOADED = "uploaded"
CS_LAST_PLEAS_SENT = "last_pleas_sent"
CS_ERROR = "error"

CHS_TRACKED = "tracked"
