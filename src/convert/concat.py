import logging
import os
import re

import copy

from src import VIDEOS_PATH
from src.convert import pre_logo_fn, post_logo_fn, DEFAULT_EXTENSION, DQ_X, DQ_Y

log = logging.getLogger("concat")


class subprocess_wrapper:
    @staticmethod
    def prepare_command(command):
        result = []
        buff = []

        for token in command.split():
            if token.startswith('"') and not token.endswith('"'):
                buff.append(token)
            elif token.endswith('"') and not token.startswith('"'):
                buff.append(token)
                _token = " ".join(buff)
                buff = []
                result.append(_token)
            else:
                result.append(token)

        log.info("Command list: \n%s" % "\n".join(result))
        return result

    @staticmethod
    def run_command(exe):
        log = logging.getLogger("concat")
        import subprocess
        p = subprocess.Popen(exe, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        while (True):
            retcode = p.poll()  # returns None while subprocess is running
            line = p.stdout.readline()
            yield line, retcode
            if (retcode is not None):
                log.info("command result code is: %s" % retcode)
                break


def add_to_fn(fn, what):
    file_name, _ = os.path.splitext(fn)
    encoded_fn = "%s-%s.mp4" % (file_name, what)
    return encoded_fn


def get_file_name(fn):
    file_name_with_ext = os.path.basename(fn)
    file_name, ext = os.path.splitext(file_name_with_ext)
    return file_name


def prepare_out_file_name(out_name):
    out_name_with_path = os.path.join(VIDEOS_PATH, out_name)
    if os.path.isfile(out_name_with_path):
        os.remove(out_name_with_path)
    return out_name_with_path


def fill_logos(video_file_names):
    file_names = []
    if os.path.isfile(pre_logo_fn):
        file_names.append(pre_logo_fn)
    file_names.extend(video_file_names)
    if os.path.isfile(post_logo_fn):
        file_names.append(post_logo_fn)
    return file_names


def concatenate_filter_concat(video_file_names, out_name="out", remove_intermediate=True):
    prepare_command = 'ffmpeg -i "%s" -c copy -bsf:v h264_mp4toannexb -f mpegts -y "%s"'
    concat_command = 'ffmpeg -i "concat:%s" -c copy -bsf:a aac_adtstoasc -y "%s"'
    out_name = "%s.%s" % (out_name, DEFAULT_EXTENSION)
    out_name_with_path = prepare_out_file_name(out_name)
    _intermediate = []

    video_file_names = fill_logos(video_file_names)

    for video_file in video_file_names:
        intermediate_file_name = os.path.join(VIDEOS_PATH, "%s-intermediate.ts" % (get_file_name(video_file)))
        _intermediate.append(intermediate_file_name)

        command = prepare_command % (video_file, intermediate_file_name)
        log.info("prepare file command:\n%s" % command)
        result_code = os.system(command)
        if result_code != 0:
            return

    command = concat_command % ("|".join(_intermediate), out_name_with_path)
    log.info("concatenate command:\n%s" % command)
    result_code = os.system(command)
    if result_code != 0:
        return
    if remove_intermediate:
        for intermediate_file_name in _intermediate:
            os.remove(intermediate_file_name)

    return out_name_with_path


def concatenate_filter_complex(video_file_names, out_name="out"):
    out_name = "%s.%s" % (out_name, DEFAULT_EXTENSION)
    out_name_with_path = prepare_out_file_name(out_name)

    video_file_names = fill_logos(scale_videos(video_file_names))

    command = 'ffmpeg %s -r 24000/1001 -filter_complex "%s concat=n=%s:v=1:a=1 [v] [a]" -map "[v]" -map "[a]" -y %s'
    inputs = " ".join(['-i "%s"' % fn for fn in video_file_names])
    filter_complex_command = " ".join(["[%s:v:0] [%s:a:0]" % (i, i) for i, _ in enumerate(video_file_names)])
    count = len(video_file_names)
    result_command = command % (inputs, filter_complex_command, count, '"%s"' % out_name_with_path)

    result_code = os.system(result_command)
    log.info("Concatenate command:\n%s\nEnded with code:%s" % (result_command, result_code))
    if result_code != 0:
        return
    return out_name_with_path


def _get_video_scale(video_file):
    pattern = re.compile('streams_stream_0_width=(?P<width>\d+)\nstreams_stream_0_height=(?P<height>\d+)\n')
    scale_result_txt = '%s_scale_result.txt' % video_file

    command = 'ffprobe -v error -of flat=s=_ -select_streams v:0 -show_entries stream=height,width %s > %s' % (
        video_file, scale_result_txt)
    os.system(command)

    with open(scale_result_txt) as f:
        result = f.read()
        matched = pattern.match(result)

    if matched:
        found = matched.groupdict()
        os.remove(scale_result_txt)
        return found


def _scale_video(video_file, x=DQ_X, y=DQ_Y, ):
    scale = _get_video_scale(video_file)
    if scale and int(scale.get('width')) == int(x) and int(scale.get('height')) == int(y):
        return video_file

    scaled_video_file = '%s_scaled_%sx%s.mp4' % (video_file, x, y,)

    command = 'ffmpeg -i "%s" -vf scale=%s:%s "%s" -hide_banner' % (
        video_file,
        x, y,
        scaled_video_file
    )

    result_code = os.system(command)
    log.info("Scale video [%s] result: %s" % (video_file, result_code))
    if result_code != 0:
        log.error("Error at scale video [%s]" % video_file)
        return

    return scaled_video_file


def scale_videos(video_file_names):
    for i, video_fn in enumerate(copy.deepcopy(video_file_names)):
        s_video_fn = _scale_video(video_fn)
        if s_video_fn:
            video_file_names[i] = s_video_fn
        else:
            log.error("Video [%s] can not used in compilation because can not be scaled :( " % video_fn)
            video_file_names.remove(video_file_names[i])

    return video_file_names
