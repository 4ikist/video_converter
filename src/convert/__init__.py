import os

from src import VIDEOS_PATH, config

DEFAULT_EXTENSION = 'mp4'
DQ_X = config.get('default_quality_resolution', {}).get('width') or 1280
DQ_Y = config.get('default_quality_resolution', {}).get('height') or 720
DEFAULT_RESOLUTION = '%sx%s' % (DQ_X, DQ_Y)
DEFAULT_RESOLUTION_SHORT = '720p'

pre_logo_fn = os.path.join(VIDEOS_PATH, "_pre_logo.%s" % (DEFAULT_EXTENSION))
post_logo_fn = os.path.join(VIDEOS_PATH, "_post_logo.%s" % (DEFAULT_EXTENSION))


def clear_cache():
    file_names = os.listdir(VIDEOS_PATH)
    for fn in file_names:
        os.remove("%s\\%s" % (VIDEOS_PATH, fn))


def test_concat(path_of_files, mask=None):
    from src.convert.concat import concatenate_filter_complex
    import os
    files = []
    for fn in os.listdir(path_of_files):
        if mask and mask in fn:
            files.append(os.path.join(path_of_files, fn))

    result = concatenate_filter_complex(files)
    return result


def test_scale(file_name):
    from src.convert.concat import scale_videos
    scale_videos([os.path.join(VIDEOS_PATH, file_name)])


if __name__ == '__main__':
    # clear_cache()
    # test_concat(VIDEOS_PATH, "cut")
    test_scale('1_0-3_cut.mp4')
