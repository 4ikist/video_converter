import logging
import os
from datetime import datetime

from src import VIDEOS_PATH

log = logging.getLogger("cut")


def cut_video(video_id, f, t, v_extension='mp4'):
    cut_file_name_f = lambda video_id, f, t: os.path.join(VIDEOS_PATH,
                                                          '%s_%s-%s_cut.%s' % (video_id, f, t, v_extension))

    cut_fn = cut_file_name_f(video_id, f, t)
    log.info("Will cut: %s and create file: %s" % (video_id, cut_fn))
    if os.path.isfile(cut_fn):
        return cut_fn

    command = 'ffmpeg -i "%s" -ss %s -t %s -async 1 -strict -2 -y "%s"' % (
        os.path.join(VIDEOS_PATH, "%s.%s" % (video_id, v_extension)),
        f,
        t - f,
        cut_fn
    )
    result_code = os.system(command)
    log.info("Cut %s from: %s to: %s\n%s\nWith code %s" % (video_id, f, t, command, result_code))
    if result_code == 0:
        return cut_fn


compilation_file_name = lambda compilation_id: "compilation_%s_%s" % \
                                               (compilation_id, datetime.now().strftime("%S%M%H%d%m%y"))

if __name__ == '__main__':
    videos_cut = [
        {"video_id": "3", "f": 0, "t": 3},
        # {"video_id": "2", "f": 5, "t": 8},
        # {"video_id": "Ik-RsDGPI5Y", "f": 6, "t": 9},
        # {"video_id": "Ik-RsDGPI5Y", "f": 7, "t": 10},
        # {"video_id":"UUkrhQ5JV50", "f":6, "t":20},
        # {"video_id":"UUkrhQ5JV50", "f":7, "t":20},
        # {"video_id":"UUkrhQ5JV50", "f":8, "t":20},
        # {"video_id":"UUkrhQ5JV50", "f":9, "t":20},
        # {"video_id":"UUkrhQ5JV50", "f":10, "t":20},
        # {"video_id":"Ik-RsDGPI5Y", "f":20, "t":30},
        # {"video_id":"Ik-RsDGPI5Y", "f":40, "t":50},
        # {"video_id":"Ik-RsDGPI5Y", "f":60, "t":70},
    ]
    for v in videos_cut:
        log.info("Cut video: %s" % cut_video(**v))
