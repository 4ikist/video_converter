import time

from src import VT_COMPILATION, VT_SOURCE, CS_LAST_PLEAS_SENT, CS_CONCATENATED, CS_UPLOADED, CS_PREPARED, PS_ASKED, \
    PS_REJECTED
from src.db import VC_Storage
from src.yt.info import get_video_info


def remove_pleases_replies(store):
    store.pleas_replies.drop()


def remove_please_dialogs(store):
    result = store.pleases.update_many({},
                                       {"$pull":
                                            {"dialog": {"reply_id": {"$exists": True}}}
                                        })
    print result


def change_channels_type(store):
    store.channels.update_many({"type": 'extract'}, {"$set": {"type": VT_COMPILATION}})
    store.channels.update_many({"type": 'please'}, {"$set": {"type": VT_SOURCE}})


def set_compilation_upload_time(store):
    store.compilations.update_many({"video_id": {"$exists": True}}, {"$set": {"upload_time": time.time()}})


def fix_compilations_states():
    store = VC_Storage()
    compilations = store.compilations.find({})
    for compilation in compilations:
        comp_id = compilation.get("_id")
        store.set_compilation_state(comp_id, CS_PREPARED)
        if compilation.get("out_fn"):
            store.set_compilation_state(comp_id, CS_CONCATENATED)
            print comp_id, CS_CONCATENATED
        if compilation.get("video_id"):
            store.set_compilation_state(comp_id, CS_UPLOADED)
            print comp_id, CS_UPLOADED
        if compilation.get("last_please_send"):
            store.set_compilation_state(comp_id, CS_LAST_PLEAS_SENT)
            print comp_id, CS_LAST_PLEAS_SENT


def delete_will_be_not_delete(store):
    store.videos.update_many({"deleted": True}, {"$unset": {"deleted": 1, "BAD": 1}})


def add_time_to_videos(store):
    store.videos.update_many({}, {"$set": {"time": time.time()}})


def add_channel_names_for_pleases(store):
    pleases = store.get_all_pleases(except_states=[PS_REJECTED])
    counter = 0
    for pleas in pleases:
        v_id = pleas.get('video_id')
        pleas_channel_id = pleas.get('channel_id')
        if pleas_channel_id:
            continue
        video = store.get_video(v_id) or get_video_info(v_id)
        if video:
            store.pleases.update_one({'video_id': v_id}, {'$set': {'channel_id': video.get('channel_id')}})
            print "for please: %s found channel id: %s" % (v_id, video.get('channel_id'))
            counter += 1
    print "Updated %s pleases" % counter


def stop_send_pleases(store, exc_compilation_video_id):
    compilation = store.compilations.find_one({'video_id': exc_compilation_video_id})
    if not compilation:
        print 'No compilation with that %s video id... ' % exc_compilation_video_id
    videos = compilation.get('cuts')
    except_video_ids = map(lambda x: x.get('video_id'), videos)
    print 'Will set send please. But except %s' % except_video_ids
    pleases = store.get_all_pleases(except_states=PS_REJECTED)
    for pleas in pleases:
        video_id = pleas.get('video_id')
        if video_id not in except_video_ids and pleas.get('send', False) == False:
            store.update_please_send_state(video_id, send=True)
            print 'set please %s was sended' % video_id


def change_pleas_templates_structure(store):
    store.pleas_templates.create_index('add-video')
    store.pleas_templates.create_index('finish')
    store.pleas_templates.drop_index('finished')


if __name__ == '__main__':
    store = VC_Storage(ensure_indexes=True)
    # store.set_channel_tracking('UCEMga_5kPDRwFXaJM1NQryA', track=False)
    # fix_compilations_states()
    # remove_pleases_replies(store)
    # remove_please_dialogs(store)
    # change_channels_type(store)
    # set_compilation_upload_time(store)
    # delete_will_be_not_delete(store)
    # add_time_to_videos(store)
    # add_channel_names_for_pleases(store)
    # stop_send_pleases(store, 'm02YLYMKGXs')
    change_pleas_templates_structure(store)
