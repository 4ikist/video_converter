set VC_ENV=production

set startdir=%cd%
set PYTHONPATH=%PYTHONPATH%;%startdir%\src
set RR_CONFIG_PATH=%startdir%\config_%VC_ENV%.json

start /B python src\pleas\sender.py >> send.log
start /B python src\compilations\__init__.py >> compilate.log
start /B python src\server.py >> main.log

rem cd ..\sniffer
rem start.cmd